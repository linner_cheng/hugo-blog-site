---
title: Author
menu:
  main:
    parent: blog
    weight: 5
    params:
      icon: <i class="fas fa-fw fa-user"></i>
      # description: List of series.
---
