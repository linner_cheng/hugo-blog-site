+++
title = "关于我"
description = "Describe yourself."
date = "2022-09-02 16:04:59"
reward = false
postDate = false
readingTime = false
+++

# 微信公众号——阿码研究社

![阿码研究社](wechat-official.jpg#center)

---

# 搭建目的

搭建这个博客网站的搭建目的主要有：

1. 展示个人技术能力。
2. 知识整理与总结。

    编写博客文章有助于对所学知识进行整理和总结。在写作的过程中，需要深入思考、梳理和表达自己的观点，这有助于加深对知识的理解和记忆，并提升自己的学习效果。

3. 交流与分享。

    通过博客文章，分享经验、见解和解决方案。

4. 拓展职业发展机会。

---

# 网站的搭建

本站基于[Hugo](https://gohugo.io/)博客框架，使用[`hugo-theme-bootstrap`](https://github.com/razonyang/hugo-theme-bootstrap)主题。

本站的Markdown文档和配置等存放在[https://gitee.com/linner_cheng/hugo-blog-site](https://gitee.com/linner_cheng/hugo-blog-site)。

本站原本部署于Github Pages，仓库地址为[https://github.com/Linna-cy/Linna-cy.github.io](https://github.com/Linna-cy/Linna-cy.github.io)。但由于Github在国内访问速度不是很理想，后改用[Netlify](https://app.netlify.com/)部署。Netlify的访问速度相较于Github Pages来说好一点。

编辑工具使用的是VS Code。如果你熟悉Markdown和Html等语言的语法，使用VS Code来编辑博客特别爽！

为什么使用VS Code？

在我的个人电脑上，我喜欢使用Docker来搭建一些服务，这样能让我的电脑尽量减少一些因为环境冲突带来的错误。本站的搭建也是在通过Docker容器中安装Hugo以及Golang、Node.js等软件来构建。使用VS Code能让我更加方便地连接到Docker容器中，并且在VS Code上编辑的体验还是很不错的。

![使用VS Code编辑](uTools_1690986627941.png)

通常撰写博客时，我的工作目录和博客目录是不一致的。我的工作目录是在博客目录的`content`目录之下。这样的好处是能让我更加专注于内容的撰写。当我要编写一篇博文时，不会因为其它不相关的目录来干扰我。但是由于工作目录与博客目录不一致，所以在预览网站效果或者发布内容的时候需要经常`cd`到上层目录。为了解决这些问题，我在工作目录里面编写了两个脚本文件`hugo.sh`和`git.sh`来简化我的工作流程。

---

> 关于本网站的任何问题或者关于文章的任何见解，可以在文章下方评论留言；或者点击联系我，提交您的建议。
