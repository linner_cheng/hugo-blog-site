---
# type: docs 
title: Docker 镜像操作
date: 2023-08-01T14:22:10Z
tags: [Docker]
draft: false
series: [Docker]
authors: Linner
navWeight: 98
categories: [note]
---

# 拉取镜像

在使用容器前，需要先获取相对应的镜像。例如要在Docker中安装MySQL 5.7，需要先获取MySQL 5.7的Docker镜像。相应的命令为：

```shell
docker pull mysql:5.7
```

命令的本质意思为，从Docker镜像仓库中获取版本为5.7的MySQL镜像。

在Docker中镜像的标识由两部分组成：

- **Repository**：通常是由镜像主要运行的软件名称组成。例如Tomcat的镜像的Repository就是`tomcat`。
- **Tag**：通常代表镜像的版本。在没有显式指定Tag的情况下，Docker会给Tag一个默认值`latest`，代表当前镜像的最新版本。

    例如拉取MySQL最新版本的镜像：

    ```shell
    docker pull mysql
    ```

    > 在Docker中，没有指定Tag的情况下，都是指使用`latest`。

每个`[repository]:[tag]`标识了唯一的一个镜像。

镜像除了`[repository]:[tag]`这个标识外，还有一个镜像ID（Image ID）。但通常使用镜像时都是使用`[repository]:[tag]`这个标识。

# 查找镜像

在我们不知道要使用的镜像的具体名称时，除了可以去[Docker Hub](https://hub.docker.com/)上查找外，还可以通过使用`docker search`命令搜索。例如搜索`nginx`相关的镜像：

```shell
docker search nginx
```

这个命令会将镜像仓库中包含`nginx`字符的镜像一一列举出来。

`docker search`查找的是镜像仓库上的镜像。如果要查看本地镜像，可以使用`docker images`命令。`docker images`命令会列出你本地上所有的镜像，并且列出镜像的`REPOSITORY`、`TAG`、`IMAGE ID`等信息。

# 删除镜像

拉取到本地或者自己创建的镜像，可以使用`docker rmi`命令来删除。例如删除`mysql:5.7`镜像：

```shell
docker rmi mysql:5.7
```

# 镜像的保存和载入

创建在Docker中的镜像，默认情况下是不能移动的。但是可以通过使用`docker save`命令将镜像导出到磁盘中：

```shell
docker save -o [保存的目标文件路径名称] [镜像名称]
```

例如导出`mysql:latest`到磁盘：

```shell
docker save -o mysql_latest.tar mysql:latest
```

> 导出的镜像是一个`tar`（压缩）包。

将导出的镜像载入Docker可以使用`docker load`命令：

```shel
docker load -i [镜像tar包名]
```

例如将导出的`mysql_latest.tar`导入Docker：

```shell
docker load -i mysql_latest.tar
```

> 注：在使用tar包导入镜像时，Docker会在tar包中读取镜像的Repository和Tag等信息。由于`[repository]:[tag]`标识唯一的一个镜像。所以在导入`mysql_latest.tar`之前需要先在Docker中删掉`mysql:latest`镜像：
> 
> ```shell
> docker rmi mysql
> ```
