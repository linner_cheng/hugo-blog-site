---
# type: docs 
title: Docker Hub
date: 2023-08-01T14:20:26Z
tags: [Docker]
toc: false
draft: false
series: [Docker]
authors: Linner
navWeight: 99
categories: [note]
---

DockerHub是一个官方的Docker镜像的托管平台，里面存放了人们配置好的镜像。这样的平台称为Docker Registry。

国内也有这样的Docker仓库，它们被称为Docker的镜像仓库或镜像源。这些镜像源都是与Docker官方仓库同步的。

例如导入腾讯的镜像源：

1. 修改`/etc/docker/daemon.json`配置文件：

    ```shell
    vim /etc/docker/daemon.json
    ```
2. 添加以下内容，并保存：

    ```json
    {
      "registry-mirrors": [
        "https://mirror.ccs.tencentyun.com"
      ]
    }
    ```

3. 重启Docker：

    ```shell
    sudo systemctl restart docker
    ```
