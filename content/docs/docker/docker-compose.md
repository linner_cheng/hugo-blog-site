---
# type: docs 
title: Docker Compose
date: 2023-08-02T08:32:18Z
tags: [Docker]
draft: false
series: [Docker]
authors: Linner
navWeight: 96
categories: [note]
---

# 介绍

Docker Compose是一个文本文件，用于定义和运行多容器 Docker应用程序的工具。通过Compose，可以使用YML文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。

Compose使用的三个步骤：

- 使用Dockerfile定义应用程序的环境。
- 使用`docker-compose.yml`定义构成应用程序的服务，这样它们可以在隔离环境中一起运行。
- 最后，执行`docker-compose up`命令来启动并运行整个应用程序。

`docker-compose.yml`的配置案例如下：

```yaml
version: "3.8"
 services:
  mysql:
    image: mysql:5.7.25
    environment:
     MYSQL_ROOT_PASSWORD: 123456
    volumes:
     - "/tmp/mysql/data:/var/lib/mysql"
     - "/tmp/mysql/conf/hmy.cnf:/etc/mysql/conf.d/hmy.cnf"
  web:
    build: .
    ports:
     - "8090:8090"
```

上方的`docker-compose.yml`描述了一个项目，其中包含两个容器：

- `mysql`：一个基于`mysql:5.7.25`镜像构建的容器，并且挂载了两个目录。
- `web`：一个基于`docker build`临时构建的镜像容器，映射端口为`8090`。

Docker Compose的详细语法可参考官网[Compose file reference](https://docs.docker.com/compose/compose-file/)或者菜鸟教程的[Docker Compose](https://www.runoob.com/docker/docker-compose.html)。

`docker-compose.yml`可以看做是将多个`docker run`命令写到一个文件，只是语法稍有差异。

将`docker-compose.yml`和Dockerfile放置在同一个工作目录中，然后执行以下命令启动程序：

```shell
docker-compose up
```

命令执行成功后Docker Compose会自动帮你构建所需的镜像。

如果想在后台执行该服务可以加上`-d`参数：

```shell
docker-compose up -d
```

---

# 安装

Linux上可以从Github上下载Compose的二进制包来使用，最新发行的版本地址：[https://github.com/docker/compose/releases](https://github.com/docker/compose/releases)。运行以下命令以下载 Docker Compose 的当前稳定版本：

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

如果要安装其他版本的Compose，请替换URL中的`v2.2.2`。

> Docker Compose存放在 GitHub，不太稳定。可以通过执行下面的命令，高速安装Docker Compose：
> 
> ```shell
> curl -L https://get.daocloud.io/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
> ```

将可执行权限应用于二进制文件：

```shell
sudo chmod +x /usr/local/bin/docker-compose
```

创建软链：

```shell
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

测试是否安装成功：

```shell
docker-compose version
```

> 注意： 对于Alpine，需要以下依赖包：`py-pip`，`python-dev`，`libffi-dev`，`openssl-dev`，`gcc`，`libc-dev和make`。
> 
> MacOS和Windows的Docker Desktop已经包含Compose和其他Docker应用程序，所以无需再进行安装。
