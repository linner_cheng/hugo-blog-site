---
# type: docs 
title: Docker 容器操作
date: 2023-08-01T14:24:30Z
tags: []
draft: false
series: [Docker]
authors: Linner
navWeight: 97
categories: [note]
---

# 容器状态

Docker容器一般会有以下三种状态：

- **运行**：进程正常运行。
- **暂停**：进程暂停，CPU不再运行，但不会释放内存。
- **停止**：进程终止，回收进程占用的内存、CPU等资源。

相关的命令如下：

命令            |说明
:--------------:|:----------------------------------------------------------
`docker run`    |创建并运行一个容器，正常情况下处于运行状态。
`docker pause`  |让一个运行的容器暂停。例如`docker pause my-debian`。
`docker unpause`|让一个容器从暂停状态恢复运行。例如`docker unpause my-debian`。
`docker stop`   |停止一个运行的容器。例如`docker stop my-debian`。
`docker start`  |让一个停止的容器再次运行。例如`docker start my-debian`。
`docker rm`     |删除一个容器。例如`docker rm my-debian`。

`docker pause`、`docker unpause`、`docker stop`、`docker start`和`docker rm`的用法十分相似，都是在命令后面指定要操作的容器名称或者容器ID（Container ID）。

# 创建容器

创建容器使用`docker run`命令。可以使用`docker run --help`查看它的用法。

如果要创建一个`debian`容器，可以使用以下命令：

```shell
docker run -ti \    # 参数 -ti 表示使用终端交互式操作
    --name my-debian \  # --name 指定容器的名称
    -p 8088:80 \    # -p 指定映射的端口，将容器的80端口映射到本地8088
    -v /home/debian:/root \ # -v 代表挂载，将本地的/home/deian目录挂载到容器的/root目录
    debian:latest \ # 指定创建容器的镜像
    /bin/bash       # 容器运行的命令，这里指运行容器的bash终端程序（也就是常见的命令行窗口）
```

运行该命令会进入创建好的Debian容器中（默认是进入`/`目录），输入`exit`可以退出Debian终端。

容器挂载目录和端口映射的语法类似，它们都使用`:`符号分隔宿主机和容器。`:`左边的是宿主机的目录地址或端口号，`:`右边的是容器的目录地址或端口号。它们的语法如下：

操作    |命令参数语法
:-----:|:------------------------------------:
挂载目录|`-v [宿主机目录或数据卷名称]:[容器目录]`
端口映射|`-p [宿主机可用端口号]:[容器应用端口号]`

> 注：在使用`docker run`指令时，如果指定的镜像未`pull`，但在镜像仓库中存在，<u>`docker run`会自动从仓库中拉取镜像</u>。

大部分情况下容器是在后台运行的，可以通过使用`-d`指定容器的运行模式：

```shell
docker run -tid --name my-debian debian:latest /bin/bash
```

加了`-d`参数就不会进入容器里面。

> 注：Docker容器和镜像一样，它们除了ID之外都有一个易于使用的标识。Docker容器的标识就是`NAME`。容器的名称在Docker中标识唯一的一个容器，但是容器的名称可以为空（在`docker run`时不指定`--name`）。名称为空的容器需要使用容器ID来对其进行操作。

# 操作容器内部

在后台运行的容器，我们想要进入容器内部或者在容器运行时运行一些其它命令，可以使用`docker exec`命令。

例如要进入在后台运行的`my-debian`：

```shell
docker exec -ti my-debian /bin/bash
```

又或者假设你在`my-debian`中装了MySQL，但是启动容器时MySQL Server并没有启动，此时你可以运行以下命令重启MySQL：

```shell
docker exec -tid my-debian systemctl restart mysql
```

# 查看容器

查看所有正在运行的Docker容器有3种方法：

```shell
docker ps
docker container ls
docker container ps
```

查看Docker所有的容器（包括停止运行的容器）：

```shell
docker ps -a
docker container ls -a
docker container ps -a
```

# 数据卷

在给容器挂载目录之前可以创建一个数据卷，这个数据卷也是在宿主机上的一个文件夹，这个文件夹的路径由Docker确定。一旦完成数据卷挂载，对容器中被挂载目录的一切操作都会作用在数据卷对应的宿主机目录。

数据卷的作用是将容器与数据分离，解耦合，方便操作容器内数据，保证数据安全。

数据卷操作命令是`docker volume`：

```shell
docker volume [COMMAND]
```

数据卷操作：

命令                   |说明
:---------------------:|:---------------------------------------------------------------------
`docker volume create` |创建数据卷。例如创建一个存放静态页面的数据卷：`docker volume create html`。
`docker volume ls`     |查看所有数据卷。
`docker volume inspect`|查看数据卷详细信息，包括关联的宿主机目录位置。例如查看上面创建的`html`数据卷：`docker volume inspect html`。
`docker volume rm`     |删除指定数据卷。删除`html`数据卷`docker volume rm html`。
`docker volume prune`  |删除所有未使用的数据卷。

假设要将`html`数据卷挂载到`nginx`上，可以这样使用：

```shell
docker run -d \
    --name nginx-test \
    -v html:/usr/share/nginx/html \
    -p 8080:80 \
    nginx
```

> 注：如果创建容器时发现`/usr/share/nginx`目录不存在，可以使用`Dockerfile`先行创建目录。

例如创建一个MySQL容器：

```shell
docker run -id --name %name% \
    -p 3306:3306 \
    -v /home/docker/mysql/lib:/var/lib/mysql \
    -v /home/docker/mysql/conf.d:/etc/mysql/conf.d \
    -v /home/docker/mysql/log:/var/log/mysql \
    -v /home/docker/mysql/my.cnf:/etc/my.cnf \
    -e TZ=Asia/Shanghai \           # 设置环境变量
    -e MYSQL_ROOT_PASSWORD=123456 \ # 某些容器创建时是通过环境变量来配置
    mysql \
    --character-set-server=utf8mb4 \        # 这些是mysql命令的参数，与Docker无关
    --collation-server=utf8mb4_general_ci
```
