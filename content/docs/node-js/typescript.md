---
# type: docs 
title: 'TypeScript 介绍'
# linkTitle: 介绍
date: 2023-08-18T09:21:24Z
featured: false
draft: false
pinned: false
series: ['Node.js']
categories: ['note']
tags: []
navWeight: 92
authors: Linner
---

TypeScript 是 JavaScript 的超集，扩展了 JavaScript 的语法，因此现有的 JavaScript 代码可与 TypeScript 一起工作无需任何修改TypeScript 是一种给 JavaScript 添加特性的语言扩展。增加的功能包括：

- 类型批注和编译时类型检查。
- 类型推断。
- 类型擦除。
- 接口。
- 枚举。
- Mixin。
- 泛型编程。
- 名字空间。
- 元组。
- Await。

---

# 安装 TypeScript

安装TypeScript：

```shell
npm install -g typescript
```

安装完成后可以使用 `tsc` 命令来执行 TypeScript 的相关代码。查看 TypeScript 版本号：

```shell
tsc -v
```

---

# Hello

新建一个`hello.ts`（通常使用 `.ts` 作为 TypeScript 代码文件的扩展名）：

```typescript
console.log('Hello TypeScript!');
```

执行以下命令将 TypeScript 转换为 JavaScript 代码：

```shell
tsc hello.ts
```

执行完成后会在当前目录下生成一个`hello.js`（与执行`tsc`时指定的`.ts`同名的`.js`文件）。然后使用`node`来执行`hello.js`：

```shell
node hello.js
```

---

# 变量声明

在TypeScript中声明变量，可以像JS那样去声明变量，当然也可以为变量指定类型：

```typescript
var [variableName] : [variableType] = value
```

类型断言（Type Assertion）可以用来手动指定一个值的类型，即允许变量从一种类型更改为另一种类型：

```typescript
<type>value
// 或者使用 as
value as type
```
