---
# type: docs 
title: 'Node.js Nodemon 模块'
date: 2023-08-12T13:01:59Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
categories: [note]
authors: Linner
tags: []
images: []
navWeight: 93
---

Nodemon是一种监听项目文件变动，让项目文件在修改时可以自动重启项目的工具。使用Nodemon时，当代码被修改，Nodemon会自动帮我们重启项目，极大方便了开发和调试。

Nodemon项目地址：[https:/www.npmjs.com/package/nodemon](https:/www.npmjs.com/package/nodemon)。

使用Nodemon时，一般是在全局进行安装：

```shell
npm i nodemon -g
```

安装完Nodemon后，可以使用`nodemon`命令来启动项目，这样当项目文件被修改时，`nodemon`会自动完成项目的重启。例如：

```shell
nodemon app.js
```
