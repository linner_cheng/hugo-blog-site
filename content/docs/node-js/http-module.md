---
# type: docs 
title: 'Node.js HTTP 模块'
linkTitle: 'HTTP 模块'
date: 2023-08-07T08:05:53Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
categories: [note]
authors: Linner
tags: []
images: []
navWeight: 97
---

`http`模块是Node.js官方提供的、用于创建Web服务器的模块。通过`http`模块提供的`http.createServer()`方法，就能像Web服务器一样对外提供Web资源服务。

在Node.js中无需使用第三方Web服务器软件，通过`http`模块就能模拟服务器软件，从而对外提供Web服务。

`http`模块主要用于搭建HTTP服务端和客户端，使用HTTP服务器或客户端功能必须调用`http`模块。在Node.js中使用Web服务的基本步骤如下：

1. 导入`http`模块：

    ```javascript
    const http = require('http')
    ```

2. 创建Web服务器实例。

    ```javascript
    const server = http.createServer()
    ```

3. 为服务器实例绑定`request`事件，监听客户端的请求。

    ```javascript
    server.on('request', (req, res) => {
        console.log('Someone visit our web server.');
        /* Request Handle... */
    })
    ```

4. 启动服务器。

    ```javascript
    server.listen(8080, () => { // 监听 8080 端口
        console.log('Server running at http://127.0.0.1:8080');
        /* Started Handle... */
    })
    ```

HTTP Demo如下：

```javascript
// 1. 导入 http 模块
const http = require('http');

// 2. 创建 web 服务器实例
const server = http.createServer()
// 3. 为服务器实例绑定 request 事件，监听客户端请求
server.on('request', (req, res) => {
    /**
     * 设置响应头信息
     * 状态码：200
     * 内容类型: 文本
     */
    res.writeHead(200, {'Content-Type': 'text/plain'})
    // 发送响应数据 "Hello World!"
    res.end('Hello World!\n');
    console.log('Someone visit our web server.');
})

// 4. 启动服务器
server.listen(8080, () => {
    console.log('Server running at http://127.0.0.1:8080');
})
```

在`server.on()`方法的回调函数的参数列表中，`req`就是Request（请求）对象，`res`就是Response（响应）对象。

---

# Request 对象

Request 对象用于获取客户端发送给服务端的请求信息。

获取请求信息（`http.createServer()`也能注册回调函数，下方演示了使用方式）：

```javascript
const { createServer } = require("http");

createServer((req, res) => {
    console.log('Request URL: ' + req.url);         // 请求URL
    console.log('Request Method: ' + req.method);   // 请求方式
}).listen(8080, () => {
    console.log('Server running at http://127.0.0.1:8080');
})
```

---

# Response 对象

Request 对象用于服务端响应给客户端数据。

Request 对象响应数据的方式是，调用`res.end()`方法。就如HTTP Demo中所示。下方展示了另外一个Demo，该Demo展示了Response对象的使用，还展示了`http.Server`对象的链式调用，并且通过给响应头设置字符集的方式解决中文乱码问题：

```javascript
const { createServer } = require('http');

var url = 'http://127.0.0.1:8080'

createServer().on('request', (req, res) => {
    // 设置响应数据类型并解决中文乱码
    res.setHeader('Content-Type', 'text/plain; charset=utf-8')
    // 发送响应数据
    res.end(`请求地址: ${url}${req.url}\n请求方式: ${req.method}`);
    // 当调用了 res.end() 之后，就表示响应已经结束了，此时再进行任何响应都是无效的
}).listen(8080, () => {
    console.log('Server running at ' + url);
})
```

---

# 根据 URL 进行响应

使用Node.js的`http`模块，为文件系统模块中[案例2](/docs/node-js/node-fs/#案例2)生成的`clock`页面搭建Web Server：

```javascript
const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer();
server.on('request', (req, res) => {
    // res.setHeader('Content-Type', 'text/html; charset=utf-8')
    let url = req.url
    // URL处理
    const DIR_NAME = 'clock'
    if (url == '/') {
        url = path.join(DIR_NAME, 'index.html')
    // 匹配以'/index'开头的URL请求
    } else if (url.startsWith('/index')) {
        url = path.join(DIR_NAME, url)
    }
    // 资源路径拼接
    const PATH = path.join(__dirname, url)
    // 读取文件
    fs.readFile(PATH, 'utf-8', (err, data) => {
        if (err) {
            // 展示错误消息
            res.end(`<h1>404 Error! ${req.url} Not Found!</h1>`)
        }
        // 发送相应数据
        res.end(data)
    })
})

server.listen(8080, () => {
    console.log('Server listen at http://127.0.0.1:8080/');
})
```
