---
# type: docs 
title: 'Node.js 入门'
linkTitle: '入门'
date: 2023-08-06T06:48:14Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
authors: Linner
categories: [note]
tags: []
images: []
navWeight: 100
# pinned: true
# pinnedWeight: 4
---

Node.js是一个基于Chrome V8引擎的JavaScript运行环境。

浏览器可以当作是JavaScript的前端运行环境；Node.js可以看作是JavaScript的后端运行环境。但是在Node.js中无法调用DOM和BOM等浏览器内置API。

Node.js的安装这里就不介绍了，推荐使用NVM来管理Node.js：[NVM安装](/blog/2023/08/nvm-安装/)。

查看Node.js的版本：

```shell
node -v
```

升级`npm`：

```shell
sudo npm install npm -g
```

---

# NPM

NPM（Node Package Manager）是 Node.js 的包管理工具，用于在 Node.js 环境中安装、管理和共享代码模块。NPM 是随同 Node.js 一起安装的，默认集成在 Node.js 安装包中。它通过命令行接口提供了一系列命令，用于管理 Node.js 应用程序所需的模块，同时也可以在前端开发中使用。

查看`npm`的版本：

```shell
npm -v
```

以下是 NPM 的一些重要功能和用途：

- 模块管理：NPM 允许开发者轻松地安装、更新和删除 Node.js 模块。
- 依赖管理：通过在项目的 `package.json` 文件中定义依赖项和版本范围，NPM 可以管理项目所需的各个模块的版本依赖关系。这样，当共享项目时，其他开发者可以执行 `npm install` 命令安装项目所需的模块及其指定的版本。
- 脚本管理：NPM 允许在项目的 `package.json` 文件中定义脚本命令，开发者可以使用 `npm run <script>` 命令来运行这些脚本。这方便了开发者在项目中自定义和组织各种构建、测试、部署等任务。
- 模块共享：NPM 是一个庞大的开源模块生态系统，开发者可以将自己编写的模块发布到 NPM 供其他开发者使用。通过 `npm publish` 命令，开发者可以将自己的模块发布到 NPM 上，并通过 `npm install` 命令在其他项目中安装和使用这些模块。

NPM模块管理命令：

- 安装模块：

    ```shell
    npm install package-name[@version]      # 本地安装
    npm install package-name[@version] -g   # 全局安装
    ```

    - 本地安装：将安装包放在 `./node_modules` 下（运行 `npm` 命令时所在的目录），如果没有 `node_modules` 目录，会在当前执行 `npm` 命令的目录下生成 `node_modules` 目录。

        本地安装的NPM包可以通过 `require()` 来引入本地安装的包。

    - 全局安装：将安装包放在 `/usr/local` 下或者 `node` 的安装目录。

        全局安装的NPM包可以直接在命令行使用。

    - `npm link`：

        在一个包文件夹内执行`npm link`可以在全局文件内创建一个指向执行`npm link`命令目录的符号链接。

        其它目录下执行 `npm link package-name` 命令，将会创建一个从全局安装的 `package-name` NPM包到当前文件内 `node_modules` 目录下的符号链接。包的名称可能有作用域前缀，如果有，`package-name` 也要对应加上。
    
    - `npm i`：

        `npm`为`npm install`命令提供了一个简便的写法，你可以像使用`npm install`命令一样使用`npm i`，例如：

        ```shell
        npm i package-name[@version]    # 本地安装
        npm i package-name[@version] -g # 全局安装
        ```

    > 如果出现以下错误：
    > 
    > ```
    > npm err! Error: connect ECONNREFUSED 127.0.0.1:8087
    > ```
    > 
    > 可以关闭代理：
    > 
    > ```shell
    > npm config set proxy null
    > ```

- 卸载模块：

    ```shell
    npm uninstall package-name
    ```

    卸载后，可以到 `/node_modules/` 目录下查看包是否还存在，或者使用以下命令查看：

    ```shell
    npm ls
    ```

- 搜索模块：

    ```shell
    npm search express
    ```

`cnpm`的用法与`npm`类似，例如安装模块可以使用`cnpm install`，卸载模块可以使用`npm uninstall`。更多`cnpm`的用法可以查看 [npmmirror 镜像站](https://npmmirror.com/)。

---

# 设置 NPM 镜像源

有两种方式可以加快`npm`下载包的速度：

- 为`npm`设置淘宝镜像源（推荐）：

    给`npm`设置淘宝镜像：

    ```shell
    npm config set registry https://registry.npmmirror.com/
    ```

    查看当前包下载服务器地址：

    ```shell
    npm config get registry
    ```

- 使用淘宝提供的`cnpm`包管理器：

    使用淘宝镜像安装`cnpm`：

    ```shell
    sudo npm install -g cnpm --registry=https://registry.npmmirror.com
    ```

`npm`镜像的提供者不止一家，如果要方便地切换多个镜像源，可以使用`nrm`镜像管理工具：

- 安装`nrm`：

    ```shell
    npm install nrm -g
    ```

- 查看所有可用的下载源：

    ```shell
    nrm ls
    ```

    结果如下所示：

    ```
      npm ---------- https://registry.npmjs.org/
      yarn --------- https://registry.yarnpkg.com/
      tencent ------ https://mirrors.cloud.tencent.com/npm/
      cnpm --------- https://r.cnpmjs.org/
    * taobao ------- https://registry.npmmirror.com/
      npmMirror ---- https://skimdb.npmjs.com/registry/
    ```

    当前正在使用的下载源前方会使用一个`*`符号来提示。

- 切换下载源：

    ```shell
    nrm use source-name
    ```

    例如切换淘宝镜像：

    ```shell
    nrm use taobao
    ```

---

# Hello Node.js

使用Node.js运行JavaScript，首先需要创建一个`.js`文件。例如`hello.js`：

```javascript
console.log("Hello Node.js!")
```

然后在终端中运行`node`命令来运行该JS文件：

```shell
node hello.js
```

---

# 导入模块

Node.js中，导入模块使用下方语法：

```javascript
const module = require('module-name')
```

---

# REPL 交互式解释器

Node.js REPL（Read Eval Print Loop，交互式解释器）是Node.js的终端交互式解释器，类似Python那样。

使用Node REPL的方法是在终端直接使用`node`命令：

```shell
$ node
> console.log("Hello Node.js!")
Hello Node.js!
undefined
> 1 + 4
5
```
