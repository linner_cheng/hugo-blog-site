---
# type: docs 
title: 'Node.js Express 路由'
linkTitle: 'Express 路由'
date: 2023-08-12T13:32:52Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
categories: [note]
authors: Linner
tags: []
images: []
navWeight: 99
---

# 路由的类型

在Express中，路由指的是客户端的请求与服务器处理函数之间的映射关系。Express中的路由分三部分组成：

```javascript
app.METHOD(PATH, HANDLER)
```

- `METHOD`：请求的类型。
- `PATH`：请求的URL地址。
- `HANDLER`：请求的处理（回调）函数。

---

# 路由的匹配过程

每当一个请求到达服务器之后，需要先经过路由的匹配，只有匹配成功之后，才会调用对应的处理函数。

在匹配时，Express会按照路由被定义的顺序进行匹配，如果请求的类型和请求的URL同时匹配成功，则Express会将这次请求，转
交给对应的函数进行处理。

关于路由的基本使用，已经在 [Express 基础](/docs/node-js/express/express-base/) 中进行了介绍。

---

# 路由的模块化使用

为了方使对路由进行模块化的管理，Express不建议将路由直接挂载到`app`上，而是推荐将路由抽离为单独的模块。

将路由抽离为单独模块的步骤如下：

1. 创建路由模块对应的 `.js` 文件。
2. 调用 `express.Router()` 函数创建路由对象。
3. 向路由对象上挂载具体的路由。
4. 使用 `module.exports` 向外共享路由对象。
5. 使用 `app.use()` 函数注册路由模块。

例如，为`user`模块创建一个路由对象：

1. 新建一个`app.js`文件，获取`app`对象，并使用`app.listen()`方法监听端口：

    ```javascript
    const express = require('express');
    const app = express()

    app.listen(80, () => {
        console.log('http://127.0.0.1/');
    })
    ```

2. 为`user`API请求创建一个`user.js`模块：

    ```javascript
    /**
     * User路由模块
     */

    const express = require('express');
    // 创建路由对象
    const router = express.Router()

    // 使用 router 对象挂载具体的路由
    router.get('/user/list', (req, res) => {
        res.send({
            status: 0,              // 自定义响应状态码，0表示成功，1表示失败
            msg: '请求成功!',       // 响应描述消息
            data: {                 // 响应给客户端的数据
                name: 'Zhangsan',
                age: '18'
            }
        })
    })

    router.post('/user/add', (req, res) => {
        res.send({
            status: 0,          // 自定义响应状态码，0表示成功，1表示失败
            msg: '请求成功!'    // 响应描述消息
        })
    })

    // 向外导出路由对象
    module.exports = router
    ```

3. 在`app.js`中，导入`user.js`模块，并使用`app.use()`函数注册`user.js`模块：

    ```javascript
    const express = require('express');
    const app = express()

    // 导入并注册路由模块
    const router = require('./user');
    /**
     * app.use() 可用来注册全局中间件
     * app.use() 还可以为路由模块添加路由前缀，例如：
     * app.use('/api', router)
     */
    app.use(router)

    app.listen(80, () => {
        console.log('Server running at http://127.0.0.1/');
    })
    ```

接着启动服务器，使用对应方式进行请求。
