---
# type: docs 
title: 'Node.js Express 模块基础'
linkTitle: 'Express 基础'
date: 2023-08-10T15:11:28Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
categories: [note]
authors: Linner
tags: []
images: []
navWeight: 100
---

Express是一个Web开发框架。Express本质上是NPM上的一个第三方模块，提供了快速创建Web服务器、API接口服务器的方法。Express中文网地址为[https://www.expressjs.com.cn/](https://www.expressjs.com.cn/)，官网地址为[https://expressjs.com/](https://expressjs.com/)。

使用Node.js提供的原生`http`模块也一样可以创建Web服务器。只不过`http`模块用起来很麻烦、开发效率低，而Express在基于`http`的基础上进行了进一步的封装，能极大提高开发效率。

安装Express 4.17.2版本：

```shell
npm i express@4.17.2
```

使用Express的基本步骤如下：

```javascript
const express = require('express');

// 创建 Web 服务器
const app = express()

// 监听 80 端口，服务器启动成功后执行回调函数
app.listen(80, () => {
    console.log('Express server running at http://127.0.0.1');
})
```

启动服务器，程序会先执行`app.listen()`方法中的回调函数。

---

# 请求处理

在Express中，请求方式并不需要自己使用Request对象去判断，可以直接调用Express提供的方法：

- `app.get()`：监听 GET 请求。

    ```javascript
    /**
     * 监听客户端 GET /user 请求
     * 参数1：客户端请求的 URL 地址
     * 参数2：请求对应的处理函数
     *      - req: Request 对象，包含了请求相关的属性与方法
     *      - res: Response 对象，包含了响应相关的属性与方法
     */
    app.get('/uer', (req, res) => {
        // 向客户端发送 JSON 对象
        res.send({
            id: req.params['id'],
            name: 'zhangsan',
            age: '20',
            gender: 'man'
        })
    })
    ```

- `app.post()`：监听 POST 请求。

    ```javascript
    /**
     * 监听客户端 POST /login 请求
     * post() 方法的参数与 get() 方法类似
     */
    app.post('/login', (req, res) => {
        // 向客户端发送文本消息
        res.send('登录成功!')
    })
    ```

在Express中，可以使用Response对象的`send()`方法向客户端响应数据，响应的数据可以是JSON、文本、HTML等等。

> 注：在调用了`res.send()`方法之后，代表业务逻辑处理完毕，所以`res.send()`方法之后不能再添加任何处理逻辑。

Express支持所有关于HTTP的请求，可以通过使用`app.METHOD()`来指定请求方式。例如DELETE请求使用`app.delete()`。

Express还支持通过`app.all()`来匹配所有请求方式。

---

# 获取请求参数

获取查询参数：

```javascript
app.post('/login', (req, res) => {
    // 使用 request.query 对象，获取查询（路径）参数
    console.log(req.query);
    // 获取具体的查询参数
    let name = req.query['name']
    let password = req.query['password']
    if (!name || !password) {
        return res.send('登录失败! 请输入用户名密码!')
    }
    if (name == 'admin' && password == '123456') {
        return res.send('登录成功!')
    }
    res.send('登录失败! 用户名或密码不正确!')
})
```

例如，使用 POST `http://localhost/login?name=admin&password=123456` 进行请求时，可以通过`req.query['name']`和`req.query['password']`获取`name=admin`和`password=123456`这两个参数。然后将获取到的参数进行判断。

获取动态参数：

```javascript
app.get('/user/:id', (req, res) => {
    // 动态参数使用 ":" + 参数名 定义
    // 使用 request.params 对象获取动态（URL）参数
    console.log(req.params);
    // 向客户端发送 JSON 对象
    res.send({
        id: req.params['id'],
        name: 'zhangsan',
        age: '20',
        gender: 'man'
    })
})
```

例如，使用 GET `http://localhost/user/1` 进行请求时，通过`req.params['id']`获取URL中这个`:id`参数。

---

# 托管静态资源

Express中使用`express.static()`和`app.use()`方法可以方便地托管静态资源。例如，托管存放静态资源的`public`目录：

```javascript
/**
 * 使用 express.static() 方法托管静态资源
 * express.static() 需要使用 app.use() 方法，并将其作为参数放入 app.use() 中
 * 在进行访问时，默认直接在根路径下请求资源，无需使用 public 作为请求路径
 * 并且 public 下的 index.html 会作为 GET / 请求时默认响应的页面
 * 注：public 目录需要与当前 .js 文件同级
 */
app.use(express.static('public'))
```

`express.static()`可以通过多次调用来托管多个静态目录：

```javascript
app.use(express.static('public'))
/**
 * 可以多次使用 express.static() 托管多个静态资源
 * 在访问静态资源时，express.static() 会根据目录的添加顺序查找所需的文件
 */
app.use(express.static('files'))
```

Express托管静态目录时，可以指定访问当前资源的路径前缀：

```javascript
/**
 * app.use() 可以使用路径前缀来访问静态资源
 * 只需要将 路径前缀 作为 参数1，将 express.static() 方法作为 参数2 即可
 */
app.use('/static', express.static('static'))
```
