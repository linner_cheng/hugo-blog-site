---
# type: docs 
title: 'Node.js 事件循环'
linkTitle: '事件循环'
date: 2023-08-06T14:46:07Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
series: ["Node.js"]
categories: [note]
authors: Linner
tags: []
images: []
navWeight: 98
---

Node.js 是单进程单线程应用程序，但是因为 V8 引擎提供的异步执行回调接口，通过这些接口可以处理大量的并发，所以性能非常高。

Node.js 几乎每一个 API 都是支持回调函数的。

Node.js 基本上所有的事件机制都是用设计模式中观察者模式实现。

Node.js 单线程类似进入一个`while(true)`的事件循环，直到没有事件观察者时退出。每个异步事件都生成一个事件观察者，如果有事件发生就调用该回调函数。

---

# 事件驱动程序

Node.js 使用事件驱动模型。在事件驱动模型中，会生成一个主循环来监听事件，当检测到事件时触发回调函数：

![事件驱动流程](uTools_1691333689230.png)

这套流程有点类似于观察者模式，事件相当于一个主题（Subject），而所有注册到这个事件上的处理函数相当于观察者（Observer）。当主题发生改变（事件触发）时，通知观察者进行更新（触发相应的回调函数）。

---

# 事件模块

Node.js 有多个内置的事件，可以通过引入 `events` 模块，并通过实例化 `EventEmitter` 类来绑定和监听事件，如：

```javascript
// 引入 events 模块
const events = require('events');

/**
 * 创建 EventEmitter 对象
 * EventEmitter可用于注册事件处理和触发事件
 */
let eventEmitter = new events.EventEmitter()

/**
 * 绑定 data_received 事件处理程序
 * eventEmitter.on() 方法：
 * - 参数1：绑定的事件名称
 * - 参数2：触发事件时执行的回调函数
 */
const DATA_RECEIVED = 'data_received' 
eventEmitter.on(DATA_RECEIVED, () => {
    console.log('Receiving Successful!');
})

// 绑定 connection 事件处理程序
const CONNECTION = 'connection' 
eventEmitter.on(CONNECTION, () => {
    console.log('Connected Successful!');

    // 触发 data_received 事件
    eventEmitter.emit(DATA_RECEIVED)
})

// 触发 connection 事件
eventEmitter.emit(CONNECTION)

console.log('Executed...');
```

执行结果如下：

```
Connected Successful!
Receiving Successful!
Executed...
```
