---
# type: docs 
title: Java 概述
linkTitle: 概述
date: 2024-06-04T06:04:04Z
tags: [Java]
draft: false
authors: Linner
navWeight: 100
categories: [note]
---

# 特点

Java 比较突出的特点：

- **面向对象**：封装，继承，多态。
- **平台无关性**（跨平台）：Java 是一种 “<u>一次编写，到处运行</u>（Write Once，Run any Where）” 的语言。也就是说，采用 Java 语言编写的程序具<u>有很好的可移植性</u>。保证 Java 可移植性的正是 Java 的虚拟机机制。
- **多线程**：Java 语言提供了多线程支持。
- **JIT**（Just-In-Time）**编译**：指的是<u>即时编译器</u>，它可以在程序运行时将字节码转换为本地机器码来提高程序运行速度。
- **编译与解释并存**：需要先将源代码编译成字节码，再解释执行字节码。

---

# JVM、JDK 和 JRE

- **JVM**（Java Virtual Machine，Java 虚拟机）：JVM 负责<u>将 Java 字节码转换为特定平台的机器码并执行</u>，是 Java <u>实现跨平台的关键</u>所在。针对不同的操作系统，有不同的 JVM 实现。
- **JRE**（Java Runtime Environment，Java 运行时环境）：JRE 包含了<u>运行 Java 程序所必需的库</u>，以及 <u>JVM</u>。
- **JDK**（Java Development Kit，Java 开发工具包）：JDK 是一套完整的 Java SDK（Software Development Kit，软件开发工具包），包括了 <u>JRE</u> 以及<u>编译器</u>（<u>`javac`</u>）、<u>Java 文档生成工具</u>（<u>Javadoc</u>）、<u>Java 调试器</u>等开发工具。为开发者提供了开发、编译、调试 Java 程序的一整套环境。

也就是说，JDK 包含了 JRE、`javac`、Javadoc、Java 调试器等；JRE 包含了 Java 库和JVM。

---

# Java 程序

Java 中主要有以下两种类型的文件：

- **源代码**：以 **`.java`** 作为后缀的文件，是开发人员编写 Java 程序的源码文件。
- **字节码**：以 **`.class`** 作为后缀的文件，是将源代码文件通过 `javac` 编译成 JVM 能理解并执行的文件。

Java 从源代码到字节码再到执行 Java 程序，其大致过程如下：

- **编译**：将源代码文件编译成 JVM 能理解并执行的字节码文件。

  源代码 $\rightarrow$ 字节码

- **解释**：JVM 执行字节码文件，并将字节码翻译成机器能识别并执行的机器码。

  字节码 $\rightarrow$ 机器码

- **执行**：在对应的机器上执行二进制机器码。

> 只需要把 Java 源代码编译成 JVM 能识别的 Java 字节码，不同的平台安装对应的 JVM，这样就可以可以实现 Java 语言的平台无关性。
