---
# type: docs 
title: Java 集合框架
linkTitle: 集合框架
date: 2024-06-04T06:51:15Z
tags: [Java]
authors: Linner
navWeight: 100
categories: [note]
---

# 概述

Java 集合框架可以分类两大类型：

- **`Collection`**：最基本的集合类型的接口，提供了添加、删除、清空等基本操作，还提供了一些对集合进行排序、二分查找、同步的静态方法。

  其主要的子接口有：
  
  - **`List`**：有序、可重复的集合。常用的实现类有：
    - **`ArrayList`**：动态数组。
    - **`LinkedList`**：链表。
  - **`Set`**：无序、不可重复的集合。常用的实现类有：
    - **`HashSet`**：哈希集合。实际上是通过一个 `HashMap` 来实现。
    - **`LinkedHashSet`**：链哈希集合。
    - **`TreeSet`**：属性集合。是在 `TreeMap` 的基础上进行实现。
  - **`Queue`**：队列。常用的实现类有：
    - **`ArrayDeque`**：双端队列。
    - **`PriorityQueue`**：优先级队列。

- **`Map`**：键值对集合类型的接口，还提供了一些对数组进行排序、打印、和对 `List` 进行转换的静态方法。

  常用的实现类有：
  
  - `HashMap`：哈希图。
  - `LinkedHashMap`：链哈希图。
  - `TreeMap`：树形哈希图。

Java 集合框架主要位于 `java.util` 包中，它们的继承关系图谱如下：

![集合类型继承关系图](1717486211257.png)
