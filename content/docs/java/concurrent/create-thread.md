---
title: Java 线程的创建
date: 2023-12-10T07:53:03Z
tags: [Java]
draft: false
authors: Linner
navWeight: 99
categories: [note]
---

Java提供了三种创建线程的方法：

- 重写`Thread`类的`run()`；
- 通过实现`Runnable`接口；
- 通过`Callable`和`FutureTask`创建线程。

---

# 通过 Thread 类创建

在Java中，每一个线程实际的创建和运行都是通过`Thread`类。`Thread`类的`run()`方法就是当前线程的入口点（入口方法）。

> 在Java中，在运行`main()`方法时，实际上是创建了一个名为`main`的线程。而`main()`方法则是当前应用程序`main`线程的入口点（入口方法）。

直接通过`Thread`类的无参构造函数创建`Thread`对象，创建出来的线程并没有任何实际的作用。也就是说它的`run()`方法并没有执行任何实际的任务。

通过`Thread`类直接创建线程，需要重写`Thread`的`run()`方法。重写方法有两种方式：

- 通过匿名内部类重写`run()`方法：

  ```java
  @Slf4j
  public class CreateThread {
    public static void main(String[] args) {
      // 匿名内部类创建线程
      Thread thread = new Thread() {
        @Override
        public void run() {
          log.debug("running...");
        }
      };

      thread.start(); // 运行线程

      log.debug("running...");
  }
  ```

  > `Thread`类的`start()`方法可以用于启动线程。当线程被创建之后，需要手动执行`start()`方法，线程才会被真正启动。
  >
  > 需要注意的是，每个`thread`对象的<u>`start()`方法仅能被执行一次</u>。如果`start()`方法被多次执行，`start()`会抛出一个`IllegalThreadStateException`异常。

  输出如下：

  ```
  16:47:40.097 [main] DEBUG CreateThread - running...
  16:47:40.097 [Thread-0] DEBUG CreateThread - running...
  ```

- 通过继承重写`run()`方法：

  ```java
  @Slf4j
  class MyThread extends Thread {
    @Override
    public void run() {
      log.debug("running...");
    }

    public static void main(String[] args) {
      new MyThread("my-thread").start();
      log.debug("running...");
    }
  }
  ```

  输出如下：

  ```
  16:47:40.097 [main] DEBUG MyThread - running...
  16:47:40.097 [Thread-0] DEBUG MyThread - running...
  ```

> PS：
> 
> 为了更好地区分各线程，可以引入`logback`：
> 
> ```xml
> <dependency>
>   <groupId>ch.qos.logback</groupId>
>   <artifactId>logback-classic</artifactId>
>   <version>1.2.3</version>
> </dependency>
> ```
>
> 接着在项目的`resources`的目录下添加一个`logback.xml`：
> 
> ```xml
> <?xml version="1.0" encoding="UTF-8"?>
> <configuration
>     xmlns="http://ch.qos.logback/xml/ns/logback"
>     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>     xsi:schemaLocation="http://ch.qos.logback/xml/ns/logback logback.xsd">
>   <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
>     <encoder>
>       <pattern>%date{HH:mm:ss} [%t] %logger - %m%n</pattern>
>     </encoder>
>   </appender>
>   <logger name="c" level="debug" additivity="false">
>     <appender-ref ref="STDOUT"/>
>   </logger>
>   <root level="ERROR">
>     <appender-ref ref="STDOUT"/>
>   </root>
> </configuration>
> ```

---

# 线程名称

通过`Thread`创建线程时，可以为线程设置名称，设置线程的名称有两种方式：

- 通过构造函数：

  ```java
  new Thread("my-thread") { // 设置线程名称
    @Override
    public void run() {
      /* ... */
    }
  }.start();
  ```

- 通过`setName()`方法：

  ```java
  Thread thread = new Thread() {
    @Override
    public void run() {
      /* ... */
    }
  };
  thread.setName("my-thread");  // 设置线程名称
  thread.start();
  ```

---

# 通过 Runnable 创建

`Runnable`是一个接口类，它只包含了`run()`这一个接口：

```java
@FunctionalInterface
public interface Runnable {
    public abstract void run();
}
```

使用`Runnable`创建`Thread`，首先需要实现`Runnable`接口，然后再将实现的`Runnable`对象传递给Thread的构造函数进行实例化。实现`Runnable`的方式如下几种：

- 通过匿名内部类实现`run()`方法。
- 通过Lambda表达式直接实现`run()`方法。

  > 由于`Runnable`中有且仅有一个接口`run()`，并且`Runnable`被`@FunctionalInterface`注解标注（标注了`@FunctionalInterface`的接口类可以直接使用Lambda来实现）。所以`Runnable`可以直接通过Lambda表达式来实现。

  示例如下：

  ```java
  // 实现 Runnable
  Runnable runnable = () -> log.debug("running...");
  // 将 Runnable 传递给 Thread 的构造函数
  new Thread(runnable).start();
  ```

- 通过创建新的类来实现`Runnable`接口。

> 当使用`Runnable`来实例化`Thread`时，可以在实例化`Thread`的同时传递一个`String`类型的参数来指定`Thread`的名称。例如：
> 
> ```java
> new Thread(runnable, "my-thread").start();
> ```

使用`Runnable`实例化`Thread`的好处是，可以实现“线程”（`Thread`）和“任务”（要执行的代码，即`Runnable`）的分离。

## Runnable 和 Thread

实际上，`Thread`实现了`Runnable`接口：

```java
public class Thread implements Runnable {
  private Runnable target;

  public Thread() {
    // 初始化
    init(null, null, "Thread-" + nextThreadNum(), 0);
    // 此时 target 为 null, run() 方法没有实际的执行代码
  }

  public Thread(Runnable target) {
    // 初始化
    init(null, target, "Thread-" + nextThreadNum(), 0);
    // init() 中会将 target 赋给成员变量, 也就是 this.target = target (下同)
  }

  public Thread(Runnable target, String name) {
    // 初始化
    init(null, target, name, 0);
  }

  @Override
  public void run() {
    if (target != null) {
      target.run();
    }
  }

  /* 省略其它... */
}
```

在`Thread`中

---

# 通过 Callable 和 FutureTask 创建

`Callable`是一个接口类，它和`Runnable`一样只包含了一个方法，并且都被`@FunctionalInterface`所修饰。只不过`Callable`需要实现的不是`run()`接口，而是`call()`接口。`Callable`的定义如下：

```java
@FunctionalInterface
public interface Callable<V> {
    V call() throws Exception;
}
```

可以看到`Callable`需要指定一个泛型`V`，而这个泛型`V`是作为`call()`接口的返回值。

`Callable`和`Runnable`一样可以使用三种方式实现（此处略）。与`Runnable`不同的是，`Callable`不是作为`Thread`构造方法的参数传入，而是作为`FutureTask`的参数传入。

通过`Callable`和`FutureTask`创建`Thread`通常包含以下步骤：

1. 实现`Callable`；
2. 通过`Callable`构造`FutureTask`；
3. 通过`FutureTask`构造`Thread`。

由于`Callable`被`@FunctionalInterface`所修饰，所以可以使用Lambda简化代码。代码示例如下：

```java
FutureTask<Integer> task = new FutureTask<>(() -> {
  log.debug("running...");
  Thread.sleep(1000);
  return 100;
});

new Thread(task).start();

// 获取线程执行结果, 等待返回结果 (阻塞)
log.debug("{}", task.get());
```

如上所示，通过`Callable`和`FutureTask`创建`Thread`的好处就是，可以使用`FutureTask`对象来获取`call()`方法的执行结果。

> `FutureTask`同样有`run()`方法。因为`FutureTask`实现了`RunnableFuture`接口：
> 
> ```java
> public class FutureTask<V> implements RunnableFuture<V> {
>   /* ... */
> }
> ```
> 
> 而`RunnableFuture`继承了`Runnable`接口。所以`FutureTask`是被作为`Runnable`的一个实现类被传入`Thread`的构造函数中。
