---
title: 'SpringMVC 基础'
date: 2023-02-20 00:00:00
tags: [Java,Spring,Maven,SpringMVC]
keywords: [SpringMVC, 请求与响应, JSON, REST, 域对象, 拦截器, 文件上传下载, 异常处理, 配置类, 转发, 重定向]
categories: [note]
navWeight: 100
authors: Linner
series: [Spring]
---

# 简介

SpringMVC是Spring下的一个用于Web表现层开发的技术。相比Servlet，SpringMVC更加高效快捷。

SpringMVC流程如下：

- 浏览器发送请求到Tomcat服务器；
- Tomcat接收请求后，将请求交给SpringMVC中的`DispatcherServlet`（前端控制器）来处理。
- `DispatcherServlet`按照对应规则将请求分发到对应的`Bean`。
- `Bean`由我们自己编写来处理不同的请求。
    每个`Bean`中可以处理一个或多个不同的请求
    URL。

> `DispatcherServlet`和`Bean`对象都需要交给Spring容器来进行管理。

综上，我们需要编写的内容为：

- `Bean`对象；
- 请求URL和`Bean`对象对应关系的配置；
- 构建Spring容器。

    将`DispatcherServlet`和`Bean`对象交给容器管理。

- 配置Tomcat服务器。

    使Tomcat能够识别Spring容器，并将请求交给容器中的`DispatcherServlet`来分发请求。

项目的基本实现步骤如下：

1. 创建Web工程（Maven结构），并在工程的`pom.xml`中添加SpringMVC和Servlet坐标。
2. 创建SpringMVC控制器类（等同于Servlet功能）。
3. 初始化SpringMVC环境（同Spring环境），设定SpringMVC加载对应的`Bean`。
4. 初始化Servlet容器，加载SpringMVC环境，并设置SpringMVC技术处理的请求。

## Spring MVC 工作流程

SpringMVC的使用过程共分两个阶段：

1. 启动服务器初始化过程；
    1. 服务器启动，执行`ServletConfig`类，初始化Web容器。
    2. 根据`getServletConfigClasses`获取所需的SpringMVC配置类（这里是`SpringMvcConfig`）来初始化SpringMVC的容器。
    3. 加载`SpringMvcConfig`配置类。
    4. 执行`@ComponentScan`加载对应的`Bean`。

        扫描指定包下所有类上的注解，将所有的Controller类（如有`@Controller`、`@RestController`等注解的类）加载到容器中。

    5. 加载每一个`Controler`。

        使用`@RequestMapping`建立请求路径与`Controler`中的方法的对应关系。

    6. 执行`getServletMappings`方法，定义所有的请求都通过SpringMVC。如：

        ```java
        protected Spring[]  getServletMappings() {
            return new Spring[]{"/"};
        }    
        ```
        
        `/`代表所拦截请求的路径规则，只有被拦截后才能交给SpringMVC来处理请求。

2. 单次请求过程。
    1. 根据请求路径发送请求。
    2. Web容器将符合设置的请求路径的请求交给SpringMVC处理。
    3. 解析请求路径。
    4. 执行匹配对应请求路径的方法。
    5. 将有`@ResponseBody`方法的返回值作为响应体返回给请求方。

---

# 项目环境及结构

## 项目结构

使用SpringMVC开发的项目结构如下：

- 📁`project-file-name`
    - 📁`src`
        - 📁`main`
            - 📁`java`
                - 📁`com.linner`
                    - 📁`config` —— 配置类
                    - 📁`controller` —— 表现层
                    - 📁`dao` —— 持久层
                    - 📁`domain` —— 实体类
                    - 📁`service` —— 业务层
            - 📁`resourcs`
            - 📁`webapp`

这种项目结构采用了SSM架构，即：

- 表现层；
- 持久层；
- 业务层。

通过IDEA创建SpringMVC项目步骤如下：

1. 创建基础的Maven-Archetype-Webapp项目。

    ![](1676947539616.png)

2. 补全项目结构：
    - 📁`src`
        - 📁`main`
            - 📁`java`
            - 📁`resourcs`
            - 📁`webapp`
3. 修改`pom.xml`文件：

    将多余的内容删除，然后添加SpringMVC所需的依赖。如：

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>

      <groupId>com.linner</groupId>
      <artifactId>springmvc-demo</artifactId>
      <version>1.0-SNAPSHOT</version>
      <packaging>war</packaging>

      <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
      </properties>

      <dependencies>

        <!-- ... -->

      </dependencies>

      <build>
        <plugins>

          <!-- ... -->

        </plugins>
      </build>
    </project>
    ```

## 基础环境

Tomcat7 Maven插件：

```xml
<plugin>
  <groupId>org.apache.tomcat.maven</groupId>
  <artifactId>tomcat7-maven-plugin</artifactId>
  <version>2.1</version>
  <configuration>
    <port>80</port>
    <path>/</path>
    <uriEncoding>UTF-8</uriEncoding>
  </configuration>
</plugin>
```

基础依赖：

```xml
<!-- Servlet -->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.1.0</version>
    <!-- Servlet需要修改作用范围，否则会与Tomcat中的servlet-api包发生冲突 -->
    <!-- provided代表的是该包只在编译和测试的时候用 -->
    <scope>provided</scope>
</dependency>
<!-- SpringMVC -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>5.2.10.RELEASE</version>
</dependency>
```

## DAO 相关依赖环境

使用SpringMVC构建Web项目，除了以上基础配置外，还需要导入其他配置。

DAO相关的坐标：

```xml
<!-- Spring JDBC -->
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-jdbc</artifactId>
  <version>5.2.10.RELEASE</version>
</dependency>
<!-- MySQL -->
<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
  <version>5.1.47</version>
</dependency>
<!-- MyBatis -->
<dependency>
  <groupId>org.mybatis</groupId>
  <artifactId>mybatis</artifactId>
  <version>3.5.6</version>
</dependency>
<!-- MyBatis Spring依赖 -->
<dependency>
  <groupId>org.mybatis</groupId>
  <artifactId>mybatis-spring</artifactId>
  <version>1.3.0</version>
</dependency>
<!-- Druid 数据库连接池 -->
<dependency>
  <groupId>com.alibaba</groupId>
  <artifactId>druid</artifactId>
  <version>1.1.16</version>
</dependency>
```

## 其它依赖环境

```xml
<!-- Jackson -->
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
  <version>2.9.0</version>
</dependency>

<!-- 日志 -->
<dependency>
  <groupId>ch.qos.logback</groupId>
  <artifactId>logback-classic</artifactId>
  <version>1.2.3</version>
</dependency>

<!-- Thymeleaf（不常用） -->
<dependency>
  <groupId>org.thymeleaf</groupId>
  <artifactId>thymeleaf-spring5</artifactId>
  <version>3.0.12.RELEASE</version>
</dependency>

<!-- 文件上传依赖 -->
<dependency>
  <groupId>commons-fileupload</groupId>
  <artifactId>commons-fileupload</artifactId>
  <version>1.3.1</version>
</dependency>

<!-- Test -->
<!-- Junit 单元测试 -->
<dependency>
  <groupId>junit</groupId>
  <artifactId>junit</artifactId>
  <version>3.8.1</version>
  <scope>test</scope>
</dependency>
```

---

# 配置

配置SpringMVC有两种方式：

- `web.xml`配置文件
- 配置类

## 配置文件方式

在`web.xml`中注册SpringMVC的前端控制器`DispatcherServlet`：

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
    <servlet>
        <servlet-name>springMVC</servlet-name>
        <!-- 指定SpringMVC前端控制器： -->
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>springMVC</servlet-name>
        <!-- 
            配对路径
            / 表示处理所有不包括.jsp的请求
            因为.jsp有自己的servlet，如果在DispatcherServlet中处理.jsp，会导致找不到相应页面，从而导致渲染失败
        -->
        <url-pattern>/</url-pattern>
    </servlet-mapping>
</webapp>
```

> `<url-pattern>`：
> 
> - `/`：表示匹配所有不包括`.jsp`的请求；
> - `/*`：能够匹配所有请求，包括`.jsp`。

扩展配置：

`web.xml`中的配置还可以定义在其它文件中，例如在`resourece`下创建新的配置文件`springMVC.xml`。然后在`web.xml`添加新配置：

```xml
<!-- 
    配置SpringMVC的前端控制器
    对浏览器发送的请求统一进行处理
 -->
<webapp>
    <servlet>
        <servlet-name>springMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!-- 
            通过初始化参数指定SpringMVC配置文件的位置和名称
         -->
        <init-param>
            <!-- contextConfigLocation为固定值 -->
            <param-name>contextConfigLocation</param-name>
            <!-- 
                使用 classpath: 表示从类路径查找配置文件，例如maven工程中的src/main/resources
             -->
            <param-value>classpath:springMVC.xml</param-value>
        </init-param>
        <!-- 
            将启动控制DispatcherServlet的初始化时间提前到服务器启动时：
        -->
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- ... -->

</webapp>
```

在`springMVC.xml`中配置：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 自动扫描包 -->
    <context:component-scan base-package="com.linner.controller"/>

    <!-- 配置Thymeleaf视图解析器（不常用） -->
    <bean id="viewResolver" class="org.thymeleaf.spring5.view.ThymeleafViewResolver">
        <property name="order" value="1"/>
        <!-- 设置编码，将编码都转为 UTF-8 -->
        <property name="characterEncoding" value="UTF-8"/>
        <property name="templateEngine">
            <bean class="org.thymeleaf.spring5.SpringTemplateEngine">
                <property name="templateResolver">
                    <bean class="org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver">
        
                        <!-- 视图前缀 -->
                        <property name="prefix" value="/WEB-INF/templates/"/>
        
                        <!-- 视图后缀 -->
                        <property name="suffix" value=".html"/>
                        <property name="templateMode" value="HTML5"/>
                        <property name="characterEncoding" value="UTF-8" />
                    </bean>
                </property>
            </bean>
        </property>
    </bean>

    <!-- 静态资源访问 -->
    <mvc:view-controller path="/" view-name="index" />

    <!-- 
        处理静态资源，例如html、js、css、jpg
        若只设置该标签，则只能访问静态资源，其他请求则无法访问
        此时必须设置<mvc:annotation-driven/>解决问题
    -->
    <mvc:default-servlet-handler/>

    <!-- 开启mvc注解驱动 -->
    <mvc:annotation-driven>
        <mvc:message-converters>
            <!-- 处理响应中文内容乱码 -->
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <property name="defaultCharset" value="UTF-8" />
                <property name="supportedMediaTypes">
                    <list>
                        <value>text/html</value>
                        <value>application/json</value>
                    </list>
                </property>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>
</beans>
```

> 在视图解析器中设置了视图前缀`<property name="prefix" ...>`和视图后缀`<property name="suffix" ...>`，那么在Controller的方法中，想要跳转到视图时，只需返回对应的视图文件名称，并去掉其后缀。例如访问`hello.html`只需返回`"hello"`，但`hello.html`必须要在对应的前缀路径中。

SpringMVC编码过滤器（必须在`web.xml`中进行注册）：

```xml
<webapp>
    <!-- 配置 SpringMVC 的编码过滤器 -->
    <filter>
        <filter-name>CharacterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceResponseEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>CharacterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <servlet>
        <!-- ... -->
    </servlet>

    <!-- ... -->
    
</webapp>
```

> SpringMVC中处理编码的过滤器一定要配置到其他过滤器之前，否则无效。

## 配置类方式

使用配置类则无需在`webapp/WEB-INF`中添加`web.xml`文件（当然也可以选择添加）。

SpringMVC项目至少需要`ServletConfig`、`SpringConfig`和`SpringMvcConfig`这三个配置类。

- ServletConfig：Spring MVC项目初始化类，也是项目的入口，作用与`web.xml类似`。
- SpringConfig：控制业务（Service）和功能（如DataSource、SqlSessionFactoryBean、 MapperScannerConfigurer等）相关的Bean。
- SpringMvcConfig（WebConfig）：加载表现层Bean（Controller）。

`ServletConfig`（Web项目入口配置类）：

```java
public class ServletConfig extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 指定Spring的配置类
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringConfig.class};
    }

    /**
     * 指定Spring MVC的配置类
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{SpringMvcConfig.class};
    }

    /**
     * 指定DispatcherServlet的映射路径，即url-pattern
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
```

> `ServletConfig`最重要的是继承`AbstractAnnotationConfigDispatcherServletInitializer`这个类，并反别重写它的三个方法。
>
> 在Servlet3.0环境中，容器会在类路径中查找实现`javax.servlet.ServletContainerlnitializer`接口的类，如果找到的话就用它来配置Servlet容器。
>
> Spring提供了这个接口的实现，名为`SpringServletContainerlnitializer`，这个类反过来又会查找实现`WebApplicationInitializer`的类并将配置的任务交给它们来完成。
>
> Spring3.2引l入了一个便利的`WebApplicationInitializer`基础实现，名为`AbstractAnnotationConfigDispatcherServletlnitializer`，当我们的类扩展了`AbstractAnnotationConfigDispatcherServletInitializer`，并将其部署到Servlet3.O容器的时候，容器会自动发现它，并用它来配置`Servlet`上下文。

`SpringConfig`（启动Tomcat服务器时加载Spring配置类）：

```java
@Configuration
@ComponentScan({"com.linner.service"})
@PropertySource("classpath:jdbc.properties")
@Import({JdbcConfig.class, MyBatisConfig.class}) // 引入其它配置
@EnableTransactionManagement    // 开启事务管理
public class SpringConfig {
}
```

> Spring需要管理的是`service`包和`dao`包。但`dao`包最终是交给`MapperScannerConfigurer`对象来进行扫描处理的。所以`SpringConfig`只需要扫描到`service`包即可。
>
> 演示`@ComponentScan`的另一种用法（排除`controller`包中的Bean）：
>
> ```java
> @ComponentScan(value="com.linner",
>   excludeFilters=@ComponentScan.Filter(
>       type = FilterType.ANNOTATION,
>       classes = Controller.class // 排除@Controller定义的Bean
>   )
> )
> ```
>
> 上面方法本质是使用`@ComponentScan`的`excludeFilters`属性设置过滤规则。
>
> - `type`：设置排除规则。
>   - `ANNOTATION`：按照注解排除。
>   - `ASSIGNABLE_TYPE`：按照指定的类型过滤。
>   - `ASPECTJ`：按照Aspectj表达式排除（基本上不会用）。
>   - `REGEX`：按照正则表达式排除。
>   - `CUSTOM`：按照自定义规则排除。
>
> `classes`：设置排除的具体注解类。

`SpringMvcConfig`

```java
@Configuration
@ComponentScan({"com.linner.controller"})
@EnableWebMvc   // MVC注解驱动
public class WebConfig implements WebMvcConfigurer {

    /**
     * 相当于 default-servlet-handler
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    /**
     * 视图控制器 view-controller
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/hello").setViewName("hello");
    }

    /**
     * 文件上传解析器
     */
    @Bean
    public MultipartResolver getMultipartResolver() {
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        return commonsMultipartResolver;
    }

    /**
     * 异常处理解析器
     */
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();
        Properties prop = new Properties();
        prop.setProperty("java.lang.ArithmeticException", "error");
        exceptionResolver.setExceptionMappings(prop);
        // 可以不设置，默认键即为 exception
        exceptionResolver.setExceptionAttribute("exception");
        exceptionResolvers.add(exceptionResolver);
    }

    /**
     * 模板解析器
     */
    @Bean
    public ITemplateResolver getTemplateResolver() {

        // 获取当前的 WebApplicationContext
        WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
        // 使用WebApplicationContext获取ServletContext，并构造ServletContextTemplateResolver
        ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(
                webApplicationContext.getServletContext());

        // 设置视图前缀
        templateResolver.setPrefix("/WEB-INF/templates/");
        // 设置视图后缀
        templateResolver.setSuffix(".html");

        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        return templateResolver;
    }

    /**
     * 模板引擎
     * @param templateResolver 模板解析器（自动装配，@Autowired可忽略不写）
     */
    @Bean
    public SpringTemplateEngine getTemplateEngine(ITemplateResolver templateResolver) {

        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        return templateEngine;
    }

    /**
     * 设置视图解析器
     * @param templateEngine 模板引擎（自动装配）
     */
    @Bean
    public ViewResolver getViewResolver(SpringTemplateEngine templateEngine) {

        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setCharacterEncoding("UTF-8");
        viewResolver.setTemplateEngine(templateEngine);

        return viewResolver;
    }
}
```

> 关于Spring MVC的配置类，除了扫描组件和配置模板解析器外，Spring提供了两种方式来配置，一种是实现`WebMvcConfigurer`接口，另一种是继承`WebMvcConfigurationSupport`。由于Java 8的接口中提供了`default`关键字来修饰接口方法，使得接口可以存在默认的实现，所以使用`WebMvcConfigurer`接口也不必实现所有接口。而在SpringBoot 2中使用`WebMvcConfigurationSupport`有可能会导致SpringBoot的自动配置不生效，并且在Spring MVC中使用`WebMvcConfigurationSupport`也可能导致配置类不生效，所以我个人<b><u>推荐使用`WebMvcConfigurer`</u></b>。
>
> 如果你使用`WebMvcConfigurationSupport`后发现拦截器等配置不生效，那么可以尝试实现`WebMvcConfigurer`接口来解决问题。

DAO相关配置类：

`jdbc.properties`（数据库配置，放在项目中`resources`目录下）：

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql:///spring_db?useSSL=false&characterEncoding=utf-8
jdbc.username=root
jdbc.password=123456
```

`JdbcConfig`：

```java
public class JdbcConfig {
    
    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    @Bean
    public DataSource dataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource){
        DataSourceTransactionManager ds = new DataSourceTransactionManager();
        ds.setDataSource(dataSource);
        return ds;
    }
}
```

`MyBatisConfig`：

```java
public class MyBatisConfig {
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource){
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setTypeAliasesPackage("com.linner.domain");
        return factoryBean;
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer(){
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        msc.setBasePackage("com.linner.dao");
        return msc;
    }
}
```

---

# 使用过滤器转换编码

在`ServletConfig`中重写`AbstractAnnotationConfigDispatcherServletInitializer`的`getServletFilters()`来注册过滤器：

```java
/**
 * 注册过滤器
 */
@Override
protected Filter[] getServletFilters() {
    
    // 设置编码，将编码都转为 UTF-8
    CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
    characterEncodingFilter.setEncoding("UTF-8");
    characterEncodingFilter.setForceEncoding(true);

    HiddenHttpMethodFilter hiddenHttpMethodFilter = new HiddenHttpMethodFilter();

    return new Filter[]{characterEncodingFilter, hiddenHttpMethodFilter};
}
```

---

# 请求与响应

SpringMVC是Web层的框架，主要作用是接收请求、接收数据、响应结果。

编写Controller只需要在Controller类上使用`@Controller`注解即可。

## 请求映射 RequestMapping

映射请求路径使用`@RequestMapping`注解。注解中的使用`value`属性指定映射的请求路径（由于是`value`属性，所以当注解中无需指定其它参数时，可以省略）。

`@RequestMapping`可以分别作用在类和方法上：

```java
@Controller
@RequestMapping("/user")
public class UserController {
    @RequestMapping(value = "/helloSpring")
    @ResponseBody
    public String helloSpring() {
        return "Hello Spring!";
    }

    @RequestMapping("/helloWorld")
    @ResponseBody
    public String helloWorld() {
        return "Hello World!";
    }
}
```

- 方法上定义的`@RequestMapping`是具体的请求方式，包括请求路径和请求方式。

    即，如果在方法上使用了`@RequestMapping`并且没有在类上使用`@RequestMapping`，那么该方法的请求路径即为方法上`@RequestMapping`中的值。

- 类上定义的`@RequestMapping`是请求目录。

    即，如果在方法和类上均使用了`@RequestMapping`，那么该方法的请求路径需要加上类`@RequestMapping`注解中定义的目录。例如上方代码中的请求路径为`/user/helloSpring`和`/user/helloWorld`。

`value`属性是一个字符串数组，可以通过以下方式来指定多个请求路径：

```java
@RequestMapping({"hello", "helloWorld"})
```

`@RequestMapping`除了`value`属性外，还有`method`属性。`method`属性是用来指定请求方式的，如：`@RequestMapping(method = RequestMethod.POST)`（匹配POST请求方式）。

> 在客户端向服务器发送请求时，`DispatcherServlet`会首先根据`@RequestMapping`获取对应的控制器方法。

## 接收 Query 参数

Query参数，也就是拼接在请求路径后面，以`?`开始，使用`&`分隔每个参数项的参数传递方式。例如：

```
http://localhost/user/hello?name=张三
```

接收Query参数，GET和POST请求的编写方式一致。

如果要使用不同的方式接收请求，可以修改`@RequestMapping`注解：

```java
@RequestMapping(value = "/save", method = RequestMethod.GET)
```

### 普通参数

```java
@Controller
@RequestMapping("/user")
public class UserController {

    // 使用 [http://localhost/user/hello?name=张三] 访问
    // 返回响应体 Hello 张三!
    @RequestMapping("/hello")
    @ResponseBody
    public String hello(String name) {
        return "Hello " + name + "!";
    }

    // 使用 [http://localhost/user/login?name=张三&password=abc] 访问
    // 返回响应体 OK，终端输出 userName=张三; password=abc
    @RequestMapping("/login")
    @ResponseBody
    public String login(@RequestParam("name") String userName, String password) {
        System.out.println("userName=" + userName + "; password=" + password);
        return "OK";
    }

}
```

- 请求参数：

    <u>定义相同的Query参数名与方法形参变量名即可接收参数。</u>

    <u>如果想要形参名与Query参数名不同则可使用`@RequestParam`注解定义Query参数名。</u>

    如果有多个请求参数则定义多个方法参数。

- 返回值：

    返回值使用`@ResponseBody`注解后直接在方法中使用`return`返回。这里注解`@ResponseBody`是指将返回值作为响应体。

### POJO类型参数

使用POJO类型接收参数，只需要让请求参数名与形参对象属性名相同即可。如果有嵌套的POJO参数，请求参数名与形参对象属性名相同，然后按照对象层次结构关系即可接收。

定义一个POJO类：

```java
public class Address {
    private String province;
    private String city;

    // setter、getter and toString...
}
```

```java
public class User {
    private Integer id;
    private String name;
    private String password;
    private Address address;

    // setter、getter and toString...
}
```

Controller：

```java
@Controller
@RequestMapping("/user")
public class UserController {

    // 访问 [http://localhost/user/login?id=123&password=abc&name=张三&address.province=广东&address.city=广州]
    // 返回响应体 OK，终端输出 Login: User{id=123, name='张三', password='abc', address=Address{province='广东', city='广州'}}
    @RequestMapping("/login")
    @ResponseBody
    public String login(User user) {
        System.out.println("Login: " + user);
        return "OK";
    }

}
```

### 数组、集合类型参数

接收数组参数只需让请求参数名与形参名相同且请求参数为多个即可（形参为数组类型）。

```java
@Controller
@RequestMapping("/user")
public class UserController {

    // 访问 [http://localhost/user/setHobbies?id=123&hobbies=唱歌,跳舞,Rap,打篮球]
    // 返回响应体 OK，终端输出 User 123's hobbyies: [唱歌, 跳舞, Rap, 打篮球]
    @RequestMapping("/setHobbies")
    @ResponseBody
    public String setHobbies(Integer id, String[] hobbies) {
        System.out.println("User " + id + "'s hobbies: " + Arrays.toString(hobbies));
        return "OK";
    }

}
```

使用集合类型形参接收参数，使用方式与数组不同（会报错）。因为SpringMVC将List看做是一个POJO对象来处理，将其创建一个对象并准备把前端的数据封装到对象中，但是List是一个接口无法创建对象。

使用集合类型形参接收参数需要使用`@RequestParam`绑定参数关系。

```java
@Controller
@RequestMapping("/user")
public class UserController {

    // 访问方式与数组形式相同，响应体与终端输出也相同
    @RequestMapping("/setHobbies")
    @ResponseBody
    public String setHobbies(Integer id, @RequestParam List<String> hobbies) {
        System.out.println("User " + id + "'s hobbies: " + hobbies);
        return "OK";
    }

}
```

### 日期格式

使用`@DateTimeFormat`可以设置参数的日期格式，如：

```java
@Controller
@RequestMapping("/user")
public class UserController {

    // URL: http://localhost/user/setBirthday?id=123&birthday=2023/1/1 18:23:40
    @RequestMapping("/setBirthday")
    @ResponseBody
    public String setBirthday(Integer id, @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date birthday) {
        System.out.println("User " + id + "'s birthday is " + birthday);
        return "OK";
    }
    
}
```


## JSON 数据参数

使用JSON传输需要添加相应依赖。SpringMVC默认使用的是`jackson`来处理json的转换：

```xml
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.9.0</version>
</dependency>
```

在配置类中添加`@EnableWebMvc`注解来开启JSON数据类型自动转换：

```java
@Configuration
@ComponentScan("com.linner.controller")
@EnableWebMvc // 开启JSON数据类型自动转换
public class SpringMvcConfig {
}
```

使用JSON传输数据只需要在形参前添加`@ResponseBody`注解来将外部传递的JSON数据映射到形参到对象中：

```java
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/login")
    @ResponseBody
    public String login(@RequestBody User user) {
        System.out.println("Login: " + user);
        return "OK";
    }

    @RequestMapping("/setHobbies")
    @ResponseBody
    public String setHobbies(Integer id, @RequestBody List<String> /* 也可以使用 String[] */ hobbies) {
        System.out.println("User " + id + "'s hobbies: " + hobbies);
        return "OK";
    }

}
```

- `login()`：

    URL：`http://localhost/user/login`

    JSON：

    ```json
    {
        "id": 123,
        "name": "张三",
        "password": "123",
        "address": {
            "province": "广东",
            "city": "广州"
        }
    } 
    ```

- `setHobbies`：

    URL：`http://localhost/user/setHobbies2?id=123`

    JSON：

    ```json
    ["唱歌", "跳舞", "Rap", "打篮球"]
    ```

## 响应 JSON 数据

响应JSON数据需要依赖于`@ResponseBody`（在Controller中）和`@EnableWebMvc`（在配置类中）注解。将返回值设置为实体类对象，设置返回值类型为实体类类型，即可实现返回对应对象的JSON数据：

实体类：

```java
public class Book {
    private Integer id;
    private String name;
    // constructor、setter、getter and toString ...
}
```

Controller:

```java
@Controller
@RequestMapping("/books")
public class BookController {

    @RequestMapping("/search")
    @ResponseBody
    public Book search(int id) {
        return new Book(id, "Hello SpringMVC");
    }

    @RequestMapping("/searchName")
    @ResponseBody
    public List<Book> searchName(String name) {
        System.out.println("Search the book " + name);
        List books = new ArrayList<Book>();
        for (int i = 0; i < 5; i++) {
            books.add(new Book(i, name + Integer.toString(i)));
        }
        return books;
    }

}
```

- `searchName()`：

    URL：`http://localhost/books/searchName?name=SpringMVC`
    返回JOSN：

    ```json
    [
        {
            "id": 0,
            "name": "SpringMVC0"
        },
        {
            "id": 1,
            "name": "SpringMVC1"
        },
        {
            "id": 2,
            "name": "SpringMVC2"
        },
        {
            "id": 3,
            "name": "SpringMVC3"
        },
        {
            "id": 4,
            "name": "SpringMVC4"
        }
    ]
    ```

- `search()`：

    URL：`http://localhost/books/search?id=123`
    返回JSON：

    ```json
    {
        "id": 123,
        "name": "SpringMVC"
    }
    ```

## Ant 风格路径

在Ant风格中，定义了以下几种符号：

- `?`：表示匹配请求资源目录中的<u>任意单个字符</u>。
- `*`：表示匹配请求资源目录中的<u>任意0个或多个字符</u>。
- `/**/`：表示匹配请求路径中任意一层或多层目录。例如`/**/user`可以匹配`/abc/user`、`/abc/def/user`等等。

## REST 风格

REST（Representational State Transfer，表现形式状态转换），是一种软件架构风格。REST的优点有：

- 隐藏资源的访问行为，无法通过地址得知对资源是何种操作。
- 简化书写。

按照REST风格访问资源时使用行为动作区分对资源进行了何种操作：

- GET：查询；
- POST：新增。
- PUT：修改。
- DELETE：删除。

如：

- `http://localhost/users` —— GET：

    查询全部用户信息（查询）。

- `http://localhost/users/1` —— GET：查询指定用户（id为1）信息（查询）。
- `http://localhost/users` —— POST：添加用户信息（新增/保存）。
- `http://localhost/users` —— PUT：修改用户信息（修改/更新）。
- `http://localhost/users/1` —— DELETE：删除用户信息（删除）。

> 描述模块的名称通常使用复数，表示此类资源，而非单个资源。

Example：

```java
@Controller
@RequestMapping("/users")
public class UserController {

    /**
     * 添加用户
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String save(@RequestBody User user) {
        return "OK";
    }

    /**
     * 删除用户
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable int id) {
        return "OK";
    }

    /**
     * 修改用户
     */
    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public String update(@RequestBody User user) {
        return "OK";
    }

    /**
     * 根据用户id查询
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String searchById(@PathVariable int id) {
        return "OK";
    }

    /**
     * 查询所有用户
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String searchAll() {
        return "OK";
    }
}
```

上方Controller中每个方法的`@RequestMapping`中都使用了`method`元素来确定请求方式。并且根据需要`save()`和`update()`都接收一个JSON数据。

`delete()`和`searchById()`都使用了路径参数（`value = "\{id}"`）。指定路径参数后，需要在方法参数列表中添加名称相同的参数，并且用`@PathVariable`注解（`public String delete(@PathVariable int id)`）。

路径参数可以定义多个，如：

```java
@RequestMapping(value = "/{id}/{name}", method = RequestMethod.GET)
@ResponseBody
public String searchById(@PathVariable int id, @PathVariable String name) {
    return "OK";
}
```

如果想要路径参数名与形参名不同，需要在`@PathVariable`中注明对应关系，如：

```java
@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
@ResponseBody
public String delete(@PathVariable("id") int userId) {
    return "OK";
}
```

### RESTful 快速开发

- 使用`@RestController`注解：

    将`@ResponseBody`注解提到类上，让所有的方法都有`@ResponseBody`的功能。
    `@RestController`注解正好相当于`@Controller`加上`@ResponseBody`的功能，所以可以使用`@RestController`替代它们。

- 使用`@GetMapping`、`@PostMapping`、`@PutMapping`、`@DeleteMapping`等替代`@RequestMapping`。

    例如`@GetMapping`就相当于`RequestMapping(method = RequestMethod.GET)`。

Example：

```java
@RestController
@RequestMapping("/books")
public class BookController {

    /**
     * 搜索全部书籍
     */
    @GetMapping
    public String getAll() {
        return "All Books";
    }

    /**
     * 搜索图书
     */
    @GetMapping("/{id}")
    public String getById(@PathVariable int id) {
        return "Get by id " + id;
    }

    /**
     * 保存图书
     */
    @PostMapping
    public String save(@RequestBody Book book) {
        return "Save " + book.toString();
    }

    /**
     * 修改图书
     */
    @PutMapping
    public String update(@RequestBody Book book) {
        return "Update " + book.toString();
    }

    /**
     * 删除图书
     */
    @DeleteMapping("/{id}")
    public String delete(@PathVariable int id) {
        return "Delete by id" + id;
    }

}
```

## Params 请求参数映射匹配

`params`是`@RequestMapping`中的一个属性，该属性通过请求的请求参数匹配请求映射。

用法如下：

- `"param"`：请求映射所匹配的请求必须携带`param`这个请求参数。

    例如：

    ```java
    @RequestMapping(value = {"login"}, params = "username")
    ```

    请求路径示例：

    ```        
    http://localhost/login?username=zhangsan
    ```

- `"!param"`：否定匹配，请求映射所匹配的请求必须不能携带`param`请求参数。

    例如：

    ```java
    @RequestMapping(value = {"login"}, params = "!username")
    ```

    如果请求路径中包含参数`username`将不会匹配到该`@RequestMapping`。

- `"param=value"`：等值匹配，请求映射所匹配的请求必须携带`param`请求参数，且`param`参数的值必须为`value`。

    例如：

    ```java
    @RequestMapping(value = {"login"}, params = "username=admin")
    ```

    请求路径为：

    ```
    http://localhost/login?username=admin
    ```

- `"param!=value"`：非值匹配，请求映射所匹配的请求必须携带`param`请求参数，但`param`的值不能为`value`。

    例如：

    ```java
    @RequestMapping(value = {"login"}, params = "username!=admin")
    ```

    如果请求路径携带参数`username`且值为`admin`则匹配失败。

- `{expression1[, expression2[, ...]]}`：

    `params`是字符串数组类型，可以指定多个参数规则，其中`expression`可以是以上任何类型的字符串表达式中的任何一种。

`params`只对其指定的参数有要求，没有被其指定的参数并没有任何限制。

如果请求的路径和方式都满足`@RequestMapping`，但是与`params`指定的规则不付，如果此时没有其它映射来匹配这个请求，服务器会返回给浏览器`400`错误。

## headers 请求头匹配

`headers`是`@RequestMapping`的属性，它指定请求的请求头信息匹配规则。

`headers`的字符串表达式语法与`params`十分相似：

- `"header"`：请求映射所匹配的请求必须携带`header`请求头信息。
- `"!header"`：请求映射所匹配的请求必须不能携带`header`请求头信息。
- `"header=value"`：请求映射所匹配的请求必须携带`header`请求头信息且`header=value`。

    请求头中使用的是`:`分隔开的键值对，`:`左边是键（即上述中的`header`），右边是值（即上述中的`value`）。只需要将`:`替换为`=`即是对应的`headers`字符串表达式。

- `"header!=value"`：要求请求映射所匹配的请求必须携带`header`请求头信息且`header!=value`。
- `{expression1[, expression2[, ...]]}`：`headers`属性同样是字符串数组类型，可以在`{}`中使用上述任何表达式。

当`@RequestMapping`其它条件都满足，除了`headers`时，服务器会返回`404`错误。

---

# 域对象共享数据

`request`域对象共享数据的常用方式大致有5种：

1. 通过`ServletAPI`获取（不建议使用）。即通过`ServletRequest`或`HttpServletRequest`对象获取`request`域。
2. 通过`ModelAndView`获取。
3. 通过`Model`获取。
4. 通过`Map<String, Object>`获取。
5. 通过`ModelMap`获取。

## ServletAPI

获取`request`域对象共享数据的方式之一就是使用`ServletAPI`。即，在Controller对象中的映射方法中，添加一个`ServletRequest`或`HttpServletRequest`对象参数。

例如：

```java
@RequestMapping("/testServletAPI")
@ResponseBody
public String testServletAPI(HttpServletRequest request) {
    request.setAttribute("testScope", "Hello ServletAPI!");
    return testRequestScope;
}
```

> 获取`response`也是类似的方法。在参数列表中指定一个`ServletResponse`或`HttpServletResponse`对象参数。

## Model

`Model`是SpringMVC提供的专用于共享`request`域对象数据。

使用`Model`的方式与使用`ServletAPI`类似，在形参列表中指定一个`Model`类型的参数即可。

```java
@RequestMapping("/testModel")
@ResponseBody
public String testModel(Model model) {
    // 写入
    model.addAttribute("testRequestScope", "Hello Model!");
    // 读取
    String testRequestScope = (String) model.getAttribute("testRequestScope");
    System.out.println(testRequestScope);

    return testRequestScope;
}
```

## Map String Object

使用`Map<String, Object>`共享`request`域对象数据，使用方式也是在形参列表中定义一个`Map<String, Object>`类型形参。

```java
@RequestMapping("/testMap")
@ResponseBody
public String testMap(Map<String, Object> map) {
    // 写入
    map.put("testRequestScope", "Hello Map!");
    // 读取
    String testRequestScope = (String) map.get("testRequestScope");
    System.out.println(testRequestScope);
    
    return testRequestScope;
}
```

## ModelMap

`ModelMap`的用法与`Map`十分类似。

```java
@RequestMapping("/testModelMap")
@ResponseBody
public String testModelMap(ModelMap modelMap) {
    // 写入
    modelMap.addAttribute("testRequestScope", "Hello ModelMap!");
    // 读取
    String testRequestScope = (String) modelMap.getAttribute("testRequestScope");
    System.out.println(testRequestScope);
    
    return testRequestScope;
}
```

## ModelAndView

`ModelAndView`是SpringMVC提供的用于共享`request`域对象数据和视图解析跳转的API。

通过`ModelAndView`共享`request`域数据，无需在形参中指定该类型参数，只需在方法中`new`一个即可。但是使用`ModelAndView`需要将该类型对象作为返回值返回。

除了使用原生`ServletAPI`之外，使用其它方法（如上`Map`、`Model`、`ModelMap`等）共享`request`域数据，最终SpringMVC都会将模型数据和视图封装到`ModelAndView`中。

```java
@RequestMapping("/testModelAndView")
public ModelAndView testModelAndView() {
    ModelAndView mav = new ModelAndView();
    // 处理模型数据，即向请求域request共享数据
    mav.addObject("testRequestScope", "Hello ModelAndView!");
    // 设置视图名称（返回视图需要有对应的页面）
    mav.setViewName("success");
    // 读取数据（第一次写入后可能读取失败，因为只有在方法执行完毕后才能真正写入）
    String testRequestScope = (String) mav.getModel().get("testRequestScope");
    System.out.println(testRequestScope);

    return mav;
}
```

`success.html`（在`Webapp/WEB-INF/templates/`下创建）：

```html
<!DOCTYPE html>
<html lang="zh">
    <head>
        <meta charset="UTF-8">
        <title>Success!</title>
    </head>
    <body>
        <h1>Success!</h1>
    </body>
</html>
```

> 其实`ModelAndView`也可以作为Controller方法的参数使用。
>
> 需要注意的是，使用`ModelAndView`设置视图对象无论是否使用`@ResponseBody`，返回的始终是视图。

## Map、Model 和 ModelMap

在使用`Map<String, Object>`、`Model`和`ModelMap`时SpringMVC传入的都是`BindingAwareModelMap`类型对象。

- `Model`是一个接口，它定义了`addAllAttributes()`、`getAttribute()`等接口方法。
- `ModelMap`继承了`LinkedHashMap<String, Object>`，所以它也是属于`Map`的子类。`ModelMap`给出了`addAllAttributes()`、`getAttribute()`等接口的实现。
- `BindingAwareModelMap`继承自`ExtendedModelMap`类，而`ExtendedModelMap`又是`ModelMap`的子类和`Model`接口的实现。

    ```java
    public class ExtendedModelMap extends ModelMap implements Model { 
        /* ... */ 
    }
    ```

    `BindingAwareModelMap`重写了`Map`的`put()`和`putAll()`，使得它能作为`Map`来读写`request`域。

综上，`BindingAwareModelMap`可以作为`Model`、`Map<String, Object>`和`ModelMap`传入Controller方法中。

在浏览器发送请求后，实际上调用Controller中对应方法的是`DispatcherServlet`中的`doDispatch()`：

```java
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {

    /* ... */

    try {
        ModelAndView mv = null;
        /* ... */

        try {

            /* ... */
            
            // Actually invoke the handler.（实际的请求处理者）
            mv = ha.handle(processedRequest, response, mappedHandler.getHandler());
            // 最后返回一个封装好的ModelAndView对象
            
            /* ... */
            
        }
        
        /* ... */

        processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);

    }/* ... */

    /* ... */

}
```

> 通过断点调试最后发现，无论是`Map`、`Model`、`ModelMap`还是`ModelAndView`，它们最后都会被封装为`ModelAndView`（即使Controller方法返回的是`ModelAndView`，Controller方法中的`ModelAndView`和`DispatcherServlet`对象的`doDispatch()`中的`ModelAndView`地址也并不相同）。

> 在`processDispatchResult()`中调用了下方所示方法来进行视图渲染：
> 
> ```java
> render(mv, request, response);
> ```

## 向 Session 域共享数据

使用原生`ServletAPI`共享：

```java
@RequestMapping("/testSession")
@ResponseBody
public String testSession(HttpSession session) {
    // 写入
    session.setAttribute("testSessionScope", "Hello Session!");
    // 读取
    String testSessionScope = (String) session.getAttribute("testSessionScope");
    System.out.println(testSessionScope);
    return testSessionScope;
}
```

Session可以从Request域中获取：

```java
@RequestMapping("/testSessionInServlet")
@ResponseBody
public String testSession(HttpServletRequest request) {
    // 从Request域中获取Session
    HttpSession session = request.getSession();
    // 写入
    session.setAttribute("testSessionScope", "Hello Session In Servlet!");
    // 读取
    String testSessionScope = (String) session.getAttribute("testSessionScope");
    System.out.println(testSessionScope);
    return testSessionScope;
}
```

## 向 Application 域共享数据

通过Session域获取`ServletContext`：

```java
@RequestMapping("/testApplication")
@ResponseBody
public String testApplication(HttpSession session) {
    // 从Session域中获取ServletContext
    ServletContext application = session.getServletContext();
    // 写入
    application.setAttribute("testSessionScope", "Hello Application!");
    // 读取
    String testSessionScope = (String) application.getAttribute("testSessionScope");
    System.out.println(testSessionScope);

    return testSessionScope;
}
```

通过Request域获取`ServletContext`（不推荐）：

```java
@RequestMapping("/testApplicationInServlet")
@ResponseBody
public String testApplication(HttpServletRequest request) {
    // 从Request域中获取ServletContext
    ServletContext application = request.getServletContext();
    // 写入
    application.setAttribute("testSessionScope", "Hello Application In Servlet !");
    // 读取
    String testSessionScope = (String) application.getAttribute("testSessionScope");
    System.out.println(testSessionScope);

    return testSessionScope;
}
```

---

# 请求报文转换

`HttpMessageConverter`即报文信息转换器，能将请求报文转换为Java对象，或将Java对象转换为响应报文

`HttpMessageConverter`提供了两个注解和两个类型：

- `@ResponseBody`：即上方Controller方法中使用的将返回值（Java对象）作为响应体发送给浏览器的注解。
- `ResponseEntity`：可以作为Controller方法的返回值返回，并响应给浏览器。
- `@RequestBody`：将Controller方法形参指定为请求体，并接收从浏览器发送过来的请求体。

    ```java
    @PostMapping("/testRequestBody")
    @ResponseBody
    public String testRequestBody(@RequestBody String requestBody) {
        requestBody = "RequestBody{'" + requestBody + "'}";
        return requestBody;
    }
    ```

- `RequestEntity`：是封装请求报文的一种类型，在Controller方法形参中使用，它获得的是<u>整个请求报文</u>。

    ```java
    @RequestMapping("/testRequestEntity")
    @ResponseBody
    public String testRequestEntity(RequestEntity<String> requestEntity) {
        HttpHeaders headers = requestEntity.getHeaders();
        String body = requestEntity.getBody();
        String response = "RequestEntity{" + headers +
                "}\nRequestBody{'" + body + "'}";
        System.out.println(response);

        return response;
    }
    ```

往响应体写入信息还有一个方法，就是使用原生`ServletAPI`：

```java
@RequestMapping("/testResponse")
public void testResponse(HttpServletResponse response) throws IOException {
    response.getWriter().print("Hello Response!");
}
```

---

# 文件上传下载

```xml
<dependency>
  <groupId>commons-fileupload</groupId>
  <artifactId>commons-fileupload</artifactId>
  <version>1.3.1</version>
</dependency>
```

## ResponseEntity 实现下载

将`ResponseEntity<byte[]>`作为返回值，在其中设置好对应的响应头、响应体和状态码。

```java
@RequestMapping("/testDown")
public ResponseEntity<byte[]> testResponseEntity(HttpSession session) throws IOException {
    // 获取ServletContext对象
    ServletContext servletContext = session.getServletContext();
    // 获取服务器中文件的真实路径 getRealPath()如果不带参数的话获取的是服务器的部署路径
    String realPath = servletContext.getRealPath("/static/img/test.png");
    System.out.println("RealPath: " + realPath);
    // 创建输入流
    FileInputStream is = new FileInputStream(realPath);
    // 创建字节数组
    byte[] bytes = new byte[is.available()];
    // 将流写到字节数组中
    is.read(bytes);
    // 创建HttpHeaders对象设置响应头信息
    MultiValueMap<String, String> headers = new HttpHeaders();
    // 设置下载方式以及下载文件的名字
    headers.add("Content-Disposition", "attachment;filename=test.png");
    // 设置响应状态码
    HttpStatus statusCode = HttpStatus.OK;
    // 创建ResponseEntity对象
    ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(bytes, headers, statusCode);
    // 关闭输入流
    is.close();

    return responseEntity;
}
```

在下载文件之前，需要通过Session获取当前要下载文件的真实路径：

```java
ServletContext servletContext = session.getServletContext();
String realPath = servletContext.getRealPath("/static/img/test.png");
```

获取了真实路径后，通过`FileInputStream`将文件读取并写入字节数组`byte[]`中：

```java
FileInputStream is = new FileInputStream(realPath);
byte[] bytes = new byte[is.available()];
is.read(bytes);
is.close();
```

下载文件时，需要在响应头中设置下载方式以及文件名：

```java
MultiValueMap<String, String> headers = new HttpHeaders();
// 以附件方式下载文件，并且默认文件名为test.png
headers.add("Content-Disposition", "attachment;filename=test.png");
```

最后将字节数组作为响应体，再加上响应头和状态码等信息，创建`ResponseEntity`：

```java
ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(bytes, headers, statusCode);
return responseEntity;
```

## 文件上传

文件上传依赖：

```xml
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.3.1</version>
</dependency>
```

在`springMVC.xml`中配置文件上传解析器：

```xml
<!-- 配置文件上传解析器，将上传的文件封装为MultipartFile -->
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver"/>
```

文件上传实现：

```java
@PostMapping("/testUp")
@ResponseBody
public String testUp(MultipartFile photo, HttpSession session) throws IOException {
    // 获取上传文件的文件名
    String fileName = photo.getOriginalFilename();
    // 获取上传文件的后缀名
    String suffixName = fileName.substring(fileName.lastIndexOf("."));
    // 将UUID作为文件名
    String uuid = UUID.randomUUID().toString();
    // 将UUID和后缀名拼接后的结果作为最终的文件名
    fileName = uuid + suffixName;
    // 需要通过 ServletContext 获取服务器中 photo 目录的路径
    ServletContext servletContext = session.getServletContext();
    // photo 其实也可以用 photo.getName() 替代
    String photoPath = servletContext.getRealPath("photo");
    File file = new File(photoPath);
    // 判断photoPath所对应路径是否存在
    if (!file.exists()) {
        // 若不存在，则创建目录
        file.mkdir();
    }
    String finalPath = photoPath + File.separator + fileName;
    photo.transferTo(new File(finalPath));
    return fileName + " OK!";
}
```

在上传文件时，需要考虑文件之间文件名的冲突问题，可以通过将文件名改为UUID解决：

```java
// 获取上传文件的文件名
String fileName = photo.getOriginalFilename();
// 获取上传文件的后缀名
String suffixName = fileName.substring(fileName.lastIndexOf("."));
// 将UUID作为文件名
String uuid = UUID.randomUUID().toString();
// 将UUID和后缀名拼接后的结果作为最终的文件名
fileName = uuid + suffixName;
```

---

# 拦截器

拦截器（Interceptor）是一种动态拦截方法调用的机制，在SpringMVC中动态拦截控制器方法的执行。拦截器可以在指定的发那个发调用前后执行预先设定的代码，可以阻止原始方法的执行。拦截器和过滤器在作用和执行顺序上很相似。它们的关系如下图所示：

![拦截器和过滤器的关系](1630676903190.png)

创建拦截器类：

```java
@Component  // 由SpringMVC来管理
public class BookInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("preHandle...");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle...");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("afterCompletion...");
    }
}
```

## 拦截器配置类

编写拦截器配置类有两种方式：

- 实现`WebMvcConfigurer`接口；
- 继承`WebMvcConfigurationSupport`类并重写方法。

```java
@Configuration
// 扫描interceptor包
@ComponentScan("com.linner.interceptor")
public class SpringMvcSupport extends WebMvcConfigurationSupport {
    @Autowired  // 自动装配
    private BookInterceptor bookInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        /*
            添加（声明）拦截器并配置拦截规则
            可以同时配置多个规则
            如果不添加拦截规则，默认拦截所有请求
        */
        registry.addInterceptor(bookInterceptor).addPathPatterns("/books", "/books/*");
    }
}
```

让SpringMVC扫描到拦截器的配置类：

```java
@Configuration
@ComponentScan({"com.linner.controller", "com.linner.config"})
@EnableWebMvc
public class SpringMvcConfig {}
```

拦截器的配置类`SpingMvcSupport`可以书写在`SpringMvcConfig`中，以简化书写（并演示继承`WebMvcConfigurer`编写拦截器配置类）：

```java
@Configuration
@ComponentScan({"com.linner.controller", "com.linner.interceptor"})
@EnableWebMvc
public class SpringMvcConfig implements WebMvcConfigurer {
    @Autowired
    private BookInterceptor bookInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(bookInterceptor).addPathPatterns("/books", "/books/*");
    }
}
```

## 拦截器执行过程

运行程序，发送`books`开头的请求（如`http://localhost/books`），终端会有如下输出：

```
preHandle...
getAll...
postHandle...
afterCompletion...
```

拦截器的执行顺序如下：

1. 执行`preHandle()`。
    - `preHandle()`返回值为`true`：
        1. 执行请求路径相应的方法或下一个拦截器的`preHandle()`。
        2. 判断是否执行`postHandle()`：
            - Controller被执行（后续拦截器链中没有一个`preHandle()`返回值为`false`），执行`postHandle()`。
            - Controller没有被执行（后续拦截器链中存在一个`preHandle()`返回值为`false`），不执行`postHandle()`。
        3. 执行`afterCompletion()`。
    - `preHandle()`返回值为`false`。        
2. 结束。

当配置多个拦截器时，形成拦截器链。多个`preHandle()`按照Interceptor被声明顺序执行；多个`postHandle()`按照Interceptor被声明顺序逆序执行。即，<u>拦截器链的运行顺序以拦截器添加顺序为准</u>。

当拦截器中出现对原始处理器的拦截，后面的拦截器均终止运行。当拦截器运行中断，仅运行配置在前面的拦截器的`afterCompletion()`操作（`afterCompletion()`代表当前拦截器执行完成，与后续拦截器链中`preHandle()`的返回值和Controller是否被执行无关）。

假设现在有如下两个拦截器：

```java
@Component
public class FirstInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("FirstInterceptor preHandle...");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("FirstInterceptor postHandle...");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("FirstInterceptor afterCompletion...");
    }
}
```

<br>

```java
@Component
public class LaterInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("LaterInterceptor preHandle...");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("LaterInterceptor postHandle...");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("LaterInterceptor afterCompletion...");
    }
}
```

它们在拦截器类中的添加顺序为：

```java
@Autowired
private FirstInterceptor firstInterceptor;
@Autowired
private LaterInterceptor laterInterceptor;

@Override
public void addInterceptors(InterceptorRegistry registry) {
    // 如果没有配置拦截路径，则默认拦截所有请求
    registry.addInterceptor(firstInterceptor);
    regisrty.addInterceptor(laterInterceptor);
}
```

访问任意资源后，终端输出：

```
FirstInterceptor preHandle...
LaterInterceptor preHandle...
LaterInterceptor postHandle...
FirstInterceptor postHandle...
LaterInterceptor afterCompletion...
FirstInterceptor afterCompletion...
```

实际上在`DispatcherServlet`的`doDispatch()`方法中，在执行`mv = ha.handle(...)`之前进行了一个条件判断：

```java
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
    /* ... */
    // 拦截器链
    HandlerExecutionChain mappedHandler = null;

    try {
        /* ... */
        try {
            /* ... */

            // 获取拦截器链
            mappedHandler = getHandler(processedRequest);
            if (mappedHandler == null) {
                noHandlerFound(processedRequest, response);
                return;
            }

            /* ... */

            // 执行相应Interceptor的preHandle
            if (!mappedHandler.applyPreHandle(processedRequest, response)) {
                return;
            }

            // Actually invoke the handler.
            mv = ha.handle(processedRequest, response, mappedHandler.getHandler());

            /* ... */

            // 执行相应Interceptor的postHandle
            mappedHandler.applyPostHandle(processedRequest, response, mv);

            /* ... */
        } catch /* ... */

        // 处理调度结果
        // 包含了ModelAndView的进一步处理（渲染视图、处理模型）、还有Interceptor的afterCompletion()调用等等
        processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);

        /* ... */
    }
}
```

即`HandlerExecutionChain`（控制器执行链）对象的`applyPreHandle()`方法：

```java
/**
 * preHandle执行链
 */
boolean applyPreHandle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    // 读取interceptorList中的interceptor，并逐个执行它们的preHandle方法
    for (int i = 0; i < this.interceptorList.size(); i++) {
        // 获取interceptor
        HandlerInterceptor interceptor = this.interceptorList.get(i);
        // 执行interceptor.preHandle
        // 如果有一个interceptor返回了false，则立即执行triggerAfterCompletion()并返回false
        if (!interceptor.preHandle(request, response, this.handler)) {
            triggerAfterCompletion(request, response, null);
            return false;
        }
        // 记录拦截器链中返回false的前一个拦截器下标
        this.interceptorIndex = i;
    }
    return true;
}
```

`applyPostHandle()`方法：

```java
/**
 * postHandle执行链
 */
void applyPostHandle(HttpServletRequest request, HttpServletResponse response, @Nullable ModelAndView mv)
        throws Exception {
    // 按照倒序，逐个执行interceptor.postHandle()
    for (int i = this.interceptorList.size() - 1; i >= 0; i--) {
        HandlerInterceptor interceptor = this.interceptorList.get(i);
        interceptor.postHandle(request, response, this.handler, mv);
    }
}
```

`triggerAfterCompletion()`：

```java
/**
 * afterCompletion执行链
 */
void triggerAfterCompletion(HttpServletRequest request, HttpServletResponse response, @Nullable Exception ex) {
    // 从interceptorIndex开始，倒序执行interceptor.afterCompletion()
    // 即triggerAfterCompletion()只会执行那些返回true的interceptor的afterCompletion()
    for (int i = this.interceptorIndex; i >= 0; i--) {
        HandlerInterceptor interceptor = this.interceptorList.get(i);
        try {
            interceptor.afterCompletion(request, response, this.handler, ex);
        }
        catch (Throwable ex2) {
            logger.error("HandlerInterceptor.afterCompletion threw exception", ex2);
        }
    }
}
```

在一切正常运行完成后，调用Interceptor的`afterCompletion()`的情况有点复杂（`DispatcherServlet`中的`processDispatchResult()`）：

```java
private void processDispatchResult(HttpServletRequest request, HttpServletResponse response,
        @Nullable HandlerExecutionChain mappedHandler, @Nullable ModelAndView mv,
        @Nullable Exception exception) throws Exception {

    /* ... */

    // Did the handler return a view to render?
    if (mv != null && !mv.wasCleared()) {
        // 渲染视图
        render(mv, request, response);
        /* ... */
    }

    /* ... */
        
    // 在处理完其它调度结果后，通过mappedHandler.triggerAfterCompletion()来调用interceptor.afterCompletion()
    if (mappedHandler != null) {
        // Exception (if any) is already handled..
        mappedHandler.triggerAfterCompletion(request, response, null);
    }
}
```

> 在`DispatcherServlet`的`doDispatch()`方法中还有许多`try ... catch`，当捕捉到异常时，`doDispatch()`也会通过`triggerAfterCompletion()`方法来调用`mappedHandler.triggerAfterCompletion()`。

更具上述代码总结出`HandlerExecutionChain`中的执行情况：

- `applyPreHandle()`：在执行Controller方法之前执行。
- `applyPostHandle()`：执行了Controller方法之后执行。
- `triggerAfterCompletion()`：

    两种执行情况：

    - 执行`applyPreHandle()`时，有一个拦截器返回了`false`；
    - 执行完`applyPostHandle()`之后，且无错误时执行（即`processDispatchResult()`中的`triggerAfterCompletion()`）；
    - 执行`applyPreHandle()`、`applyPostHandle()`和Controller方法过程中，出现异常时执行（出现异常时是在`DispatcherServlet`的`triggerAfterCompletion()`中调用）。

## 配置文件配置拦截器

除了使用配置类外，也可以使用配置文件的方式来配置拦截器。

使用`<mvc:interceptors>`来配置拦截器。添加拦截器的方式有两种，一种是使用<u>`<bean class="..."/>`</u>，另一种是使用<u>`<ref bean="..."/>`</u>，这两种方式本质上没有什么区别。

```xml
<mvc:interceptors>
    <bean class="asia.linner.interceptor.FirstInterceptor"/>
    <!-- bean中指定的是Bean的默认id，即首字母小写的类名 -->
    <ref bean="laterInterceptor"/>
</mvc:interceptors>
```

在`<mvc:interceptors>`中可以使用`<mvc:interceptor>`来配置具体的拦截规则：

```xml
<mvc:interceptors>
    <mvc:interceptor>
        <!-- <mvc:mapping>: 拦截路径 -->
        <mvc:mapping path="/**"/>
        <!-- <mvc:exclude-mapping>: 排除路径（不拦截） -->
        <mvc:exclude-mapping path="/users"/>
        <mvc:exclude-mapping path="/users/**"/>
        <!-- 假设只有users和books这两个请求 -->
        <ref bean="bookInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```

---

# 静态资源处理器

```java
@Configuration
public class SpringMvcSupport extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 放行单个目录（Webapp目录下）
        registry.addResourceHandler("/pages/**").addResourceLocations("/pages/");
        // 也可以将Webapp整个目录都添加：
        // registry.addResourceHandler("/**").addResourceLocations("classpath:/");
    }
}
```

- `addResourceHandler()`：定义访问资源路径。
- `addResourceLocations()`：定义访问路径时的静态资源目录。

---

# 异常处理

Spring MVC 自带了两个异常处理器分别是`SimpleMappingExceptionResolver`和`DefaultHandlerExceptionResolver`。

其中`DefaultHandlerExceptionResolver`是由Spring MVC定义的默认异常处理器，它的`doResolveException()`定义了一些常见的异常处理：

```java
protected ModelAndView doResolveException(
        HttpServletRequest request, HttpServletResponse response, @Nullable Object handler, Exception ex) {

    try {
        if (ex instanceof HttpRequestMethodNotSupportedException) {
            return handleHttpRequestMethodNotSupported(
                    (HttpRequestMethodNotSupportedException) ex, request, response, handler);
        }
        else if (ex instanceof HttpMediaTypeNotSupportedException) {
            return handleHttpMediaTypeNotSupported(
                    (HttpMediaTypeNotSupportedException) ex, request, response, handler);
        }
        else if (ex instanceof HttpMediaTypeNotAcceptableException) {
            return handleHttpMediaTypeNotAcceptable(
                    (HttpMediaTypeNotAcceptableException) ex, request, response, handler);
        }
        else if (ex instanceof MissingPathVariableException) {
            return handleMissingPathVariable(
                    (MissingPathVariableException) ex, request, response, handler);
        }
        else if (ex instanceof MissingServletRequestParameterException) {
            return handleMissingServletRequestParameter(
                    (MissingServletRequestParameterException) ex, request, response, handler);
        }
        else if (ex instanceof ServletRequestBindingException) {
            return handleServletRequestBindingException(
                    (ServletRequestBindingException) ex, request, response, handler);
        }
        else if (ex instanceof ConversionNotSupportedException) {
            return handleConversionNotSupported(
                    (ConversionNotSupportedException) ex, request, response, handler);
        }
        else if (ex instanceof TypeMismatchException) {
            return handleTypeMismatch(
                    (TypeMismatchException) ex, request, response, handler);
        }
        else if (ex instanceof HttpMessageNotReadableException) {
            return handleHttpMessageNotReadable(
                    (HttpMessageNotReadableException) ex, request, response, handler);
        }
        else if (ex instanceof HttpMessageNotWritableException) {
            return handleHttpMessageNotWritable(
                    (HttpMessageNotWritableException) ex, request, response, handler);
        }
        else if (ex instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValidException(
                    (MethodArgumentNotValidException) ex, request, response, handler);
        }
        else if (ex instanceof MissingServletRequestPartException) {
            return handleMissingServletRequestPartException(
                    (MissingServletRequestPartException) ex, request, response, handler);
        }
        else if (ex instanceof BindException) {
            return handleBindException((BindException) ex, request, response, handler);
        }
        else if (ex instanceof NoHandlerFoundException) {
            return handleNoHandlerFoundException(
                    (NoHandlerFoundException) ex, request, response, handler);
        }
        else if (ex instanceof AsyncRequestTimeoutException) {
            return handleAsyncRequestTimeoutException(
                    (AsyncRequestTimeoutException) ex, request, response, handler);
        }
    }
    catch (Exception handlerEx) {
        if (logger.isWarnEnabled()) {
            logger.warn("Failure while trying to resolve exception [" + ex.getClass().getName() + "]", handlerEx);
        }
    }
    return null;
}
```

`SimpleMappingExceptionResolver`是Spring MVC提供的自定义异常处理器。

## 基于配置的异常处理

在`springMVC.xml`中配置异常处理器：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans>

    <!-- ... -->

    <!-- 配置异常处理 -->
    <bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
        <property name="exceptionMappings">
            <props>
                <!--
                    设置要处理的异常和返回的视图
                    prop的key：表示处理器方法执行过程中出现的异常
                    prop的值：表示若出现指定异常时，设置一个新的视图名称，跳转到指定页面
                 -->
                <prop key="java.lang.ArithmeticException">error</prop>
            </props>
        </property>
        <!-- 
            将异常信息共享在请求域中的键
            exceptionAttribute设置一个属性名，将出现的异常信息在请求域中进行共享
         -->
        <property name="exceptionAttribute" value="ex"/>
    </bean>

</beans>
```

在相应路径（如`/WEB-INF/templates/`）下创建异常视图（如`error.html`）：

```html
<!DOCTYPE html>
<html lang="zh" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
</head>
<body>
    <h1>出现异常</h1>
    <p th:text="${ex}"></p>
</body>
</html>
```

> 注：使用Thymeleaf管理Html视图，需要在`<html>`标签中声明`xmlns:th="http://www.thymeleaf.org"`。

## 基于注解的异常处理

使用`@ControllerAdvice`标注在异常处理类上，这样的异常处理类与Controller类似。在类的方法上用`@ExceptionHandler`指定要处理的异常，`@ExceptionHandler`的`value`属性接收一个Class类型的数组，意味着可以同时处理多个异常。

```java
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler({
            ArithmeticException.class,
            NullPointerException.class
    })
    public String testException(
            Exception ex /* 当前出现的异常 */,
            Model model) {

        // 用Model设置属性，将异常信息返回
        model.addAttribute("exception", ex);
        return "error";
    }
}
```

在`ExceptionController`中用`@ExceptionHandler`标注的方法，在它的参数列表中定义一个`Exception`类型的参数，可用于获取当前处理的实际的异常。

---

# 重定向

Spring MVC默认的方式是`forward`（即转发），而要使用`redirect`需要在视图名称中添加`redirect:`说明。Spring MVC重定向有以下几种方式：

```java
@Controller
public class TestController {

    /**
     * 通过返回类型为String的方法，返回一个"redirect:..."的字符串进行重定向
     * @return 重定向的路径
     */
    @RequestMapping("/test1")
    public String test1() {
        return "redirect:/index.html";
    }

    /**
     * 通过ModelAndView设置视图名称为"redirect:..."
     * @return
     */
    @RequestMapping("/test2")
    public ModelAndView test2() {
        return new ModelAndView("redirect:/test1");
        // 相当于：
        /*
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/index");
        return mav;
        */
    }

    /**
     * 通过原生ServletAPI
     */
    @RequestMapping("/test3")
    public void test3(HttpServletResponse response) throws IOException {
        response.sendRedirect("/test1");
    }
}
```

---

# Spring MVC 执行流程

## 常用组件

- **DispatcherServlet**：**前端控制器**，由框架提供。
    
    作用：统一处理请求和响应，整个流程控制的中心，由它调用其它组件处理用户的请求。
    
    > Controller、Interceptor、HandlerExceptionResolver等等都由它来调用。

- **HandlerMapping**：**处理器映射器**，由框架提供。

    作用：根据请求的`url`、`method`等信息查找相应的Handler（即控制器方法）。

    > 就是请求中的`@RequestMapping`、`@GetMapping`、`@PostMapping`等等。将请求和控制器或控制器方法进行映射。

- **Handler**：**处理器**（控制器方法），由工程师开发。

    作用：在DispatcherServlet的控制下，Handler对具体的用户请求进行处理。

- **HandlerAdapter**：**处理器适配器**，由框架提供。

    作用：通过HandlerAdapter执行处理器（控制器方法）。

    > 由HandlerMapping找到对应的Handler，接着由HandlerAdapter执行对应的Handler。

- **ViewResolver**：**视图解析器**，由框架提供。
    
    作用：进行视图解析，得到相应的视图，例如：ThymeleafView、InternalResourceView（例如forward，即转发时）、RedirectView（例如redirect，即重定向时）。

- **Viw**：**视图**，由框架或视图技术提供。
    
    作用：将模型数据通过页面展示给用户。

## DispatcherServlet 继承链

`DispatcherServlet` $\xrightarrow{extends}$ `FrameworkServlet` $\xrightarrow{extends}$ `HttpServletBean` $\xrightarrow{extends}$ `HttpServlet` $\xrightarrow{extends}$ `GenericServlet` $\xrightarrow{implements}$ `Servlet`

## DispatcherServlet 初始化过程

DispatcherServlet初始化过程需要根据它的继承链，查找每个类或接口的`init()`方法。

- `Servlet.init(ServletConfig)`：

    ```java
    public void init(ServletConfig config) throws ServletException;
    ```

- `GenericServlet`：

    `GenericServlet`不仅实现了`Servlet.init(ServletConfig)`还给出了一个未实现的`init()`。

    `init(ServletConfig config)`：

    ```java
    public void init(ServletConfig config) throws ServletException {
        this.config = config;
        this.init();
    }
    ```

    `init()`：

    ```java
    public void init() throws ServletException {}
    ```

- `HttpServlet`：

    `HttpServlet`并没有重写`GenericServlet.init(ServletConfig)`或`GenericServlet.init()`。

- `HttpServletBean`：

    `HttpServletBean`实现了`GenericServlet.init()`并且给出了一个未实现的`initServletBean()`。

    `init()`：

    ```java
    @Override
    public final void init() throws ServletException {

        // Set bean properties from init parameters.
        /* ... */

        // Let subclasses do whatever initialization they like.
        initServletBean();
    }
    ```

    `initServletBean()`：初始化ServletBean。

    ```java
    protected void initServletBean() throws ServletException {}
    ```

- `FrameworkServlet`：

    `FrameworkServlet`实现了`HttpServletBean.initServletBean()`，并且给出了`initServletBean()`的实现。

    `initServletBean()`：

    ```java
    @Override
    protected final void initServletBean() throws ServletException {
        
        /* ... */

        try {
            // 初始化WebApplicationContext
            this.webApplicationContext = initWebApplicationContext();
            initFrameworkServlet();
        }
        /* catch ... */

        /* ... */
    }
    ```

    `initWebApplicationContext()`：初始化WebApplicationContext。

    ```java
    protected WebApplicationContext initWebApplicationContext() {
        // 获取当前的WebApplicationContext
        WebApplicationContext rootContext =
                WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        WebApplicationContext wac = null;

        // 判断当前WebApplicationContext是否为空（第一次执行时恒为空）
        if (this.webApplicationContext != null) {
            // A context instance was injected at construction time -> use it
            wac = this.webApplicationContext;
            if (wac instanceof ConfigurableWebApplicationContext) {
                ConfigurableWebApplicationContext cwac = (ConfigurableWebApplicationContext) wac;
                if (!cwac.isActive()) {
                    // The context has not yet been refreshed -> provide services such as
                    // setting the parent context, setting the application context id, etc
                    if (cwac.getParent() == null) {
                        // The context instance was injected without an explicit parent -> set
                        // the root application context (if any; may be null) as the parent
                        cwac.setParent(rootContext);
                    }
                    // 装配并刷新WebApplicationContext
                    configureAndRefreshWebApplicationContext(cwac);
                }
            }
        }
        /*
            如果wac为空则查找WebApplicationContext
            （第一次执行完后wac还是为空，因为没有任何WebApplicationContext）
         */
        if (wac == null) {
            // No context instance was injected at construction time -> see if one
            // has been registered in the servlet context. If one exists, it is assumed
            // that the parent context (if any) has already been set and that the
            // user has performed any initialization such as setting the context id
            wac = findWebApplicationContext();
        }
        // 如果wac为空则创建一个WebApplicationContext
        if (wac == null) {
            // No context instance is defined for this servlet -> create a local one
            // 创建一个WebApplicationContext
            wac = createWebApplicationContext(rootContext);
        }

        // 没有接收到刷新事件时
        if (!this.refreshEventReceived) {
            // Either the context is not a ConfigurableApplicationContext with refresh
            // support or the context injected at construction time had already been
            // refreshed -> trigger initial onRefresh manually here.
            synchronized (this.onRefreshMonitor) {
                // 刷新WebApplicationContext
                onRefresh(wac);
            }
        }

        if (this.publishContext) {
            // Publish the context as a servlet context attribute.
            // 将ServletContext作为属性，获取它的属性名
            String attrName = getServletContextAttributeName();
            // 将IOC容器在应用域共享
            getServletContext().setAttribute(attrName, wac);
        }

        return wac;
    }
    ```

    `createWebApplicationContext(WebApplicationContext)`：通过WebApplicationContext创建WebApplicationContext。

    ```java
    protected WebApplicationContext createWebApplicationContext(@Nullable WebApplicationContext parent) {
        return createWebApplicationContext((ApplicationContext) parent);
    }
    ```

    `createWebApplicationContext(ApplicationContext)`：通过ApplicationContext创建WebApplicationContext。

    ```java
    protected WebApplicationContext createWebApplicationContext(@Nullable ApplicationContext parent) {
        Class<?> contextClass = getContextClass();
        if (!ConfigurableWebApplicationContext.class.isAssignableFrom(contextClass)) {
            throw new ApplicationContextException(
                    "Fatal initialization error in servlet with name '" + getServletName() +
                    "': custom WebApplicationContext class [" + contextClass.getName() +
                    "] is not of type ConfigurableWebApplicationContext");
        }
        // Web IOC 容器对象（即SpringMVC IOC）
        ConfigurableWebApplicationContext wac =
                (ConfigurableWebApplicationContext) BeanUtils.instantiateClass(contextClass);

        // 配置环境
        wac.setEnvironment(getEnvironment());
        /* 
            整合Spring和SpringMVC时，
            设置SpringMVC的父容器，
            让Spring和SpringMVC的IOC容器能无缝衔接
            SpringMVC IOC容器是Spring IOC容器的子容器
         */
        wac.setParent(parent);
        String configLocation = getContextConfigLocation();
        if (configLocation != null) {
            wac.setConfigLocation(configLocation);
        }
        // 装配并刷新WebApplicationContext
        configureAndRefreshWebApplicationContext(wac);

        return wac;
    }
    ```

    `onRefresh()`：刷新WebApplicationContext。

    ```java
    protected void onRefresh(ApplicationContext context) {
        // For subclasses: do nothing by default.
        // 由子类去实现
    }
    ```

    > FrameworkServlete创建WebApplicationContext后，刷新容器，调用`onRefresh(wac)`，此方法在`DispatcherServlet`进行了重写（实现）。

    `getServletContextAttributeName()`：

    ```java
    // FrameworkServlet的全类名.CONTEXT.
    public static final String SERVLET_CONTEXT_PREFIX = FrameworkServlet.class.getName() + ".CONTEXT.";

    public String getServletContextAttributeName() {
        // 前缀+Servlet友好名称（即前缀 + <servlet-name>）
        return SERVLET_CONTEXT_PREFIX + getServletName();
    }
    ```

- `DispatcherServlet`：

    实现了`FrameworkServlet.onRefresh()`。

    ```java
    @Override
    protected void onRefresh(ApplicationContext context) {
        initStrategies(context);
    }
    ```

    `initStrategies()`：`DispatcherServlet`初始化策略。

    ```java
    protected void initStrategies(ApplicationContext context) {
        // 初始化多个解析器，例如文件上传解析器等
        initMultipartResolver(context);
        initLocaleResolver(context);
        // 初始化模板解析器
        initThemeResolver(context);
        // 初始化处理器映射器
        initHandlerMappings(context);
        // 初始阿虎处理器适配器
        initHandlerAdapters(context);
        // 初始化异常处理器
        initHandlerExceptionResolvers(context);
        // 初始化转换器，将请求转换为视图名称
        initRequestToViewNameTranslator(context);
        // 初始化视图解析器
        initViewResolvers(context);
        initFlashMapManager(context);
    }
    ```

## DispatcherServlet 请求处理过程

- `Servlet`和`GenericServlet`：

    `Servlet`提供了`service()`接口，`GenericServlet`并未对其进行实现。

    `service(ServletRequest, ServletResponse)`：

    ```java
    public void service(ServletRequest req, ServletResponse res)
            throws ServletException, IOException;
    ```

- `HttpServlet`：

    `service()`由`HttpServlet`实现。并且`HttpServlet`还提供了`service(HttpServletRequest, HttpServletResponse)`。

    `service(ServletRequest, ServletResponse)`：

    ```java
    @Override
    public void service(ServletRequest req, ServletResponse res)
        throws ServletException, IOException {
    
        HttpServletRequest  request;
        HttpServletResponse response;
        
        if (!(req instanceof HttpServletRequest &&
                res instanceof HttpServletResponse)) {
            throw new ServletException("non-HTTP request or response");
        }

        // 转换为HttpServletRequest和HttpServletResponse
        request = (HttpServletRequest) req;
        response = (HttpServletResponse) res;

        service(request, response);
    }
    ```

    > `HttpServlet.service(ServletRequest, ServletResponse)`的主要作用就是将`ServletRequest`和`ServletResponse`分别转换为`HttpServletRequest`和`HttpServletResponse`，然后调用`HttpServlet.service(HttpServletRequest, HttpServletResponse)`。

    `service(HttpServletRequest, HttpServletResponse)`：

    ```java
    protected void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        
        // 获取请求方式
        String method = req.getMethod();

        // 请求的分发处理，根据请求方式调用相应方法（如调用doGet）
        if (method.equals(METHOD_GET)) {
            long lastModified = getLastModified(req);
            if (lastModified == -1) {
                // servlet doesn't support if-modified-since, no reason
                // to go through further expensive logic
                doGet(req, resp);
            } else {
                long ifModifiedSince = req.getDateHeader(HEADER_IFMODSINCE);
                if (ifModifiedSince < lastModified) {
                    // If the servlet mod time is later, call doGet()
                    // Round down to the nearest second for a proper compare
                    // A ifModifiedSince of -1 will always be less
                    maybeSetLastModified(resp, lastModified);
                    doGet(req, resp);
                } else {
                    resp.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                }
            }

        } else if (method.equals(METHOD_HEAD)) {
            long lastModified = getLastModified(req);
            maybeSetLastModified(resp, lastModified);
            doHead(req, resp);

        } else if (method.equals(METHOD_POST)) {
            doPost(req, resp);
            
        } else if (method.equals(METHOD_PUT)) {
            doPut(req, resp);
            
        } else if (method.equals(METHOD_DELETE)) {
            doDelete(req, resp);
            
        } else if (method.equals(METHOD_OPTIONS)) {
            doOptions(req,resp);
            
        } else if (method.equals(METHOD_TRACE)) {
            doTrace(req,resp);
            
        } else {
            //
            // Note that this means NO servlet supports whatever
            // method was requested, anywhere on this server.
            //

            /* ... */
        }
    }
    ```

- `HttpServletBean`：

    没有对`HttpServlet.service()`和`HttpServlet`中`do`开头的处理请求分发的方法进行重写。

- `FrameworkServlet`：

    重写了`HttpServlet.service(HttpServletRequest, HttpServletResponse)`和`HttpServlet`中`do`开头的处理请求分发的方法。

    `service(HttpServletRequest, HttpServletResponse)`：

    ```java
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpMethod httpMethod = HttpMethod.resolve(request.getMethod());
        // 请求方式为PATCH或null时
        if (httpMethod == HttpMethod.PATCH || httpMethod == null) {
            // 执行请求
            processRequest(request, response);
        }
        else {
            super.service(request, response);
        }
    }
    ```

    `processRequest()`：

    ```java
    protected final void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* ... */

        try {
            // 执行服务
            doService(request, response);
        }
        /* catch ... */
        finally {
            /* ... */
        }
    }
    ```

    在`FrameworkServlet`中的`doGet()`、`doPost()`、`doPut()`和`doDelete()`中都是直接调用`processRequest()`方法：

    ```java
    processRequest(request, response);
    ```

    在`doOptions()`和`doTrace()`中也有对`processRequest()`方法的调用。

    `doService()`：交由子类实现。

    ```java
    protected abstract void doService(HttpServletRequest request, HttpServletResponse response)
            throws Exception;
    ```

- `DispatcherServlet`：

    ```java
    @Override
    protected void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logRequest(request);

        // Keep a snapshot of the request attributes in case of an include,
        // to be able to restore the original attributes after the include.
        /* ... */

        // Make framework objects available to handlers and view objects.
        /* ... */

        try {
            // 最终调用doDispatch()来处理
            doDispatch(request, response);
        }
        finally {
            if (!WebAsyncUtils.getAsyncManager(request).isConcurrentHandlingStarted()) {
                // Restore the original attribute snapshot, in case of an include.
                if (attributesSnapshot != null) {
                    restoreAttributesAfterInclude(request, attributesSnapshot);
                }
            }
        }
    }
    ```

    `doDispatch()`：

    ```java
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpServletRequest processedRequest = request;
        /*
            执行链
            HandlerExecutionChain包含以下三个部分：
            - handler：与请求所匹配的控制器方法
            - interceptorList：处理控制器方法的所有拦截器集合，即拦截器链
            - interceptorIndex：拦截器索引，控制拦截器afterCompletion()的执行
         */
        HandlerExecutionChain mappedHandler = null;
        boolean multipartRequestParsed = false;

        WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);

        try {
            ModelAndView mv = null;
            Exception dispatchException = null;

            try {
                processedRequest = checkMultipart(request);
                multipartRequestParsed = (processedRequest != request);

                // Determine handler for the current request.
                // 获取当前请求的执行链
                mappedHandler = getHandler(processedRequest);
                if (mappedHandler == null) {
                    noHandlerFound(processedRequest, response);
                    return;
                }

                // Determine handler adapter for the current request.
                /*
                    获取当前请求的处理器适配器
                    通过控制器方法创建对应的处理器适配器，从而能调用所对应的控制器方法
                 */
                HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());

                // Process last-modified header, if supported by the handler.
                /* ... */

                // 执行拦截器链的 preHandle，正序执行
                if (!mappedHandler.applyPreHandle(processedRequest, response)) {
                    return;
                }

                // Actually invoke the handler.
                // 通过HandlerAdapter来调用请求处理，最终获得ModelAndView对象
                mv = ha.handle(processedRequest, response, mappedHandler.getHandler());

                if (asyncManager.isConcurrentHandlingStarted()) {
                    return;
                }

                applyDefaultViewName(processedRequest, mv);
                // 执行拦截器链的 postHandle，倒序执行
                mappedHandler.applyPostHandle(processedRequest, response, mv);
            }
            /* catch ... */
            // 执行完请求的后续处理，如视图渲染、异常处理等等
            processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);
        }
        /* catch ... */
        /* finally ... */
    }
    ```

    `processDispatchResult()`：

    ```java
    private void processDispatchResult(HttpServletRequest request, HttpServletResponse response,
            @Nullable HandlerExecutionChain mappedHandler, @Nullable ModelAndView mv,
            @Nullable Exception exception) throws Exception {

        boolean errorView = false;

        // 异常处理
        if (exception != null) {
            if (exception instanceof ModelAndViewDefiningException) {
                logger.debug("ModelAndViewDefiningException encountered", exception);
                // 获取异常页面的视图
                mv = ((ModelAndViewDefiningException) exception).getModelAndView();
            }
            else {
                /* ... */
            }
        }

        // Did the handler return a view to render?
        if (mv != null && !mv.wasCleared()) {
            // 视图渲染
            render(mv, request, response);
            /* ... */
        }
        /* else ... */

        /* ... */

        if (mappedHandler != null) {
			// Exception (if any) is already handled..
            // 执行拦截器链的 afterCompletion，倒序执行（此处所有的异常已经被处理完成）
			mappedHandler.triggerAfterCompletion(request, response, null);
		}
    }
    ```

## MVC 执行流程

1. 用户向服务器发送请求，请求被SpringMVC前端控制器`DispatcherServlet`捕获。
2. `DispatcherServlet`对请求URL进行解析，得到请求资源标识符（URI），判断请求URI对应的映射：
    - 如果没有对应的映射：
        
        判断是否配置了`mvc:default-servlet-handler`（默认处理器）。
        
        -  如果没配置，则控制台报映射查找不到，向客户端展示404错误。
        -  如果有配置，则访问目标资源（一般为静态资源，如JS、CSS、HTML等等）。
        
            如果找不到客户端也会展示404错误。

    - 如果存在对应的映射则接着执行下面的流程。
3. 根据该URI，调用`HandlerMapping`获得该`Handler`配置的所有相关的对象（包括`Handler`对象以及`Handler`对象对应的拦截器），最后以`HandlerExecutionChain`执行链对象的形式返回。
4. `DispatcherServlet`根据获得的`Handler`，选择一个合适的`HandlerAdapter`。
5. 如果成功获得`HandlerAdapter`，此时将开始执行拦截器的`preHandler()`方法。

    根据拦截器链的顺序，正序执行。

6. 提取`Request`中的模型数据，填充`Handler`入参，开始执行`Handler`（`Controller`)方法，处理请求。

    在填充Handler的入参过程中，根据你的配置，Spring将帮你做一些额外的工作：

    - `HttpMessageConveter`： 将请求消息（如JSON、XML等数据）转换成一个对象，或将对象转换为指定的响应信息。
    - 数据转换：对请求消息进行数据转换。

        如`String`转换成`Integer`、`Double`等。
    
    - 数据格式化：对请求消息进行数据格式化。 

        如将字符串转换成格式化数字或格式化日期等。

    - 数据验证：验证数据的有效性（长度、格式等），验证结果存储到`BindingResult`或`Error`中。
    - `Handler`执行完成后，向`DispatcherServlet`返回一个`ModelAndView`对象。
8. 如果`Handler`被成功执行，则开始执行拦截器的`postHandle()`方法。

    根据拦截器链的顺序，倒序执行。

9. 根据返回的`ModelAndView`（此时会判断是否存在异常：如果存在异常，则执行`HandlerExceptionResolver`进行异常处理）选择一个适合的`ViewResolver`进行视图解析，根据`Model`和`View`，来渲染视图。
10. 渲染视图完毕执行拦截器的`afterCompletion()`方法。

    根据拦截器链的顺序，倒序执行。

11. 将渲染结果返回给客户端。
