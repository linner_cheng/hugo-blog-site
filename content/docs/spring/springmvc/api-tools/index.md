---
# type: docs 
title: API 测试工具
date: 2023-07-27T13:29:13Z
draft: false
pinned: false
authors: Linner
series: [Spring]
categories: [memo]
tags: [API测试]
images: [images/dark-purple.png]
---

# Postman

[![Postman](96bff51802e8a43c.png)](https://www.postman.com/)

# Apifox（作者也在用）

[![Apifox](dark-purple.png)](https://apifox.com/)

> Apifox和Postman差不多，但是比Postman更易用。界面美观、支持中文。而且设置接口参数也很方便，还能自动生成随机的参数。支持将接口导出为HTML、Markdown、PDF等格式。在编写API测试时，还能用它来编写Markdown文档来进一步说明。
>
> 支持多种导入方式：
> 
> ![Apifox数据导入方式](uTools_1690465592640.png)
>
> 缺点就是每次开机第一次打开Apifox都需要等待一段时间的加载（也有可能是我机械硬盘太慢了）。

# Swagger（Knife4j）

Swagger是一款用于Java的API文档自动生成的框架。

Knife4j是Swagger的增强UI实现。并且Swagger和Knife4j可以无缝切换。

Knife4j（Swagger）的Maven依赖坐标如下：

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>3.0.2</version> <!-- 版本号 -->
</dependency>
```
