---
title: 'Maven 高级'
date: 2023-02-27 00:00:00
tags: [Java,项目管理,Maven]
authors: Linner
categories: [note]
navWeight: 95
series: [Spring]
---

# 多模块开发

多模块开发有以下两种：

- 按照功能拆分。
- 按照模块拆分。

例如，将项目中的DAO类拆分出来作为一个独立的模块。然后使用Maven将该模块作为依赖导入项目中。

假设现在已经将项目中的实体类拆分出来作为`maven-dao`模块。该模块也使用Maven构建，然后导入该项目所需依赖。经过测试后，将该项目的`<groupId>`、`<artifactId>`和`<version>`导入原本项目。

`maven-dao`模块的`pom.xml`中：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <!-- ... -->

    <!-- 将下面内容作为依赖导入到原项目中 -->
    <groupId>com.linner</groupId>
    <artifactId>springmvc-demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <!-- END -->

    <!-- ... -->

</project>
```

将上方`<groupId>`、`<artifactId>`和`<version>`导入原项目：

```xml
<dependencies>

    <!-- ... -->

    <dependency>
        <groupId>com.linner</groupId>
        <artifactId>springmvc-demo</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>

    <!-- ... -->

</dependencies>
```

接着使用Maven的`install`命令将`maven-dao`模块安装到Maven本地仓库中，项目即可正常启动。

---

# 依赖管理

## 依赖传递

Maven项目中的依赖是具有传递性的。即，依赖的依赖可以作为依赖使用。将依赖关系作为树状结构看待，那么依赖的子孙依赖，也可以作为项目的依赖。

依赖传递有直接依赖和间接依赖。

- 直接依赖：

    在`pom.xml`中编写的依赖就是该项目的直接依赖。即，依赖树中，子依赖（子节点）即为当前项目（根节点）的直接依赖。
    
- 间接依赖：
    依赖树中，直接依赖下的所有依赖（孙子节点）即为当前项目的间接依赖。

因为依赖传递的存在，会导致使用依赖的过程中出现冲突问题。例如有两个相同的依赖，它们的版本不同，就会导致依赖冲突。

Maven指定了一系列规则来解决依赖冲突问题。

- 特殊优先：同级下（依赖树中同个父节点的依赖为同级，即同个`pom.xml`下），配置了相同资源的不同版本，后配置的覆盖先配置。
- 路径优先：依赖树中，层级越浅，优先级越高；层级越深，优先级越低。
- 声明优先：资源在相同层级被依赖时，配置顺序靠前的覆盖配置顺序靠后的。即，谁先声明用谁。

> 注意：依赖的优先级只对当前项目起作用。即，当前项目选择的依赖版本并不会影响到依赖中相同资源不同版本的依赖。
> 最终选择的依赖结果根据Idea中Mavem面板的依赖树视图。

## 可选依赖

可选依赖指对外隐藏当前所依赖的资源（不透明），隐藏后对应资源将不具有依赖传递。

可选依赖的开关使用`<optional>`。如：

```xml
<dependency>
    <groupId>xxx.xxxxxx</groupId>
    <artifactId>xxxxx-xxxxxx</artifactId>
    <version>x.x.x</version>
    <!-- 默认为false，设置为true为开启可选依赖 -->
    <optional>true</optional>
</dependency>
```

## 排除依赖

排除依赖指主动断开依赖的资源，被排除的资源无需指定版本（不需要）。即，在当前项目中，排除依赖中不需要的间接依赖。

排除依赖使用`<exclusions>`来指定一个排除的依赖列表。列表中使用`<exclusion>`来指定要排除的依赖。如：

```xml
<dependency>
    <groupId>xxx.xxxxxx</groupId>
    <artifactId>xxxxx-xxxxxx</artifactId>
    <version>x.x.x</version>
    <exclusions>
        <exclusion>
            <groupId>yyy.yyyyyy</groupId>
            <artifactId>yyyyy-yyyyyyy</artifactId>
            <!-- 排除依赖不需要指定版本 -->
        </exclusion>
    </exclusions>
</dependency>
```

## 聚合工程

聚合即为将多个模块组织成一个整体，同时进行项目构建的过程。聚合工程通常是一个不具有业务功能的"空"工程（有且仅有一个pom文件）。使用聚合工程可以将多个工程编组，通过对聚合工程进行构建，实现对所包含的模块进行同步构建。当工程中某个模块发生更新（变更）时，必须保障工程中与已更新模块关联的模块同步更新，此时可以使用聚合工程来解决批量模块同步构建的问题。

Example：

创建一个空的Maven项目，并将其打包方式设置为`pom`，然后添加所要管理的项目：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.linner</groupId>
    <artifactId>maven-parent</artifactId>
    <version>x.x.x</version>
    <packaging>pom</packaging>
    
    <!-- 设置管理的模块名称 -->
    <modules>
        <!-- module里面的值为管理模块的相对路径 -->
        <module>../xxxx</module>
        <module>../yyyy</module>
        <module>../zzzz</module>
    </modules>
    
</project>
```

当该`maven-parent`被`compile`后，所有被其管理的项目都会被执行编译操作。

> 聚合工程管理的项目在进行运行的时候，会按照项目与项目之间的依赖关系来自动决定执行的顺序和配置的顺序无关。

## 继承

继承是用来解决重复配置问题。继承描述的是两个工程间的关系子工程可以继承父工程中的配置信息，常见于依赖关系的继承。继承的作用：简化配置、减少版本冲突。

> 一般继承和聚合都是使用同一个空项目来构建，但是这两个的功能是不一样的。

在子工程中配置当前工程继承自parent工程：

```xml
<!-- 在project下配置 -->
<parent>
    <groupId>com.linner</groupId>
    <artifactId>maven-parent</artifactId>
    <version>x.x.x</version>
    <!--设置父项目pom.xml位置路径-->
    <relativePath>../maven-parent/pom.xml</relativePath>
</parent>
```

将子项目共同使用的依赖都抽取出来，维护在父项目的`pom.xml`中：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.linner</groupId>
    <artifactId>maven-parent</artifactId>
    <version>x.x.x</version>
    <packaging>pom</packaging>
    
    <modules>
        <module>../xxxx</module>
        <module>../yyyy</module>
        <module>../zzzz</module>
    </modules>
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>5.2.10.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.10.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.10.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.2.10.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.6</version>
        </dependency>

        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.0</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.47</version>
        </dependency>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.16</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.9.0</version>
        </dependency>
    </dependencies>
</project>
```

### 子项目依赖版本问题

如果把所有用到的依赖都管理在父项目的`pom.xml`，这样就会导致有很多项目引入了过多自己不需要的依赖，这样对于子项目来说也是种负担。

可以在父工程中使用`<dependencyManagement>`来定义依赖管理：

```xml
<!-- 在project下配置 -->
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

`<dependencyManagement>`标签不真正引入jar包，而是配置可供子项目选择的jar包依赖。

如果子项目要想使用它所提供的这些jar包，需要自己添加依赖，并且不需要指定`<version>`：

```xml
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <scope>test</scope>
</dependency>
```

子项目使用的jar包`version`由父项目决定。

---

# 属性

Maven属性的概念和变量的概念很相似。在一个地方声明，其他地方使用，当属性的声明修改后，所有使用属性的地方都会跟着修改。

在Maven中的属性分为:

- 自定义属性。
- 内置属性。

    使用`mvn help:system`命令查看。

- Setting属性。
- Java系统属性。
- 环境变量属性。

## 自定义属性

在父工程中使用`<properties>`定义属性：

<properties>
    <spring.version>5.2.10.RELEASE</spring.version>
    <junit.version>4.12</junit.version>
    <mybatis-spring.version>1.3.0</mybatis-spring.version>
</properties>

定义属性标签的名称可以自定义，如`<spring.version>`也可以修改为`<spring-version>`。

接着修改依赖的`version`：

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-core</artifactId>
    <version>${spring.version}</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>${spring.version}</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-jdbc</artifactId>
    <version>${spring.version}</version>
</dependency>
```

使用`${属性名}`来使用属性。

## 配置文件加载属性

属性也可以作用于其他配置文件中（如`jdbc.properties`）。

先在父工程中定义属性，并且设置Maven过滤文件范围：

```xml
<properties>
   <jdbc.url>jdbc:mysql://127.1.1.1:3306/spring_db</jdbc.url>
</properties>

<!-- ... -->

<build>
    <resources>
        <resource>
            <!-- 设置资源目录（相对路径） -->
            <directory>../xxxx/src/main/resources</directory>
            <!-- 设置是否能够解析${}，默认是false -->
            <filtering>true</filtering>
        </resource>
    </resources>
</build>
```

修改`jdbc.properties`（属性值的使用方式与`pom.xml`中相同）：

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=${jdbc.url}
jdbc.username=root
jdbc.password=root
```

如果需要在多个项目中解析属性值，可以使用`${project.basedir}`（Maven的内置系统属性）来简化书写：

```xml
<build>
    <resources>
        <!--
			${project.basedir}: 表示当前项目所在目录
			子项目继承了父项目，相当于所有的子项目都添加了资源目录的过滤
		-->
        <resource>
            <directory>${project.basedir}/src/main/resources</directory>
            <filtering>true</filtering>
        </resource>
    </resources>

    <!-- 
        忽略 web.xml 检查
        或者在 src\main\webapp\WEB-INF\ 添加一个 web.xml 文件 
    -->
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
            <version>3.2.3</version>
            <configuration>
                <!-- 忽略 web.xml 检查 -->
                <failOnMissingWebXml>false</failOnMissingWebXml>
            </configuration>
        </plugin>
    </plugins>
</build>
```

> 使用`mvn help:system`命令可以查看更多的内置属性。

---

# 版本管理

- `SNAPSHOT`（快照版本）：
    - 项目开发过程中临时输出的版本，称为快照版本。
    - 快照版本会随着开发的进展不断更新。
- `RELEASE`（发布版本）：
    - 项目开发到一定阶段里程碑后，向团队外部发布较为稳定的版本，这种版本所对应的构件文件是稳定的。
    - 即便进行功能的后续开发，也不会改变当前发布版本内容，这种版本称为发布版本。
- `alpha`（内测版）：Bug多、不稳定、内部版本不断添加新功能。
- `beta`（公测版）：不稳定（相对比`alpha`稳定些），Bug相对较多不断添加新功能。
- 纯数字版本。

---

# 多环境开发

Maven提供配置多种环境的设定，帮助开发者在使用过程中快速切换环境。

在父工程中配置多个环境，并指定默认激活环境：

```xml
<profiles>
    <!--开发环境-->
    <profile>
        <id>dev</id>
        <properties>
            <jdbc.url>jdbc:mysql://127.1.1.1:3306/spring_db</jdbc.url>
        </properties>
        <activation>
            <!-- 设定是否为默认启动环境 -->
            <activeByDefault>true</activeByDefault>
        </activation>
    </profile>
    <!--生产环境-->
    <profile>
        <id>pro</id>
        <properties>
            <jdbc.url>jdbc:mysql://127.2.2.2:3306/spring_db</jdbc.url>
        </properties>
    </profile>
    <!--测试环境-->
    <profile>
        <id>test</id>
        <properties>
            <jdbc.url>jdbc:mysql://127.3.3.3:3306/spring_db</jdbc.url>
        </properties>
    </profile>
</profiles>
```

动态切换配置环境可以使用Maven的`-P`参数来指定，参数值为环境`id`：

```shell
mvn install -P test
```

---

# 跳过测试

在执行`install`指令的时候，Maven都会按照顺序从上往下依次执行，每次都会执行`test`。`test`可以确保每次打包或者安装的时候，程序的正确性。

但是，假如测试已经通过，在没有修改程序的前提下再次执行打包或安装命令，由于顺序执行，测试会被再次执行，就有点耗费时间了。或者，功能开发过程中有部分模块还没有开发完毕，测试无法通过，但是想要把其中某一部分进行快速打包，此时由于测试环境失败就会导致打包失败。此时就需要跳过测试：

1. Idea工具可以实现跳过测试（Maven面板中带闪电图标的按钮）。
2. 在父工程中的`pom.xml`中添加测试插件配置：

    ```xml
    <build>
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.12.4</version>
                <configuration>
                    <skipTests>false</skipTests>
                    <!--排除掉不参与测试的内容-->
                    <excludes>
                        <exclude>**/BookServiceTest.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
    ```

    `<configuration>`中有如下标签：

    * `skipTests`：如果为`true`，跳过所有测试；为`false`，不跳过测试。
    * `excludes`：不参与测试的测试类，针对`skipTests`为`false`来设置的。
    * `includes`：参与测试的测试类，针对`skipTests`为`true`来设置的。

3. 命令跳过测试：
    
    ```shell
    mvn 指令 -D skipTests
    ```

    > - 执行的项目构建指令必须包含测试生命周期，否则无效果。
    > - 命令需要在`pom.xml`所在目录下进行执行。
