---
title: 'MyBatisPlus'
date: 2023-02-28 00:00:00
tags: [Java,数据库,Maven]
authors: Linner
categories: [note]
navWeight: 99
series: [Spring]
---

# 简介

MybatisPlus（简称MP）是基于MyBatis框架基础上开发的增强型工具（依然可以使用MyBatis里的工具），旨在简化开发、提供效率。更多详情请访问[MyBatisPlus官网](https://mp.baomidou.com/)（官网有两个地址，[https://mp.baomidou.com/](https://mp.baomidou.com/)或[https://mybatis.plus](https://mybatis.plus)）。

基于Spring使用MybatisPlus，在构建Spring工程时需要勾选MySQL和MyBatis相关技术。由于Spring并未收录MP，所以需要手动导入坐标：

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.4.1</version>
</dependency>
<!-- 可选： -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.16</version>
</dependency>
```

在`application.yml`（配置文件）中配置数据库信息：

```yaml
spring:
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/mybatisplus_db?serverTimezone=Asia/Shanghai 
    username: root
    password: root
```

---

# DAO接口

使用MP编写DAO接口，可以直接继承`BaseMapper<>`（该接口内置了许多DAO方法）：

```java
@Mapper
public interface UserDao extends BaseMapper<User>{
}
```

编写引导类：

```java
@SpringBootApplication
public class MybatisplusApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisplusApplication.class, args);
    }

}
```

DAO接口要想被容器扫描到，有两种方案：

1. 在DAO接口上添加`@Mapper`注解，并且确保DAO处在引导类所在包或其子包中。
2. 在引导类上添加`@MapperScan`注解，其属性为所要扫描的DAO所在包：`@MapperScan("com.linner.dao")`（可以不写`@Mapper`）。
