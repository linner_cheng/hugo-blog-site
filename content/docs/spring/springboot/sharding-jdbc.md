--- 
title: Sharding Jdbc
date: 2023-03-20
draft: false
pinned: false
series: [Spring]
categories: [note]
authors: Linner
tags: [数据库]
navWeight: 96
---

# 介绍

Sharding Jdbc是一个用于实现数据库读写分离的框架。

---

# 依赖

Sharding Jdbc依赖坐标：

```xml
<dependency>
    <groupId>org.apache.shardingsphere</groupId>
    <artifactId>sharding-jdbc-spring-boot-starter</artifactId>
    <version>4.0.0-RC1</version>
</dependency>
```

---

# 配置

配置`application.yaml`：

```yaml
spring:
  sharding-sphere:  # 旧版为 shardingsphere
    datasource:     # 数据源有多个，用名称来进行区分
      names:
        master,slave  # 名称可以自定义，多个名称用','分隔
      # 主数据源
      master:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3316/reggie?characterEncoding=utf-8
        username: root
        password: 123456
      # 从数据源
      slave:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3326/reggie?characterEncoding=utf-8
        username: root
        password: 123456
    masters-lave: # 课程中为 masterslave
      # 读写分离配置
      load-balance-algorithm-type: round_robin  # 定义从库负载均衡策略策略，round_robin为轮询
      # 最终的数据源名称（即Bean的名称）
      name: dataSource
      # 主库数据源名称
      master-data-source-name: master
      # 从库数据源名称列表，多个用','分隔
      slave-data-source-names: slave
    props:
      sql:
        show: true # 开启SQL显示，默认false
  main:
    # 允许Bean定义覆盖，否则会与其他数据源的Bean发生冲突（如Druid）
    allow-bean-definition-overriding: true
```
