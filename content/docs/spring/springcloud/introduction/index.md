---
title: "微服务简介"
date: 2023-06-26T11:46:12+08:00
tags: [Java,Maven,SpringCloud]
draft: false
authors: Linner
navWeight: 100
categories: [note]
series: [Spring]
# pinned: true
# pinnedWeight: 1
---

# 微服务架构

从前的项目是使用单体架构，单体架构是指<u>将业务的所有功能集中在一个项目中开发，打成一个包部署</u>。

单体架构的优点是：

- 架构简单；
- 部署成本低。

缺点是模块之间的耦合度高，不利于大型项目开发。

随着互联网行业的发展，服务架构从单体架构逐渐演变为现在流行的微服务架构（属于分布式架构的一种）。

![微服务架构](uTools_1687757195463.png)

分布式架构是指<u>根据业务功能对系统进行拆分，每个业务模块作为独立项目开发，称为一个服务</u>。

分布式架构的优点：

- 降低服务之间的耦合；
- 有利于服务升级拓展。

微服务是一种经过良好架构设计的分布式架构方案。

微服务的架构特征：

- 单一职责：微服务拆分粒度更小，每一个服务都对应唯一的业务能力，做到单一职责。
- 自治：团队独立、技术独立、数据独立，独立部署和交付。
- 面向服务：服务提供统一标准的接口，与语言和技术无关。
- 隔离性强：服务调用做好隔离、容错、降级，避免出现级联问题。

微服务按照服务的不同将项目分为不同的项目，每个项目运行一个服务。

例如，将用户服务和订单服务分为两个项目。

# 微服务技术栈

微服务技术栈包括：

- 微服务技术
- 异步通讯技术
- 缓存技术
- DevOps
- 搜索技术

# 微服务技术

> 在国内知名的微服务落地技术有SpringCloud和阿里的Dubbo。

常见微服务技术的对比：

<br>        |Dubbo                |SpringCloud             |SpringCloudAlibaba
:----------:|:-------------------:|:----------------------:|:-----------------
注册中心     |`Zookeeper`、`Redis` |Eureka、Consul          |Nacos、Eureka
服务远程调用  |Dubbo协议           |Feign（HTTP协议）        |Dubbo、Feign
配置中心     |无                   |SpringCloudConfig       |SpringCloudConfig、Nacos
服务网关     |无                   |SpringCloudGateway、Zuul|SpringCloudGateway、Zuul
服务监控和保护|dubbo-admin（功能弱）|Hystix                  |Sentinel

> [SpringCloud 官网](https://spring.io/projects/spring-cloud)。
>
> SpringCloud集成了各种微服务功能组件，并基于SpringBoot实现了这些组件的自动装配。而SpringCloud与SpringBoot的之间的版本存在一个兼容关系，可通过官网的说明文档查看。
