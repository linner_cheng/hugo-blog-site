---
title: "Nacos 部署"
date: 2023-07-24T02:58:36+08:00
featured: true
tags: [Spring,Maven,Nacos,Docker,环境搭建]
categories: [memo]
draft: false
authors: Linner
navWeight: 95
---


# 本地部署

Nacos是一个开源项目，已经在Github上开源。在本地搭建Nacos需要访问[Nacos项目仓库](https://github.com/alibaba/nacos)，你可以选择将该项目`clone`到本地自己打包后部署，也可以选择直接访问[Nacos的Releases页面](https://github.com/alibaba/nacos/releases)下载已经打包好的项目压缩文件进行部署。你也可以访问[Nacos官网](https://nacos.io/zh-cn/)查看如何部署。

- `clone`到本地：

    ```shell
    git clone https://github.com/alibaba/nacos
    ```

- 访问[Nacos的Releases页面](https://github.com/alibaba/nacos/releases)：

    ![Nacos的Releases页面](uTools_1690139609912.png)

    选择自己想要的版本（如`2.2.3`）进行下载：

    ![](uTools_1690139697680.png)

    无论是选择下载`.zip`还是`.tar.gz`后缀的压缩包都可以。但是在Linux环境下选择下载`.tar.gz`为佳。

    Linux中解压`.tar.gz`的命令如：

    ```shell
    tar -zxvf 压缩包文件名.tar.gz
    ```

将下载好的压缩包解压到任意非中文目录下。解压好的目录结构大致如下：

![Nacos目录结构](uTools_1690140191883.png)

- `target`：存放Nacos`jar`包的目录。
- `logs`：存放日志文件。
- `conf`：存放项目的配置文件（如`application.properties`等）以及搭建存放Nacos配置信息的数据库的`.sql`脚本文件（如`nacos-mysql.sql`等）。
- `bin`：存放启动和关闭Nacos Server的脚本文件以及一些其它的文件。

    在Windows系统中启动和关闭Nacos使用的是`.cmd`文件，即`startup.cmd`和`shutdown.cmd`，分别对应启动和关闭。在Linux系统中使用的是`.sh`文件，`startup.sh`启动，`shutdown.sh`关闭。

    使用`startup.cmd`或`startup.sh`默认为集群模式，可以添加参数`-m standalone`使用单机模式启动。

    Windows系统：

    ```bat
    cd Nacos的路径\bin
    .\startup.cmd -m standalone
    ```

    Linux系统：

    ```shell
    cd Nacos的路径/bin
    ./startup.sh -m standalone
    ```

> 在Windows系统和Linux系统上进行本地简单地部署Nacos并没有区别，这里不再分别进行赘述。

访问[http://localhsot:8848/nacos](http://localhsot:8848/nacos)即可。默认的用户名和密码均为`nacos`。

启动成功后的Nacos大概长这样子：

![Nacos 页面](uTools_1690143487422.png)

---

# 基于 MySQL 数据库部署

Nacos可以使用MySQL数据库存放配置信息。首先需要确保你的电脑上有MySQL。

> 注意：使用MySQL搭建Nacos的数据库，推荐使用MySQL 5，因为Nacos的MySQL驱动版本可以不支持MySQL 8。如果非要使用MySQL 8，可以在Nacos中升级MySQL的驱动。

首先在MySQL中新建一个数据库：

```sql
CREATE DATABASE nacos;
USE nacos;
source "Nacos的路径/bin/mysql-schema.sql";
```

创建成功后的Nacos数据库如下：

![Nacos数据库的表](uTools_1690142171755.png)

接着修改在`conf`目录里的`application.properties`文件（如果没有，就将`application.properties.example`文件复制一份，改名为`application.properties`）。需要修改的配置如下：

```properties
# 数据源
spring.datasource.platform=mysql
# 数据库数量
db.num=1
# 连接第1个数据库的url（替换成自己的）
db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
# 连接第1个数据库的用户和密码（替换成自己的）
db.user.0=root
db.password.0=root

# 数据库驱动（如果其它配置正确，但无法启动时，修改该配置）
# db.driver-class-name=com.mysql.cj.jdbc.Driver
```

接下来重启Nacos即可：

```shell
./shutdown.sh # 也可以直接 CTRL+C 结束
./startup.sh -m standalone
```

---

# 部署 Nacos 集群

在本地部署Nacos集群首先将Nacos整个应用目录复制，要在集群中部署几个Nacos就复制几份。接着再分别修改它们的配置文件`application.properties`。

例如要部署两个Nacos，做负载均衡：

第一个Nacos的配置文件可以如下：

```properties
# 服务端口
server.port=8841

# 数据库配置
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://localhost:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=root
db.password.0=root
```

第二个Nacos的配置文件可以如下：

```properties
# 服务端口
server.port=8842

# 数据库配置
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=root
db.password.0=root
```

> 因为是部署在本地（即一台机器上），所以两个Nacos的端口不能相同（否则会发生冲突）。

接着使用`startup.cmd`或`startup.sh`分别启动这两个Nacos（这里要使用集群模式启动，Nacos默认使用集群模式启动，所以不能加上`-m standalone`参数）。

此时分别访问这两个服务（例如使用[http://localhost:8841/nacos](http://localhost:8841/nacos)和[http://localhost:8842/nacos](http://localhost:8842/nacos)访问），都能正常使用。

最后需要给这个Nacos集群加个路由（例如使用Nginx）。Nginx的配置如下：

```
# Nacos 集群
upstream nacos-cluster {
    server nacos-cluster-test-1:8848;
    server nacos-cluster-test-2:8848;
}

# Nacos 服务
server {
    listen 8849;

    server_name localhost;

    location /nacos {
        proxy_pass http://nacos-cluster;
    }
}
```

添加完配置后重启Nginx即可，这里不做赘述。

配置完后访问[http://localhsot:8849/nacos](http://localhsot:8848/nacos)即可。

---

# 基于 Docker 部署

使用Docker部署，首先需要下载Nacos的镜像。Nacos官方提供了对应的镜像`nacos/nacos-server`。使用下方命令下载该容器：

```shell
docker pull nacos/nacos-server
```

> 上方的命令拉取（下载）的是版本为`latest`。要拉取对应版本的镜像，可以查看Docker官方提供的网站[hub.docker.com](https://hub.docker.com/)，在其中搜索`nacos-server`并点击`tags`查找对应的版本（[点击快速查找Nacos Server版本](https://hub.docker.com/r/nacos/nacos-server/tags)。
>
> 例如拉取`v1.4.6`版本的`nacos/nacos-server`：
>
> ```shell
> docker pull nacos/nacos-server:v1.4.6
> ```

`nacos/nacos-server`拉取完成后就可以创建对应容器：

```shell
docker run -d \
    --name nacos-server \ # 容器名称
    -p 8848:8848 \ # 端口映射，分号左边为本地端口，分号右边为容器的内部端口
    --privileged=true \
    -e MODE=standalone \ # 指定为单机模式（使用Docker创建的Docker容器默认也为集群模式，所以需要指定单机模式）
    -e PREFER_HOST_MODE=hostname \
    -v /home/nacos/nacos-server/logs:/home/nacos/logs \ # 文件挂载，分号左边为本地目录，分号右边为容器内部的目录，这里是进行日志文件目录映射
    -v /home/nacos/nacos-server/conf/:/home/nacos/conf/ \ # 配置文件映射
    -e JVM_XMS=256m -e JVM_XMX=256m \
    nacos/nacos-server \ # 创建容器所使用的镜像
    /usr/sbin/init
```

访问[http://localhsot:8848/nacos](http://localhsot:8848/nacos)即可。默认的用户名和密码均为`nacos`。

如果是在Windows中使用Docker Desktop部署，目录映射的路径应该这样写：

```bat
docker run -d \
    --name nacos-server \
    -p 8848:8848 \
    --privileged=true \
    -e MODE=standalone \
    -e PREFER_HOST_MODE=hostname \
    -v /d/docker-volume/nacos/nacos-server/logs:/home/nacos/logs \
    -v /d/docker-volume/nacos/nacos-server/conf/:/home/nacos/conf/ \
    -e JVM_XMS=256m -e JVM_XMX=256m \
    nacos/nacos-server \
    /usr/sbin/init
```

---

# 基于 Docker 使用 MySQL 部署

根据如上方法创建Nacos容器，然后修改映射到本地的配置文件。例如修改`/home/nacos/nacos-server/conf`下的`application.properties`：

```properties
# 数据库配置（根据你的配置环境修改）
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://localhost:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=root
db.password.0=root
```

> 注：这个MySQL需要使用`mysql-schema.sql`创建`nacos`数据库。

如果MySQL也是使用Docker创建的，可以给Nacos容器和MySQL容器添加一个Docker Network，并将它们都加入这个Network中：

1. 创建Docker Network：

    ```shell
    docker network create nacos-network
    ```

2. 分别将Nacos容器和MySQL容器加入Docker Network（假设MySQL的容器名为`mysql`）：

    ```shell
    docker network connect nacos-network nacos-server
    docker network connect nacos-network mysql
    ```

之后在加入这个Network的容器中，可以将它们的容器名当作IP地址来使用。所以在`nacos-server`中可以将`db.url`修改为如下：

```properties
db.url.0=jdbc:mysql://mysql:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
```

其实使用Docker部署并使用MySQL数据库还有一个简便的方法（按照上述方法直接修改配置文件可能不生效），可以在创建容器的Docker命令中通过`nacos-server`提供的环境变量直接指定MySQL服务的配置信息（`nacos-server`容器在创建时会自动生成对应的配置文件）：

```shell
docker run -d \
    --name nacos-server \
    -p 8848:8848 \
    --network nacos-network\ # 在创建容器时指定Network
    --privileged=true \
    -e SPRING_DATASOURCE_PLATFORM=mysql \ # 指定数据源类型为MySQL
    -e MYSQL_SERVICE_HOST=mysql \ # MySQL服务器的地址（这里加入了Network所以可以直接使用容器名称来当Host）
    -e MYSQL_SERVICE_PORT=3306 \ # MySQL服务端口
    -e MYSQL_SERVICE_USER=root \ # MySQL用户名
    -e MYSQL_SERVICE_PASSWORD=root \ # MySQL密码
    -e MYSQL_SERVICE_DB_NAME=nacos \ # MySQL的数据库名称
    -e PREFER_HOST_MODE=hostname \
    -v /d/27120/docker-volume/nacos/%name%/logs:/home/nacos/logs \
    -v /d/27120/docker-volume/nacos/%name%/conf/:/home/nacos/conf/ \
    -e JVM_XMS=256m -e JVM_XMX=256m \
    nacos/nacos-server \
    /usr/sbin/init
```

> 使用Docker创建的MySQL容器同样需要创建`nacos`数据库。可以通过其它文件传输方式传输到容器中（这种方式适用于你有一个已经在运行的MySQL容器）。
>
> 在容器中创建数据库可以在本机输入命令（假设`mysql-schema.sql`导入到了容器的`root`目录中）：
> 
> ```shell
> docker exec mysql mysql -uroot -proot -e "CREATE DATABASE nacos; USE nacos; SOURCE /root/mysql-schema.sql;"
> ```
> 
> 上方命令第一个`mysql`是指容器名称，第二个是指容器中的`mysql`命令。
> 
> 或者是在本地使用Docker登录MySQL后进行操作：
> 
> ```shell
> docker exec mysql mysql -uroot -p
> # 然后输入你的密码，回车
> mysql> CREATE DATABASE nacos;
> mysql> USE nacos;
> mysql> SOURCE /root/mysql-schema.sql;
> ```

推荐使用Dockerfile方式创建一个包含`nacos`数据库的MySQL容器：

```dockerfile
FROM mysql:5.7
ENV TZ=Asia/Shanghai
ENV MYSQL_ROOT_PASSWORD=123456
ENV LANG=C.UTF-8
COPY ./mysql-schema.sql /docker-entrypoint-initdb.d
```

这是利用MySQL镜像的机制。MySQL容器在创建时会自动执行`/docker-entrypoint-initdb.d`目录下的`.sh`、`.sql`等文件。因为Nacos官方提供的`mysql-schema.sql`中并没有创建数据库和使用数据库的SQL语句，所以在创建容器之前还需要对`mysql-schema.sql`稍加修改。在`mysql-schema.sql`里面最上方添加这么两句：

```sql
 CREATE DATABASE IF NOT EXISTS nacos;
 USE nacos;
```

需要注意的是这里的数据库名称要和创建`nacos-server`时配置的一致。

然后将`mysql-schema.sql`复制到与Dockerfile同级的目录中（或者你也可以在Dockerfile将`mysql-schema.sql`的路径修改到你想要的位置）。接着使用`docker build`命令构建镜像：

```shell
docker build -f .\Dockerfile . -t linner/mysql-nacos
```

最后使用`docker run`运行容器：

```shell
docker run -id --name mysql-nacos -p 3306:3306 linner/nacos-mysql
```

> 因为一些配置在Dockerfile提前设置过了，所以这条构建MySQL容器的命令十分简洁。

---

# 基于 Docker 搭建 Nacos 集群

基于Docker搭建Nacos集群，首先可以创建一个用于创建集群模式Nacos的脚本`create_nacos.sh`（使用MySQL）：

```shell
name=$1
port=$2

docker run -d \
    --name $name \
    -p $port:8848 \
    --network nacos-network \
    --privileged=true \
    -e SPRING_DATASOURCE_PLATFORM=mysql \
    -e MYSQL_SERVICE_HOST=mysql \
    -e MYSQL_SERVICE_PORT=3306 \
    -e MYSQL_SERVICE_USER=root \
    -e MYSQL_SERVICE_PASSWORD=123456 \
    -e MYSQL_SERVICE_DB_NAME=nacos_config \
    -e PREFER_HOST_MODE=hostname \
    -v /home/nacos/$name/logs:/home/nacos/logs \
    -v /home/nacos/$name/conf/:/home/nacos/conf/ \
    -e JVM_XMS=256m -e JVM_XMX=256m \
    nacos/nacos-server \
    /usr/sbin/init
```

例如创建两个Nacos容器（在本地创建两个容器，端口不能重复）：

```shell
./create_nacos.sh nacos1 8841
./create_nacos.sh nacos2 8842
```

接着将它们加入`nacos-network`后重启（如果这两个Nacos容器没有停止运行的话，可以忽略）：

```
docker restart nacos1
docker restart nacos2
```

接着配置Nginx的方法和之前描述的方式类似。如果Nginx是使用Docker创建的，也可以将该Nginx容器加入`nacos-network`，然后直接使用容器的名称作为Host即可。

最后访问Nginx即可。
