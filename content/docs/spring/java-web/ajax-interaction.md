---
title: 'Ajax 前后端交互'
linkTitle: Ajax
date: 2022-09-25 00:00:00
tags: [Java,JavaWeb,Tomcat,Ajax]
authors: Linner
navWeight: 93
categories: [note]
---

# Ajax

`AJAX` (Asynchronous JavaScript And XML)，其含义为异步的 JavaScript 和 XML。其中 `JavaScript` 表明该技术和前端相关；`XML` 是指以此进行数据交换。

AJAX 作用有以下两方面：

1. **与服务器进行数据交换**：通过AJAX可以给服务器发送请求，服务器将数据直接响应回给浏览器。
2. **异步交互**：可以在不重新加载整个页面的情况下，与服务器交换数据并更新部分网页的技术，如：搜索联想、用户名是否可用校验等等。
    - 同步：浏览器页面在发送请求给服务器，在服务器处理请求的过程中，浏览器页面不能做其他的操作。只能等到服务器响应结束后，浏览器页面才能继续做其他的操作。
    - 异步：浏览器页面发送请求给服务器，在服务器处理请求的过程中，浏览器页面还可以做其他的操作。

## Ajax 简单使用

服务端：

```java
package web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ajaxServlet")
public class AjaxServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 返回给客户端响应数据
        resp.getWriter().write("Hello Ajax!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
```

客户端：

```java
//1. 创建核心对象
let xhttp;
if (window.XMLHttpRequest) {
    xhttp = new XMLHttpRequest();
} else {
    // code for IE6, IE5
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}

//2. 发送请求
xhttp.open("GET", "http://localhost:8080/ajax-demo/ajaxServlet");
xhttp.send();

//3. 获取响应
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        // 处理从服务端发送过来的响应数据
        alert(this.responseText);
    }
};
```

---

# Axios

Axios 是一个对原生的AJAX进行封装，简化书写的前端框架。

## 使用 Axios 进行交互

Axios 的使用分为以下两步：

* 引入 Axios 的 js 文件

  ```html
  <script src="js/axios.js"></script>
  ```

* 使用 Axios 发送请求，并获取响应结果：

  * 发送 GET 请求的基本形式：

    ```js
    axios({
        method: "get",
        // 请求参数会传递给服务器，此处请求参数可以转义也可以不转义
        url: "http://localhost:8080/axios-demo/demo?message=Hello%20World"
    }).then(function (resp){
        // 对响应数据进行处理
        alert(resp.data);
    })
    ```

  * 发送 POST 请求的基本形式：

    ```js
    axios({
        method: "post",
        url: "http://localhost:8080/axios-demo/demo",
        data: "message=Hello World"
    }).then(function (resp){
        alert(resp.data);
    });
    ```

其中：

* `axios()` 是用来发送异步请求的，小括号中使用 js 对象传递请求相关的参数：
    * `method` 属性：用来设置请求方式的。

        取值为 `get` 或者 `post`。

    * `url` 属性：用来书写请求的资源路径。

        如果是 `get` 请求，需要将请求参数拼接到路径的后面，格式为： `url?参数名=参数值&参数名2=参数值2`。

    * `data` 属性：作为请求体被发送的数据。

        即，如果是 `post` 请求的话，数据需要作为 `data` 属性的值。

* `then()` 需要传递一个匿名函数。

    `then()` 中传递的匿名函数称为回调函数，意思是该匿名函数在发送请求时不会被调用，而是在成功响应后调用的函数。
    
    而该回调函数中的 `resp` 参数是对响应的数据进行封装的对象，通过 `resp.data` 可以获取到响应的数据。

## 请求方法别名

为了方便起见， Axios 已经为所有支持的请求方法提供了别名。如下：

* `get` 请求：

    ```javascript
    axios.get(url[,config])
    ```

* `delete` 请求：

    ```javascript
    axios.delete(url[,config])
    ```

* `head` 请求：

    ```javascript
    axios.head(url[,config])
    ```

* `options` 请求：

    ```javascript
    axios.option(url[,config])
    ```

* `post` 请求：

    ```javascript
    axios.post(url[,data[,config])
    ```

* `put` 请求：

    ```javascript
    axios.put(url[,data[,config])
    ```

* `patch` 请求：

    ```javascript
    axios.patch(url[,data[,config])
    ```

### this 作用域问题

使用请求方法别名和箭头函数可以解决this作用域问题，防止this指向错误：

- GET请求：

    ```javascript
    axios.get(
        "http://localhost:8080/axios-demo/demo?message=Hello World"
    ).then(resp => {
        // 对响应数据进行处理
        alert(resp.data);
    })
    ```

- POST请求：

    ```java
    axios.post(
        "http://localhost:8080/axios-demo/demo",
        "message=Hello World"
    ).then(resp => {
        // 对响应数据进行处理
        alert(resp.data);
    })
    ```

# JSON

JSON（JavaScript Object Notation）是指 JavaScript 对象表示法。

JSON的优点：数据格式简单，所占的字节数少等。

## JSON 的格式

`JSON` 本质就是一个字符串，但是该字符串内容是有一定的格式要求的：

```json
{
	"key_1": value_1,
	"key_2": value_2,
	"key_3": value_3,
    ...,
    "key_n": value_n
}
```

- `JSON` 格式中的键要求必须使用双引号括起来。
- Value 的数据类型分为如下：
    - 数字（整数或浮点数）
    - 字符串（使用双引号括起来）
    - 逻辑值（`true`或者`false`）
    - 数组（在方括号`{}`中）
    - 对象（在花括号`[]`中）
    - null

在 js 中定义 json：

```js
var 变量名 = `{
        "key1":value1,
        "key2":value2,
        ...
    }`;
```

## 在 JS 中使用 JOSN

JS 提供了一个对象 `JSON` ，该对象有如下两个方法：

- `parse(str)`：将 JSON串转换为 js 对象。

    使用方式： 

    ```js
    var jsObject = JSON.parse(jsonStr);
    ```

- `stringify(obj)`：将 js 对象转换为 JSON 串。

    使用方式：

    ```js
    var jsonStr = JSON.stringify(jsObject)
    ```

### 携带JSON发送异步请求

可以使用 `JSON.stringify()` 将js对象转换为 `JSON` 串，再将该 `JSON` 串作为 `axios` 的 `data` 属性值进行请求参数的提交。

但其实只需要将需要提交的参数封装成 js 对象，并将该 js 对象作为 `axios` 的 `data` 属性值。`axios` 会自动将 js 对象转换为 `JSON` 串进行提交。

> 发送异步请求时，如果请求参数是 `JSON` 格式，那请求方式必须是 `POST`。因为 `JSON` 串需要放在请求体中。

## Fastjson

`Fastjson` 是阿里巴巴提供的一个Java语言编写的高性能功能完善的 `JSON` 库，是目前Java语言中最快的 `JSON` 库，可以实现 `Java` 对象和 `JSON` 字符串的相互转换。

依赖坐标：

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.62</version>
</dependency>
```

JOSN 和 Java对象的相互转换包含：

- 请求数据（反序列化）：

    JSON字符串转为Java对象。

    将 json 转换为 Java 对象，只需要使用 `Fastjson` 提供的 `JSON` 类中的 `parseObject()` 静态方法即可。
    
    ```java
    String toJSONString(Object object)
    ```

- 响应数据（序列化）：

    Java对象转为JSON字符串。

    将 Java 对象转换为 JSON 串，只需要使用 `Fastjson` 提供的 `JSON` 类中的 `toJSONString()` 静态方法即可。

    ```java
    <T> T parseObject(String jsonStr, Class<T> clazz)
    ```

Example：

- 导入坐标。
- 编写`pojo`类：

    ```java
    package pojo;

    public class User {
        private Integer id;
        private String username;
        private String password;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
    ```
    
- 编写`FastJsonDemo`：

    ```java
    package web.servlet;

    import com.alibaba.fastjson.JSON;
    import pojo.User;

    public class FastJsonDemo {
        public static void main(String[] args) {
            // 1. Java对象转JSON字符串
            User user = new User();
            user.setId(1);
            user.setUsername("zhangsan");
            user.setPassword("123");

            String jsonString = JSON.toJSONString(user);
            System.out.println(jsonString);
            // 输出：{"id":1,"password":"123","username":"zhangsan"}

            // 2. 将JSON字符串转为Java对象
            User u = JSON.parseObject(
                    "{\"id\":1,\"password\":\"123\",\"username\":\"zhangsan\"}",
                    User.class);
            System.out.println(u);
        }
    }
    ```


