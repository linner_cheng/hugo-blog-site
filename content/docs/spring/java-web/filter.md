---
title: 'Filter 过滤器'
linkTitle: 'Filter'
date: 2022-09-25 00:00:00
tags: [Java,JavaWeb,Tomcat,Maven]
authors: Linner
categories: [note]
navWeight: 91
series: [Spring]
---

# Filter 概述

Filter 表示过滤器，是 JavaWeb 三大组件(Servlet、Filter、Listener)之一。

过滤器可以把对资源的请求拦截下来，从而实现一些特殊的功能。设置 Filter 会在访问资源之前会先经过 Filter，将资源拦截，从而实现权限控制、统一编码处理、敏感字符处理等功能。

过滤器Filter是在请求进入容器后，但在进入Servlet之前进行预处理。

进行 `Filter` 开发分成以下三步实现：

-  定义类，实现 Filter接口，并重写其所有方法：

    ```java
    package filter;

    import javax.servlet.*;
    import javax.servlet.annotation.WebFilter;
    import java.io.IOException;

    // 拦截的路径，这里 "/*" 拦截了项目中所有资源请求
    @WebFilter("/*")
    public class FilterDemo implements Filter {
        
        // Filter的初始化方法
        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
            // ...
        }

        // Filter的操作方法，在init后执行
        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            // ...
        }

        // 销毁Filter执行的方法
        @Override
        public void destroy() {
            // ...
        }
    }
    ```

- 配置Filter拦截资源的路径：

    在类上定义 `@WebFilter` 注解。

    此处注解的 `value` 属性值 `/*` 表示拦截所有的资源。

    ```java
    package filter;

    // import ...

    // 拦截的路径，这里 "/*" 拦截了项目中所有资源请求
    @WebFilter("/*")
    public class FilterDemo implements Filter {
        // ...
    }
    ```

- 在`doFilter()`方法中对拦截的资源进行处理，并选择是否放行：

    ```java
    package web.filter;

    // import ...

    // 拦截的路径，这里 "/*" 拦截了项目中所有资源请求
    @WebFilter("/*")
    public class FilterDemo implements Filter {
        
        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

            // 1. 放行前，对request数据进行处理
            // ...
            // 放行前response中没有数据

            // 2. 放行
            filterChain.doFilter(servletRequest, servletResponse);
            // 调用该方法，会执行其它拦截器或访问路径中对应的Servlet或访问对应资源
            // 如果该方法没有被调用，那么对应资源将不可被访问，且后续的其它拦截器都不会被执行

            // 3. 放行后，对response数据进行处理
            // ...
        }

        // ...
    }
    ```

项目依赖：

```xml
<packaging>war</packaging>

<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
</properties>

<dependencies>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.apache.tomcat.maven</groupId>
            <artifactId>tomcat7-maven-plugin</artifactId>
            <version>2.2</version>
            <configuration>
                <port>80</port>
            </configuration>
        </plugin>
    </plugins>
</build>
```

> 在`doFilter()`中，如果要使用`request`和`response`，可以将它们强转为对应的`HttpServletRequest`和`HttpServletResponse`对象来进行操作。

---

# Filter 执行流程

Filter 的执行流程如下：

- 浏览器访问资源
- 资源被Filter拦截

    Filter 执行放行前逻辑

- 判断是否放行
    - 放行：

        可能会执行以下操作：
        
        - 如果有，则执行后续的Filter
        - 如果资源没有被后续其它Filter拦截，那么访问对应资源
    - 不放行：

        后续的其它 Filter 将不会被执行，资源也不可被访问
- Filter 执行放行后逻辑

---

# Filter 拦截路径配置

拦截路径表示 Filter 会对请求的哪些资源进行拦截，使用 `@WebFilter` 注解进行配置。

拦截路径有如下四种配置方式：

1. 拦截具体的资源：`/index.html`，只有访问`index.html`时才会被拦截
2. 目录拦截：`/user/*`，访问`/user`下的所有资源，都会被拦截
3. 后缀名拦截：`*.html`，访问后缀名为jsp的资源，都会被拦截
4. 拦截所有：`/*`，访问项目下所有资源，都会被拦截

---

# 过滤器链

过滤器链是指在一个Web应用，可以对同个拦截路径配置多个过滤器，这多个过滤器组成了过滤器链。

过滤器链基于函数回调，多个过滤器之间的优先级顺序以过滤器类名的自然顺序（A-Z 0-9）为规则进行排序。

例如，有`Filter1`和`Filter2`两个拦截路径相同的Filter：

1. 执行 `Filter1` 的放行前逻辑代码
2. 执行 `Filter1` 的放行代码
3. 执行 `Filter2` 的放行前逻辑代码
4. 执行 `Filter2` 的放行代码
5. 访问到资源
6. 执行 `Filter2` 的放行后逻辑代码
7. 执行 `Filter1` 的放行后逻辑代码
