---
title: 'Tomcat'
date: 2022-09-10 00:00:00
authors: Linner
tags: [Java, JavaWeb, Tomcat,Maven]
categories: [memo]
navWeight: 96
series: [Spring]
---

# Tomcat基本使用

## Tomcat安装

访问[Tomcat官网](https://tomcat.apache.org/)下载。Tomcat是绿色版，直接解压即可。

## 启动Tomcat

以Windows系统为例，运行Tomcat目录下 `bin\startup.bat` 启动脚本即可完成启动。

启动后，通过浏览器访问 `http://localhost:8080`能看到Apache Tomcat的内容就说明Tomcat已经启动成功。

Windows系统下启动的过程中，如果控制台有中文乱码，需要修改`conf/logging.prooperties`。

将

```
java.util.logging.ConsoleHandler.encoding = UTF-8
```

修改为

```
java.util.logging.ConsoleHandler.encoding = GBK
```

## 关闭Tomcat

关闭Tomcat不应该直接关闭运行窗口来强制关闭。应该运行`bin\shutdown.bat`或`ctrl+c`来正常关闭。

## 修改端口

Tomcat默认的端口是8080，通过修改 `conf/server.xml` 来修改端口号：

```xml
<Connector port="8080" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443">
```

## 部署

Tomcat部署项目： 将项目放置到`webapps`目录下，即部署完成。

一般JavaWeb项目会被打包称`wa`r包，然后将`war`包放到`webapps`目录下，Tomcat会自动解压缩。

---

# Maven 创建 Web 项目

使用Maven工具能更加简单快捷的创建Web项目。

## Maven Web 项目结构

- 📁`project_name` --- 项目主目录
  - 📁`src` --- 源代码和测试代码文件目录
    - 📁`main` --- 源代码文件目录
      - 📁`java` --- 源代码Java文件目录
      - 📁`resourcs` --- 源代码配置文件目录
      - ❗📁`webapp` 或 `web` --- Web项目核心目录
        存放web配置文件和web项目代码 (`html`, `css`, `javascript`等)
        - ❗📁`WEB-INF` --- Web项目核心目录
            - 📄`web.xml` --- Web项目配置文件
        - 其它目录或文件，如 `index.html`、`html`、`css`、`js`等
    - 📁`test` --- 测试代码文件目录
      - 📁`java` --- 测试代码Java文件目录
      - 📁`resource` --- 测试代码配置
    - 📄`pom.xml` --- 项目核心配置文件

开发完成部署的 Maven Web 项目结构：

- 📁`project_name` --- 项目访问路径（虚拟目录）
  - ❗📁`WEB-INF` --- Web项目核心目录
    - 📁`classes` --- Java字节码文件
      编译后的Java字节码文件和`resources`的资源文件，会被放到该目录下
      包括`pom.xml`中依赖坐标对应的jar包
    - 📁`lib` --- 项目所需jar包
    - 📄`web.xml` --- Web项目配置文件
    - 其它目录或文件，如 `index.html`、`html`、`css`、`js`等

## 创建 Maven Web 项目

创建方式有两种：使用骨架和不使用骨架

使用骨架大致步骤如下：

1. 创建Maven项目
2. <mark>选择使用Web项目骨架</mark>

  ![](1627227650406.png)

3. 输入Maven项目坐标创建项目
4. 确认Maven相关的配置信息后，完成项目创建
5. <mark>删除pom.xml中多余内容</mark>
  ![](1627228584625.png)
6. 补齐Maven Web项目缺失的目录结构

   最终目录结构如下：

   - 📁`project_name` 
      - 📁`src` 
         - 📁`main` 
            - 📁`java` 
            - 📁`resourcs` 
            - 📁`webapp`
               - 📁`WEB-INF` 
                    - 📄`web.xml`
        - 📄`pom.xml`

![](1627204022604.png)

不使用骨架大致步骤如下：

1. 创建Maven项目
2. 选择<mark>不使用Web项目骨架</mark>

  按照正常的方式创建，不勾选`Create from archetype`

3. 输入Maven项目坐标创建项目
4. <mark>在pom.xml设置打包方式为war</mark>
  
   ```xml
   <project>
     <!-- ...... -->
     <groupId>......</groupId>
     <artifactId>.......</artifactId>
     <vesion>.......</vesion>
     <packaging>war</packaging>
     <!-- ...... -->
   </project>
   ```

5. <mark>补齐Maven Web项目缺失webapp的目录结构</mark>

  文件-->项目结构-->Facet中按`+`然后选择`Web`，选择当前项目

  修改webapp目录，并补齐Maven Web项目缺失`WEB-INF/web.xml`的目录结构，即可完成构建

![](1627204076090.png)

---

# 在IDEA中使用Tomcat

在IDEA中集成使用Tomcat有两种方式，分别是集成本地Tomcat和Tomcat Maven插件。

## 集成本地Tomcat

1. 右上角 编辑运行/调试配置 对话框-->编辑配置-->`+`-->Tomcat服务器-->`本地`

  ![](1627229992900.png)

2. 指定本地Tomcat的具体路径

  ![](1627230313062.png)

3. 将开发项目部署项目到Tomcat中

  ![](1627230913259.png)

> `xxx.war` 和 `xxx.war exploded` 这两种部署项目模式的区别：
>
> - war模式是将WEB工程打成war包，把war包发布到Tomcat服务器上
> - war exploded模式是将WEB工程以当前文件夹的位置关系发布到Tomcat服务器上
>
> war模式部署成功后，Tomcat的webapps目录下会有部署的项目内容
>
> war exploded模式部署成功后，Tomcat的webapps目录下没有，而使用的是项目的target目录下的内容进行部署
>
> 建议选war模式进行部署，更符合项目部署的实际情况

![](1627205657117.png)

## Tomcat Maven插件

1. 在`pom.xml`中添加Tomcat插件：

   ```xml
   <build>
       <plugins>
       	<!--Tomcat插件 -->
           <plugin>
               <groupId>org.apache.tomcat.maven</groupId>
               <artifactId>tomcat7-maven-plugin</artifactId>
               <version>2.2</version>
           </plugin>
       </plugins>
   </build>
   ```
   
2. 使用Maven Helper插件快速启动项目，选中项目，右键 -->Run Maven --> tomcat7:run

> 安装Maven Helper插件：File --> Settings --> Plugins --> Maven Helper ---> Install，重启IDEA
