---
# type: docs 
title: Gin Cookie 和 Session
date: 2024-07-02T14:49:41Z
featured: false
draft: true
authors: Linner
navWeight: 96
categories: [note]
tags: [Golang, Go, Gin, Session]
---

# Cookie

在 Gin 中，使用 Cookie 涉及到两个方法，`Context.Cookie()` 和 `Context.SetCookie()`：

```go
r.GET("/cookie", func(c *gin.Context) {
	cookie, err := c.Cookie("gin_cookie")

	if err != nil {
		cookie = "NotSet"
		c.SetCookie("gin_cookie", "test", 3600, "/", "localhost", false, true)
	}

	fmt.Printf("Cookie value: %s \n", cookie)
})
```

其中：

- `Context.Cookie()`：获取指定名称的 Cookie。
- `Context.SetCookie()`：设置 Cookie。它的方法签名如下：

	```go
	func (c *Context) SetCookie(name, value string, maxAge int, path, domain string, secure, httpOnly bool) {
		// ...
	}
	```

	参数解释如下：

	- `name`：设置 Cookie 的名称。
	- `value`：设置 Cookie 的值。
	- `maxAge`：Cookie 的最大存活时间，单位为秒。如果为负数，则表示会话 Cookie（在浏览器关闭之后删除）；如果为零，则表示立即删除Cookie。
	- `path`：Cookie 的生效路径。如果为空字符串，则使用当前请求的 URI 路径作为默认值。如果是 `"/"`，那么所有路径都访问该 Cookie。
	- `domain`：Cookie 的生效域。如果为空字符串，则不设置域名。
	- `secure`：Cookie 是否仅用于 HTTPS 连接。如果为 `true`，则仅通过 HTTPS 连接发送 Cookie；否则，使用 HTTP 或 HTTPS 连接都可以发送 Cookie。
	- `httpOnly`：Cookie 是否允许通过 JavaScript 访问。如果为 `true`，则无法通过 JavaScript 访问 Cookie；否则，可以通过 JavaScript 访问 Cookie。

---

# Session

Gin 是一个轻量的 Web 框架，在 Gin 中并不直接对 Session 提供支持，但是可以通过其他第三方中间件让 Gin 支持 Session。

在使用 Session 前，需要 `get` 对应的模块：

```bash
go get github.com/gin-contrib/sessions
```

然后在需要使用到 Session 的地方引入：

```go
import "github.com/gin-contrib/sessions"
```

接着设置 Session 中间件：

```go
store := cookie.NewStore([]byte("secret"))
r.Use(sessions.Sessions("mysession", store))
```

具体示例如下：

```go
func (sessionsController) SetSession(ctx *gin.Context) {
	// 从 Context 中获取 Session 数据
	session := sessions.Default(ctx)
	// 设置 Session
	session.Set("username", "lisi")
	session.Set("nickname", "李四")

	// 保存设置的 Session，设置完成之后必须要调用 Save()
	if err := session.Save(); err != nil {
		logrus.Error(err)
		panic(err)
	}

	NewResponseModel(ctx).OK()
}

func (sessionsController) GetSession(ctx *gin.Context) {
	// 从 Context 中获取 Session 数据
	session := sessions.Default(ctx)
	// 获取 Session
	username := session.Get("username")
	nickname := session.Get("nickname")

	model := NewResponseModel(ctx)
	model.Date = gin.H{
		"username": username,
		"nickname": nickname,
	}
	model.OK()
}

var store = cookie.NewStore([]byte("secret"))

func main() {
	r := gin.Default()

	// 设置 Session 中间件
	r.Use(sessions.Sessions("mysession", store))

	r.GET("/session/set", SetSession)
	r.GET("/session/get", GetSession)

	if err := r.Run(":8080"); err != nil {
		panic(err)
	}
}
```

## Redis

`github.com/gin-contrib/sessions` 支持通过 Redis 缓存 Session，使其能够支持分布式系统。

```go
func redisStore() (store sessions.Store) {
	store, err := redis.NewStore(10, "tcp",
		"localhost:6379", "123456", []byte("secret"))
	if err != nil {
		panic(err)
	}
	return
}

var store = redisStore()

func main() {
	// ...
	r.Use(sessions.Sessions("mysession", store))
	// ...
}
```
