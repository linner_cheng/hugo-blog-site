---
# type: docs 
title: 'Vue 入门'
linkTitle: '入门'
date: 2023-08-19T07:36:10Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 100
authors: Linner
---

Vue.js 是一套构建用户界面的渐进式框架。Vue 只关注视图层，采用自底向上增量开发的设计。Vue 的目标是通过尽可能简单的 API 实现响应的数据绑定和组合的视图组件。

在学习Vue之前需要了解HTML、CSS和JavaScript。

Vue的特点：

- 采用组件化模式，提高代码复用率、且让代码更好维护。
- 采用声明式编码，让编码人员无需直接操作DOM，提高开发效率。

    使用传统JS操作DOM属于命令式编码。

- 使用虚拟DOM和优秀的Diff算法，尽量复用DOM节点。

Vue官网为：[https://cn.vuejs.org/](https://cn.vuejs.org/)。

---

# 安装 Vue

安装Vue的方式有很多，可以在[Vue2文档](https://v2.cn.vuejs.org/v2/guide/installation.html)中查阅。

其中最简单的方式就是通过`<script>`标签引入。通过`<script>`标签，可以直接使用Vue CDN引入：


```html
<script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
```

通常引入的Vue有两种版本：

- 开发版本：包含完整的警告和调试模式。如上方引入的即为`vue@2.7.14`的开发版本。
- 生产版本：删除了警告。通常Vue开发版本的文件名为`vue.min.js`。

    例如：

    ```html
    <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.min.js"></script>
    ```

在使用Vue时，通常会在浏览器中安装Vue的开发者工具插件：[https://github.com/vuejs/devtools#vue-devtools](https://github.com/vuejs/devtools#vue-devtools)。

---

# Hello

下方演示了Vue的基本使用方式：

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Hello Vue</title>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
</head>
<body>
  <div id="root"> <!-- Vue 容器 -->
    <h1>Hello, {{name}}<!-- 插值表达式 -->!</h1>
  </div>

  <script type="text/javascript">
    /*
      阻止 vue 在启动时生成生产提示
      Vue 2.x 中可能不会生效，直接修改 Vue 源码
     */
    Vue.config.productionTip = false

    // 创建 Vue 实例
    new Vue({
      el: '#root',  // 将实例与容器进行绑定
      data: { // data 中用于存储数据，存储的数据供 el 所指定的容器使用
        name: 'Vue.js'
      }
    })
  </script>
</body>
</html>
```

Hello实例中的一些关键点：

- Vue 容器：
    - 容器中的代码依然符合 HTML 规范，只不过混入了一些特殊的 Vue 语法。
    - 容器中的代码被称为 Vue模板。
    - 插值语法：在HTML标签体中，使用两个花括号`{{ }}`将JS表达式引起来。与语句不同的是，表达式指可以生成值的语句，这个语句可以放在任何需要值的地方。

        也就是说插值表达式一定需要有值（或返回值）。

        插值表达式中的变量、方法都是在对应 Vue实例 的 `data` 中定义的。当`data`发生了改变，页面中对应的数据也会发生改变。

        > 插值是，Vue模板语法中的一种。

- Vue 实例：Vue 实例用于解析 Vue 容器，Vue 实例会根据配置对象的信息去解析对应的模板。创建Vue 实例时需要传入一个配置对象。
    - `el`属性：将实例与容器进行绑定，`el` 的值通常为 CSS 选择器字符串。

        `el`属性中传入的值也可以是具体的Element对象，例如：

        ```javascript
        new Vue({
            el: document.getElementById('root') // el 还可以这么绑定
        })
        ```
    
    - `data`：`data` 中用于存储数据（对象或方法），存储的数据供 `el` 所指定的容器使用。`data` 可以是对象或函数。

    Vue实例和Vue容器是一对一关系。当一个实例的`el`配置有多个与之对应的容器时，Vue不会报错，但页面显示会有问题；当一个容器被多个实例指定时，Vue会报错。

    真实开发中只有一个Vue实例，并且会配合着组件一起使用。

---

# 创建 Vue 实例的另一种方式

在创建Vue实例时，可以不用立即指定`el`来绑定容器。

可以通过`vm.$mount()`来绑定容器：

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
</head>
<body>
  <div id="app">
    <h1>Hello {{name}}!</h1>
  </div>

  <script type="text/javascript">
    const vm = new Vue({
      data: {
        name: 'Linner',
      },
    });
    // 使用 $mount() 绑定容器
    vm.$mount('#app')
  </script>
</body>
</html>
```

> `vm.$mount()`是Vue原型对象上的一个方法。

---

# 数据对象的函数式写法

Vue实例中的`data`不仅可以作为对象去定义，还能作为函数去定义：

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
</head>
<body>
  <div id="app">
    <h1>Hello {{name}}!</h1>
  </div>

  <script type="text/javascript">
    new Vue({
      el: "#app",
      // 函数式 data，将返回值作为数据对象
      data () {
        /**
         * data() 的 this 是当前的Vue实例对象
         * 定义data()时不要使用箭头函数
         * 如果将data()定义为箭头函数，那么this就不再是当前Vue的实例对象了
         */
        return {
          name: 'Linner'
        }
      },
    });
  </script>
</body>
</html>
```

---

# MVVM 模型

Vue是在MVVM模型的基础上进行设计，MVVM模型包含以下3个部分：

- **M**（Model）：模型，对应Vue实例`data`中的数据。
- **V**（View）：模板，即Vue容器。
- **VM**（View Model）：视图模型，对应Vue实例对象。

![Vue 和 MVVM 模型](uTools_1692692952621.png)

- DOM Listeners：DOM监听器，监听View中数据的改变，并根据Data Bindings指定的关系，将View中被修改的数据在Model中做相应的改变。
- Data Bindings：数据绑定，将View与Model中对应的数据进行绑定，表示视图与模型间的一种对应关系。

在代码中，Vue和MVVM模型的对应关系如下所示：

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Hello Vue</title>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.min.js"></script>
</head>
<body>
  <div id="app"> <!-- View -->
    <h1>Hello, {{name}}!</h1>
  </div>

  <script type="text/javascript">
    // 在代码中，可以使用vm代表Vue实例，vm表示View Model
    const vm = new Vue({ // View Model
      el: '#app',
      data: { // Model
        name: 'Vue.js'
      }
    })

    console.log(vm);
  </script>
</body>
</html>
```
