---
# type: docs 
title: 'Vue 模板语法'
linkTitle: '介绍'
date: 2023-08-19T10:36:30Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 100
authors: Linner
---

Vue.js 使用了基于 HTML 的模板语法，允许开发者声明式地将 DOM 绑定至底层 Vue 实例的数据。结合响应系统，在应用状态改变时， Vue 能够智能地计算出重新渲染组件的最小代价并应用到 DOM 操作上。

Vue中模板语法有以下两种类型：

- 插值语法：用`{{}}`包裹起来的表达式。

    Vue插值语法中，可以指定JS表达式将数据与模板进行绑定。

    与语句不同的是，插值表达式指可以生成值的语句，这个语句可以放在任何需要值的地方。也就是说插值表达式一定需要有值（或返回值）。表达式中的变量、方法都是在对应Vue实例的 `data` 中定义的。当`data`发生了改变，页面中对应的数据也会发生改变（动态）。

- 指令语法：以`v-`为开头的Vue指令。
