---
# type: docs 
title: 'Vue 插值语法'
linkTitle: '插值语法'
date: 2023-08-19T10:36:30Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 99
authors: Linner
---

# 文本插值

数据绑定最常见的形式就是**文本插值**。

在HTML标签体中，使用两个花括号`{{ }}`将JS表达式引起来。文本插值含义是将JS表达式的结果作为文本显示到页面中；并不是指JS表达式的结果为文本。

插值表达式的语法如下：

```html
{{expression}}
```

其中，`expression`将作为JS表达式去解析并执行。示例如下：

```html
<div id="app">
  <h1>Hello, {{name}}<!-- 插值表达式 -->!</h1>
</div>

<script type="text/javascript">
  new Vue({
    el: '#app',
    data: {
      name: 'Vue.js'
    }
  })
</script>
```

因为文本插值使用的是JS表达式，所以在`{{ }}`中，也可以写入`1+1`这类常量表达式，也可以调用方法（例如`name.toUpperCase()`），还可以使用`size+1`等表达式。

实际上，插值表达式中可以使用的对象不仅仅是`data`中的对象，所有Vue实例中的属性、原型中属性的它都能使用。因为`data`中的对象在Vue实例化后，是作为Vue实例的属性所存在。

---

# 过滤器

Vue允许自定义过滤器，被用作一些常见的文本格式化。过滤器由“管道符”`|`指示，并且过滤器可以串联，其格式如下：

```html
<!-- 元素插值中 -->
{{ expression | filter1[ | filter2[ | ... ]] }}

<!-- v-bind 指令中 -->
v-bind:attribute="expression | filter1[ | filter2[ | ... ]]"
```

使用过滤器时，`|`左边表达式的值（或输出）将会作为`|`右边过滤器的（第1个参数的）输入。

过滤器一般是在Vue实例配置中的`filters`中以函数的形式定义。例如：

```html
<div id="app">
  <h1>{{ message | capitalize }}</h1>
</div>
<script type="text/javascript">
  new Vue({
    el: "#app",
    data: {
      message: 'hello vue.js!'
    },
    filters: {
      capitalize(value) {
        if(!value) return ''
        value = value.toString()
        return value.charAt(0).toUpperCase() + value.slice(1)
      },
    },
  });
</script>
```

如果过滤器的定义中，拥有两个或两个以上的参数，那么在使用过滤器时，`|`左边的表达式的值将会作为其右边过滤器的第1个参数进行输入，例如：

```
{{ message | filter(arg1, arg2) }}
```

`arg1`作为`filter()`的第2个参数输入，`arg2`作为`filter()`的第3个参数输入，而`message`将作为`filter()`的第1个参数进行输入。

> 注：
>
> 1. 过滤器在定义时，一定至少要定义有一个参数`value`，因为过滤器始终会将管道符`|`左边的表达式的结果作为参数1`value`传入。如果不定义参数`value`，那么过滤器将显得无意义。
> 2. 过滤器不能在`v-model`指令中使用。
> 3. 一般的过滤器不会改变原本的数据。

在Vue实例中配置的`filters`只能在当前Vue实例中使用，这种过滤器被称为局部过滤器。当应用中存在多个Vue实例时，可以使用全局过滤器。全局过滤器使用`Vue.filter()`来注册。例如：

```javascript
Vue.filter('capitalize', function (value) {
  if(!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})
```

`Vue.filter()`有两个参数，它们的作用分别是：

1. 参数1：定义过滤器的名称。
2. 参数2：定义过滤器的方法。
