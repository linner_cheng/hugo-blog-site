---
# type: docs 
title: Vue 编程式路由导航
linkTitle: 编程式导航
date: 2023-09-26T13:19:57+08:00
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 96
authors: Linner
---

编程式路由导航是指不通过`<router-link>`创建`<a>`标签来定义导航链接，而是借助路由组件实例中的`$router`实例，通过编写代码来实现路由导航。

使用编程式路由导航，可以让路由的跳转方式变得更加灵活。

---

# 两种导航方式

`$router`也有与`<router-link>`相同两种导航方式，分别是`push`和`replace`。这两种方式分别调用两个不同的API，不过这两个API的使用方式是一致的。并且他们传入的参数与`<router-link>`中`to` props的值一样，可以传入两种类型的参数。

- `$router.push()`：

  ```JavaScript
  this.$router.push('/user/zhangsan')
  ```

  ```JavaScript
  router.push({
    name: 'user', 
    params: { 
      name: 'zhangsan' 
    } 
  })
  ```

- `$router.replace()`：

  ```JavaScript
  this.$router.replace('/user/zhangsan')
  ```

  ```JavaScript
  router.replace({
    name: 'user', 
    params: { 
      name: 'zhangsan',
    }
  })
  ```

一般情况下，`push`和`replace`的区别是，`replace`模式不会向`history`添加新记录。

`$router.push()`可以在配置对象中加入一个`replace:true`，起到与`$router.replace()`相同的效果：

```JavaScript
router.push({
  path: '/user/zhangsan', 
  replace: true,
})
// 相当于
router.replace({
  path: '/user/zhangsan', 
})
```

---

# 移动历史记录

`$router`还封装了一些用于操作当前应用历史浏览记录的API：

- `$router.forward()`：向前移动一条记录。
- `router.back()`：回退上一条记录。
- `$router.go(n)`：携带一个整数类型的参数，用于在历史堆栈中前进后退$n$步（类似于`window.history.go(n)`）。
  - 当$n$为正整数时，在历史堆栈中前进$n$步。
  - 当$n$为负整数时，在历史堆栈中后退$|n|$步。
  - 如果移动的步数超过历史堆栈的长度（$|n|$过大），那么移动静默失败。

  例如：

  - `$router.go(1)`：向前移动一条记录。与`$router.forward()`相同。
  - `$router.go(-1)`：回退上一条记录。与`$router.back()`相同。
  - `$router.go(3)`：向前移动3条记录。
