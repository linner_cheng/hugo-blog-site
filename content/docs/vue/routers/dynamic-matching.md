---
# type: docs 
title: Vue 动态路由匹配
linkTitle: 动态路由匹配
date: 2023-09-25T04:27:20Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 98
authors: Linner
---

在Vue Router中，路由路径有以下几种匹配形式：

- **静态匹配**：即完全按照`path`中的内容，进行相等匹配。
- **动态匹配**：根据`path`中定义的路径匹配规则匹配多个路径。

---

# 路径参数

Vue中的动态参数匹配使用的是**路径参数匹配**。路径参数是路径中的一个动态字段。路径参数的值会动态变化，根据这些路径参数的值，可以读取相应的数据或展示相应的页面。

例如，有个`User`组件对所有用户进行渲染，但用户ID不同。使用路径参数，可以让用户ID像路径中的某个资源或目录一样在URL中展示。变换不同的用户ID，就像切换目录一样路由到不同的用户界面。

使用路径参数包含3个步骤：

- 在Router配置文件（`router/index.js`）中，配置路径参数的匹配规则。
- 使用`<router-link>`传递路径参数。
- 在对应的路由组件中使用`$route.params`接收路径参数。

## 配置路径参数匹配规则

在Router配置文件的`route`元素或`children`中的`path`属性上配置当前路由的路径参数匹配规则。

例如，在`User`路由组件中使用路径参数，其路由配置如下：

```JavaScript
/* ... */
import User from '../pages/User.vue'

export default new VueRouter({
  routes: [
    {
      path: '/user/:id',
      component: User,
    },
  ],
})
```

如上`User`的路由规则可以使用`/user/zhangsan`、`/user/lisi`等方式映射。

将[嵌套路由 —— 路由 Query 传参](../nested-routes/#路由-query-传参)中的`MessageDetail`改为使用路径参数形式传参，其路由配置如下：

```JavaScript
/* ... */

export default new VueRouter({
  routes: [
    /* about route... */
    {
      path: '/home',
      component: Home,
      children: [
        /* news route... */
        {
          path: 'message',
          component: Message,
          children: [
            {
              path: ':id/:title', // 路径传参
              component: MessageDetail,
            }
          ],
        },
      ]
    },
  ],
})
```

在`MessageDetail`的`path`中，使用了两个以`:`为开头的占位符（`:id`和`:title`）。`:`后是当前路径参数的名称（`id`和`title`）。

---

## 传递路径参数

传递路径参数的方式与传递Query参数的方式十分相同。传递路径参数也有两种方式：

- 字符拼接：

  ```html
  <router-link :to="`/home/message/${message.id}/${message.title}`">
  ```

- 传入对象：与传递Query参数的方式基本相同。不同的是，传递路径参数需要配置的是`to` prop对象中`params`属性。并且传递路径参数时不能使用`path`属性指定路由，必须使用`name`来指定路由。

  ```html
  <router-link :to="{
    name: 'MessageDetail',
    params: {
      id: message.id,
      title: message.title,
    },
  }">
    {{ message.title }}
  </router-link>
  ```

  在`params`中配置传递的参数以及它们的值。`params`属性的`key`是传递的参数的名称（对应路由配置中占位符里`:`后的内容），`value`是传递的参数的值（就像Axios的`params`配置那样）。

## 接收路径参数

接收路径参数的形式与接收Query参数的形式也十分相似。在对应的路由组件实例中，使用`$route.params`来接收并获取这些路径参数。

接上例，在`MessageDetail`中接收并获取路径参数：

```JavaScript
computed: {
  id() {
    return this.$route.params.id
  },
  title() {
    return this.$route.params.title
  }
}
```

在路由实例中，通过`$route.params.argName`的形式获取路径参数的值。

> 注：
>
> 获取路径参数的形式很固定，为了使代码更加简介、书写更加简便，可以如上使用`computed`属性来接收它们的值。当然还有其它更简便的方法。
>
> 路径参数需要在路由的`path`中使用占位符配置，否则Vue Router不会接收到这些路径参数。
