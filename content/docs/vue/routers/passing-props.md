---
# type: docs 
title: Vue 路由通过 Props 传参
linkTitle: 通过 Props 传参
date: 2023-09-26T10:27:58+08:00
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 97
authors: Linner
---

路由的`props`配置可以让路由组件更方便地接收到参数。在组件中使用`$route`会让组件与路由紧耦合（使用组件必须配置路由）。通过`props`可以解除组件与路由的紧耦合。

> 注：使用`props`，让组件通过`props`接收参数，让路由通过`props`给组件传递参数。这样当组件被用在其它不使用路由的组件中时，组件依然可以通过`props`接收参数，而无需配置路由。

---

# 对象模式

当`props`是一个对象时，它将被作为组件的`props`，原封不动地被传递给组件`props`。在`props`为静态的时候可以使用这种方式。

```JavaScript
routes: [
  {
    path: '/promotion/from-newsletter',
    component: Promotion,
    props: { newsletterPopup: false }
  }
]
```

---

# 布尔模式

在路由中，给`props`设置一个为`true`的布尔值，路由会自动将当前的所有`params`通过`props`传递给组件。

例如`User`组件：

```JavaScript
export default new  VueRouter({
  name: 'User',
  props: ['id'],
  /* ... */
})
```

通过以下方式给`User`配置路由，并以`props`形式将`params`传递给`User`：

```JavaScript
routes: [
  {
    path: '/user/:id',
    component: User,
    props: true,  // 以 props 形式将 params 传递给 User 
  }
]
```

也就是说，当`props`设置为`true`时，`$route.params`将被设置为组件的`props`。

---

# 函数模式

将`props`设置为函数类型，可以在函数中定义传递给组件的`props`。这种方式可以包括将`query`、`parmas`作为`props`传递，传递静态的`props`等等。

函数模式的路由`props`，可以接收到一个`route`参数，这个参数的与其对应的路由组件实例中的`$route`类似，可以通过`route.query`和`route.params`获取Query参数和路径参数。

基本用法示例如下：

```JavaScript
routes: [
  {
    path: '/searchUser',
    component: searchUser,
    props(route) {
      return {
        id: route.query.id,
        name: route.query.name,
      }
    }
  }
]
```

`props`可以使用Lambda表达式定义：

```JavaScript
routes: [
  {
    path: '/searchUsers',
    component: searchUsers,
    props: route => ({
      id: route.query.id,
      name: route.query.name,
    }),
  }
]
```

`props`可以使用解构赋值或多重解构赋值：

```JavaScript
routes: [
  {
    path: '/searchUsers',
    component: searchUsers,
    props: ({query:{id, name}}) => ({id, name}),
  }
]
```

将所有的`query`和`params`作为`props`传递：

```JavaScript
routes: [
  {
    path: '/searchUsers',
    component: searchUsers,
    props: ({query, params}) => ({...query, ...params}),
  }
]
```
