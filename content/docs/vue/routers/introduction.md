---
# type: docs 
title: Vue Router 入门
linkTitle: 入门
date: 2023-09-23T05:27:36Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 100
authors: Linner
---

Vue.js路由（Router）允许通过不同的URL访问不同的内容。通过Vue Router，可以实现SPA（Single Page web Application，单页Web应用）。

SPA是指整个Web应用中，只有一个完整的页面。在点击SPA中的导航链接时，浏览器不会刷新页面，而是做页面的局部更新。Vue SPA的数据通过Ajax请求获取。

在使用Router之前，需要理清SPA中Route和Router之间的关系：

- Route：指的是SPA的路径与页面（组件）之间的一个对应关系。通常是将路径作为某条特定Route的`key`，将页面作为该Route的`value`。

  > 在前端路由中，Route Value指的是页面（组件）；在后端路由中，Route Value指的是Function。

- Router：指的是控制整个SPA中所有的Routes的一个角色。Router需要根据所有的Routes，通过Route Key来给出对应的Route Value。

---

# 安装 Vue Router

在Node.js环境中安装`vue-router`：

```shell
npm i vue-router
```

需要注意的是，兼容Vue2的`vue-router@3`，而当前默认安装的是`vue-router@4`（适用于Vue3），如果在Vue2的环境下，就需要安装`vue-router@3`：

```shell
npm i vue-router@3
```

---

# 配置 Router

安装完成后，首先需要引入Vue Router：

```JavaScript
import VueRouter from 'vue-router'  // 引入 VueRouter
```

接着就是在Vue中应用Vue Router插件：

```JavaScript
Vue.use(VueRouter)  // 应用 VueRouter 插件
```

假设现在要为两个组件`Home`和`About`配置路由，那么其Router的基本配置（配置文件可以存放在项目根目录下的`router/index.js`）如下：

```JavaScript
import Vue from 'vue'
import VueRouter from 'vue-router'  // 引入 VueRouter

// 引入组件
import Home from '../pages/Home.vue'
import About from '../pages/About.vue'

Vue.use(VueRouter)  // 应用 VueRouter 插件

// 创建并导出 Router
export default new VueRouter({
  routes: [
    // 路由规则：
    {
      path: '/about',
      component: About,
    },
    {
      path: '/home',
      component: Home,
    },
    /* ... */
  ],
})
```

在`routes`中指定的每个路由规则都使用对象来定义，这个对象的`path`属性定义路由的路径，`component`属性定义该路径下路由的组件。

路由配置完成后，就可以将其配置在`main.js`（Vue实例）中：

```JavaScript
import Vue from 'vue'
import router from './router' // 引入 Router

/* ... */

Vue.config.productionTip = false

new Vue({
  /* ... */
  router,
  /* ... */
}).$mount('#app')
```

注：

- 通常，路由组件被存放在项目根目录下的`pages`文件夹中，而一般组件通常存放在`components`。
- 使用路由切换页面时，被切换掉的路由组件，默认会被销毁。再次被切换回来时，才去重新挂载。
- 每个路由组件实例对象都有自己的`$route`属性，里面存放着组件自生的路由信息。
- 整个应用（Vue实例）中只有唯一的一个Router，可以通过路由组件实例对象的`$router`属性获取。

---

# 使用 Router

将组件配置在Vue实例之后，就可以在任意组件中使用路由。使用路由时，有两个基本的组件：

- `<router-link>`：在页面中展示路由链接。该组件会被Vue渲染成`<a>`标签。
- `<router-view>`：根据用户打开的路径，渲染对应的组件。可以将其放在任何地方，以适应你的布局。

例如：

```html
<router-link active-class="active" to="/about">
  About
</router-link>
<router-link active-class="active" to="/home">
  Home
</router-link>
```

```html
<router-view></router-view>
```

当点击对应的`<router-link>`，Vue会根据`<router-link>`上的`to`属性，改变应用的路径，并根据`to`属性将对应的组件渲染到`<router-view>`的位置上。

`<router-link>`的`active-class`可以指定一个当`<router-link>`处于活动状态时的CSS样式。Vue会根据情况，自动将该样式应用到对应的元素中。

---

# Router Link 的两种模式

`<router-link>`默认是`push`模式，也就是点击`<router-link>`后，浏览器会将当前浏览记录`push`到当前页面的浏览历史记录栈的栈顶。也就是说，`push`模式的`<router-link>`可以在浏览器中保存访问历史，让当前应用可以根据历史记录前进后退。

`<router-link>`还支持`replace`模式，`replace`模式就是将当前浏览记录替换历史记录栈顶的记录。

> 在导航时，`push`模式会向`history`添加新记录，而`replace`模式不会。

`<router-link>`开启历史记录，可以在`<router-link>`中使用`v-bind`将`replace`设置为`true`，也就是`:replace="true"`。开启`replace`可以使用简便的写法，直接在`<router-link>`中添加`replace`即可。

```html
<router-link 
  replace
  active-class="active" 
  to="/about"
>
  About
</router-link>
```
