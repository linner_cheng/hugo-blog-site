---
# type: docs 
title: Vue Router 两种工作模式
linkTitle: 两种工作模式
date: 2023-09-28T14:09:10+08:00
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 92
authors: Linner
---

Vue Router中有两种工作模式，它们分别是：

- `hash`模式：使用URL的`hash`来模拟一个完整的URL。
  
  `hash`：在URL中，`#`符号及其之后的内容。

  - 当URL改变时，页面不会重新加载。
  - URL中会包含一个`/#/`，不够美观。
  - 在浏览器中，`hash`的内容并不会被浏览器作为请求中的一部分发送给后端服务器。
  - `hash`模式兼容性强。
  
  工作原理：通过监听`hashchange`事件，从而监控`hash`的变化。当`hash`发生变化时，Vue Router会解析`hash`之后的内容，并将对应的结果呈现出来。

- `history`模式（依赖于HTML5）：通过HTML5新增的`history`，将`hash` URL中的`#`符号去除。

  去除了URL中的`#`符号之后，在刷新或者直接使用特定的路径进入页面时，浏览器会直接向服务器请求对应的资源。而Vue开发的是SAP（单页Web应用），所以在上述情况发生时，会造成请求错误。

  解决方法是，在部署时，对前端页面的请求资源路径进行匹配，将所有访问都指向`index.html`。
  
  如果使用的是Express部署页面，可以在Express中使用`connect-history-api-fallback`中间件快速解决。

安装`connect-history-api-fallback`：

```shell
npm i connect-history-api-fallback
```

使用`connect-history-api-fallback`：

```JavaScript
const history = require('connect-history-api-fallback');  // 引入模块
app.use(history())  // 使用中间件（在挂载静态资源之前使用）
```

`hash`模式与`history`模式的比较：

- `hash`模式：
  - URL中永远存在`#`符号，不美观。
  - URL中存在`#`符号，可能会被其它软件标记为不合法。
  - 兼容性较好。
- `history`模式：
  - URL不会永远带着`#`符号，相对较美观。
  - 兼容性不如`hash`模式。
  - 应用部署时需要进行一些调整，以解决刷新及进入特定页面的问题（可能需要后端人员配合）。
