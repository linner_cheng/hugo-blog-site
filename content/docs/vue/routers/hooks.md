---
# type: docs 
title: Vue 路由组件钩子
linkTitle: 路由组件钩子
date: 2023-09-26T16:28:34+08:00
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 94
authors: Linner
---

# 路由组件生命周期钩子

Vue路由组件除了能使用组件的那些生命钩子外，还能使用两个专用于路由组件的生命周期钩子。

当用户打开链接，路由到相应的组件时，这个被路由的组件就呈现激活状态；反之则呈现失活状态。

- `activated()`：在路由组件被激活时自动调用。
- `deactivated()`：在路由组件失活时自动调用。

路由组件的生命周期钩子使用方式与生命周期钩子相同：

```JavaScript
export default {
  activated() {
    /* ... */
  },
  deactivated() {
    /* ... */
  },
}
```

---

# 组件内路由守卫

Vue路由组件钩子除了生命周期钩子外，还有[组件内路由守卫](../guards)。、

组件内路由守卫和生命周期钩子类似，它们都是专门用于路由组件的钩子。
