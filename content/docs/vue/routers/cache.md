---
# type: docs 
title: Vue 路由组件缓存
linkRitle: 路由组件缓存
date: 2023-09-26T15:12:36+08:00
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 95
authors: Linner
---

Vue Router中，开启路由组件缓存可以使用`<keep-alive>`将`<router-view>`包裹起来。使用了`<keep-alive>`之后，路由组件被用户切换掉时，不会被销毁。

```html
<keep-alive>
  <router-view></router-view>
</keep-alive>
```

`<keep-alive>`默认会缓存当前在`<router-view>`中展示的所有组件。如果要指定缓存的组件，可以使用`include`。在`include`中指定要缓存的组件的名称即可。

例如：

```html
<keep-alive include="Home">
  <router-view></router-view>
</keep-alive>
```

如果要指定缓存多个组件，可以使用`v-bind`：

```html
<keep-alive :include="['Home', 'About']">
  <router-view></router-view>
</keep-alive>
```
