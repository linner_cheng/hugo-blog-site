---
# type: docs 
title: 在 Vue 中使用 Element-UI
linkTitle: Element UI
date: 2023-09-28T16:34:08+08:00
draft: false
featured: falses
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 90
authors: Linner
---

# Vue UI 组件库

在Vue中，可以通过引入他人编写好的组件库，来构建自己的页面，提高开发效率。

Vue中常用的UI组件库有：

- 移动端常用UI组件库：
  - [Vant](https://vant-ui.github.io/vant/)
  - [Cube UI](https://didi.github.io/cube-ui/)
  - [Mint UI](https://mint-ui.github.io/)
  - [NutUI](https://nutui.jd.com/)
- PC端常用UI组件库：
  - [Element UI](https://element.eleme.cn/)
  - [IView UI](https://www.iviewui.com/)
  - [Ant Design Vue](https://www.antdv.com/components/overview)

---

# 安装 Element-UI

```shell
npm i element-ui
```

---

# 引入并应用 Element-UI

```JavaScript
import ElementUI from 'element-ui'; // 引入 Element-UI
import 'element-ui/lib/theme-chalk/index.css';  // 引入 Element-UI 的全部样式
Vue.use(ElementUI); // 应用 Element-UI
```

直接使用以上方式引入Element-UI，引入的是Element-UI中所有的组件以及它们的样式和交互。这会使页面的体积变得十分庞大。

在使用Element-UI时，可以根据需要使用的组件，按需引入它们。

引入`babel-plugin-component`开发依赖：

```shell
npm install babel-plugin-component -D
```

接着在`babel.config.js`中追加如下内容：

```JavaScript
{
  "presets": [
    ["@babel/preset-env", { "modules": false }]
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
}
```

然后就可以按需引入项目中所需的组件。例如：

```JavaScript
import { Button, Select } from 'element-ui';  // 引入 Button 和 Select 组件

// 将 Button 和 Select 注册为全局组件
Vue.component(Button.name, Button);
Vue.component(Select.name, Select);
```

注册Element-UI组件也可以使用`Vue.use()`：

```JavaScript
Vue.use(Button)
Vue.use(Select)
```

---

# 使用 Element-UI

Element-UI的使用可直接参考官方文档：[Element UI](https://element.eleme.cn/)。
