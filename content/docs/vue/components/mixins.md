---
# type: docs 
title: 'Vue 混入配置对象'
linkTitle: 混入配置对象
date: 2023-09-18T01:22:04Z
draft: false
featured: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 96
authors: Linner
---

混入（`mixin`）是指将多个组件重复的、可共用的配置选项提取成一个混入对象，然后在这些组件中对这些配置选项进行重用。

---

# 局部混入

局部混入是指在组件或特定的Vue实例中引入通用的配置对象。

例如：

```javascript
/**
 * mixin.js
 * 混入配置对象
 */

// 组件中的配置选项基本上都能在 mixin 中使用
export const mixin1 = {
  methods: {
    showName() {
      alert(this.name)
    }
  },
  mounted() {
    console.log('Hello mixin1!');
  },
}

export const mixin2 = {
  data() {
    return {
      x: 100,
      y: 200,
    }
  },
  mounted() {
    console.log('Hello mixin2!');
  },
}
```

```html
<!-- 
  MySite.vue
 -->
<template>
  <div>
    <h2 @click="showName">网站名称：{{name}}</h2>
    <h2>网站地址：{{url}}</h2>
  </div>
</template>

<script>
// 引入一个混合
import { mixin1, mixin2 } from "../mixin";

export default {
  name: 'MySite',
  data() {
    return {
      name: 'Linner\'s Blog',
      url: 'blog.linner.asia',
      x: 666, // 
    }
  },
  /**
   * mixins 中相同的生命周期函数会以被注册的顺序执行
   */
  mixins: [mixin1, mixin2,],
  /**
   * 配置重复的生命周期函数，它们都会被Vue调用
   * mixins 中的会先被调用
   */
  mounted() {
    console.log('Hello MySite!');
  },
};
</script>
```

---

# 全局混入

全局混入是指在Vue原型对象中引入通用配置对象，这个配置对象在全局生效，项目中所有的Vue实例和组件都会自动启用该配置。

全局混入使用的是`Vue.mixin()`方法。

例如：

```javascript
/**
 * main.js
 */

import Vue from 'vue'
import App from './App.vue'

import { mixin1, mixin2 } from "./mixin";

Vue.config.productionTip = false

// 全局应用 mixin
Vue.mixin(mixin1)
Vue.mixin(mixin2)

new Vue({
  render: h => h(App),
}).$mount('#app')
```

---

# 注意事项

使用`mixin`时需要注意：

- 组件中的配置选项基本上都能在`mixin`中使用。
- 当混入对象和组件中的某些配置（例如`data`、计算属性、`methods`）重复时，以组件中的配置为主。
- 当混入对象和组件中的生命周期函数重复时，它们都会被Vue调用，并且`mixins`中的生命周期函数会先被Vue调用。
- 如果`mixins`中存在相同的生命周期函数，Vue会按照`mixin`在组件`mixins`中被注册的顺序进行调用。
