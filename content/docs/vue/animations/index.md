---
# type: docs 
title: Vue 过渡和动画
linkTitle: 过渡和动画
date: 2023-09-20T09:47:53Z
featured: false
draft: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 95
authors: Linner
---

Vue封装了一套可以快速构建基于状态变化的过渡和过度与动画的组件`<transition>`（`<Transition>`）和`<transition-group>`（`<TransitionGroup>`）。

`<transition>`和`<transition-group>`是Vue内置的组件，无需注册即可使用。它们可以将进入和离开的动画应用到通过默认插槽传递给它的元素或组件上。

它们的进入/离开动画的切换可以由`v-if`或`v-show`所触发。

---

# Transition 组件

`<transition>`用于在一个元素或组件进入和离开DOM时应用动画。

最基本的用法如下：

```html
<template>
<div>
  <button @click="isShow = !isShow">显示/隐藏</button>
  <!-- 
    <transition> 用于给元素自动加上动画效果
   -->
  <transition>
    <h1 v-show="isShow">Hello Vue.js!</h1>
  </transition>
</div>
</template>

<script>
export default {
  name: 'Hello',
  data() {
    return {
      isShow: true,
    }
  },
}
</script>

<style scoped>
h1 {
  background-color: orange;
}

.v-enter-active {
  animation: fade 1s linear;
}

.v-leave-active {
  animation: fade 1s linear reverse;
}

@keyframes fade {
  from {
    transform: translateX(-100%);
  }
  to {
    transform: translateX(0px);
  }
}
</style>
```

当`<transition>`中的元素被显示或隐藏时，Vue会在适当的时机自动将一些CSS过渡`class`往目标元素上添加或移除。

> `<transition>`仅支持单个元素或组件作为其插槽内容。如果内容是一个组件，该组件必须仅有一个根元素。

---

# CSS 过渡 class

Vue中一共有6种应用于进入与离开过度效果的 CSS `class`：

Vue2的CSS过渡`class`：

![Vue2 CSS 过渡 class](1695255059938.png)

Vue3的CSS过渡`class`：

![Vue3 CSS 过渡 class](1695253388660.png)

- `v-enter`（Vue2）/ `v-enter-from`（Vue3）：进入动画的起始状态。在元素插入之前添加，在元素插入完成后的下一帧移除。
- `v-enter-active`：进入动画的生效状态。应用于整个进入动画阶段。在元素被插入之前添加，在过渡或动画完成之后移除。这个`class`可以被用来定义进入动画的持续时间、延迟与速度曲线类型。
- `v-enter-to`（`2.1.8`版及以上）：进入动画的结束状态。在元素插入完成后的下一帧被添加 (也就是`v-enter-from`被移除的同时)，在过渡或动画完成之后移除。
- `v-leave`（Vue2）/ `v-leave-from`（Vue3）：离开动画的起始状态。在离开过渡效果被触发时立即添加，在一帧后被移除。
- `v-leave-active`：离开动画的生效状态。应用于整个离开动画阶段。在离开过渡效果被触发时立即添加，在过渡或动画完成之后移除。这个`class`可以被用来定义离开动画的持续时间、延迟与速度曲线类型。
- `v-leave-to`（`2.1.8`版及以上）：离开动画的结束状态。在一个离开动画被触发后的下一帧被添加 (也就是`v-leave-from`被移除的同时)，在过渡或动画完成之后移除。

> 注：Vue3将过渡类名`v-enter`修改为`v-enter-from`，将过渡类名`v-leave`修改为`v-leave-from`。

---

# 过度效果命名

`<transition>`和`<transition-group>`有一个名为`name`的`prop`，它可以为过渡效果命名，并将该名称作用于其一系列CSS过渡`class`的名称前缀。

例如：

```html
<transition name="fade">
  <!-- ... -->
</transition>
```

那么其一系列的CSS过渡`class`的名称如下：

```css
/* 进入的起点 和 离开的终点 */
.fade-enter /* 或 .fade-enter-from */,
.fade-leave-to {
  transform: translateX(-100%);
}

/* 进入 和 离开的过程 */
.fade-enter-active,
.fade-leave-active {
  transition: 0.5s linear;
}

/* 进入的终点 和 离开的起点 */
.fade-enter-to,
.fade-leave /* 或 .fade-leave-from */ {
  transform: translateX(0);
}
```

---

# 自定义过渡 class

可以向`<transition>`和`<transition-group>`传递以下的`props`来指定自定义的过渡`class`。

- `enter-class`（Vue2）/ `enter-from-class`（Vue3）：定义目标元素<u>进入动画起点</u>的过渡`class`。
- `enter-active-class`：定义目标元素<u>进入动画过程</u>的过渡`class`。
- `enter-to-class`（`2.1.8+`）：定义目标元素<u>进入动画终点</u>的过渡`class`。
- `leave-class`（Vue2）/ `leave-from-class`（Vue3）：定义目标元素<u>离开动画起点</u>的过渡`class`。
- `leave-active-class`：定义目标元素<u>离开动画过程</u>的过渡`class`。
- `leave-to-class`（`2.1.8+`）：定义目标元素<u>离开动画终点</u>的过渡`class`。

传入的这些`class`会覆盖相应阶段的默认`class`名。这个功能在Vue的动画机制下集成其他的第三方CSS动画库时非常有用。

例如使用`animate.css`：

```html
<template>
<div>
  <button @click="isShow = !isShow">显示/隐藏</button>
  <transition
    appear
    name="animate__animated animate__bounce"
    enter-active-class="animate__swing"
    leave-active-class="animate__backOutUp"
  >
    <h1 v-show="!isShow">Hello Vue.js!</h1>
  </transition>
</div>
</template>

<script>
import 'animate.css'

export default {
  name: 'Hello',
  data() {
    return {
      isShow: true,
    }
  },
}
</script>

<style scoped>
h1 {
  background-color: orange;
}
</style>
```

---

# TransitionGroup 组件

`<transition-group>`支持和`<transition>`基本相同的`props`、CSS过渡`class`和JavaScript钩子监听器，但有以下几点区别：

- 默认情况下，它不会渲染一个容器元素。但可以通过传入`tag` prop来指定一个元素作为容器元素来渲染。
- 过渡模式在这里不可用，因为我们不再是在互斥的元素之间进行切换。
- `<transition-group>`中渲染的每个元素都必须有一个唯一的`key`属性值。
- CSS过渡`class`会被应用在列表内的元素上，而不是容器元素上。

`<transition-group>`用于同时渲染多个元素，通常情况下渲染的是列表元素。被`<transition-group>`渲染的每个元素都需要有一个我i唯一的`key`属性值。
