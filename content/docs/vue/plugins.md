---
# type: docs 
title: Vue 插件
linkTitle: 插件
date: 2023-09-18T01:48:20Z
draft: false
featured: false
pinned: false
series: ['Vue']
categories: ['note']
tags: []
navWeight: 94
authors: Linner
---

Vue插件主要用于增强Vue。

---

# 定义插件

在Vue中，插件的本质是一个包含`install()`方法的对象。

`install()`方法：

1. 参数1：Vue原型对象.
2. 参数2 ~ n：第二个参数开始之后的参数（包括第二个）是插件使用者传递的数据。

定义一个简单的插件：

```javascript
/**
 * plugins.js
 * Vue 插件
 */

export const myPlugin = {
  install(Vue) {
    console.log('Hello my plugin!');
    console.log(Vue); // 获取 Vue 原型对象

    /* 
      可以在插件中使用 Vue 原型对象定义一些全局的配置
      例如定义全局过滤器、全局指令、全局混入，在Vue原型添加方法等等...
      甚至可以在插件中启用其它插件
    */
  }
}
```

接收参数：

```javascript
export const plugin = {
  install(Vue, x, y, z) {
    console.log(x, y, z);
  }
}
```

---

# 使用插件

要使用插件需要以下两个步骤：

1. 引入插件。
2. 使用`Vue.use()`方法使插件生效。

`Vue.use()`方法：

1. 参数1：要启用的插件的配置对象。
2. 参数2 ~ n：要传递给插件的参数，这些参数会传递给插件的`install()`中的参数2 ~ n。

例如使用上方定义的插件：

```javascript
/**
 * main.js
 */

import Vue from 'vue'
import App from './App.vue'

// 引入插件
import {myPlugin} from './plugins'
// 使用插件
Vue.use(myPlugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
```

使用时传入参数：

```javascript
Vue.use(myPlugin, 1, 2, 3)
```
