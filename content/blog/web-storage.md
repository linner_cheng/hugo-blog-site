---
# type: docs 
title: 'WebStorage 浏览器本地存储'
date: 2023-09-18T23:39:38Z
draft: false
featured: false
pinned: false
series: 
categories: ['note']
tags: []
authors: Linner
---

WebStorage是指浏览器自带的本地存储。WebStorage存储内容大小一般支持5MB左右（不同浏览器的存储大小可能不一样）。

浏览器通过以下两种属性来实现本地存储：

- `window.sessionStorage`：存储的内容仅在当前会话下有效。浏览器关闭，`sessionStorage`中的内容会被清空。
- `window.localStorage`：存储的内容与会话的关闭无关。浏览器关闭后，`localStorage`中的内容依旧存在。手动清除（例如清空浏览器缓存）才会导致`localStorage`被清空。

`sessionStorage`和`localStorage`是按照网站地址（IP或域名+端口）进行存储。并且`sessionStorage`和`localStorage`存储的内容都是`String`类型。

`window.sessionStorage`和`window.localStorage`的4个API几乎一样，它们分别是：

- `setItem(key, value)`：往`sessionStorage`或`localStorage`中存储一条数据，存储的数据会被解析成`String`类型进行存储。在解析时，`setItem()`调用的是对象的`toString()`方法。

  - `key`：字符串类型的键，用于标识`value`。并且在`sessionStorage`或`localStorage`中，同一个网站地址的`key`是唯一的。

    也就是说，在同一个网站的`sessionStorage`中，不存在两个相同的`key`，如果一个相同的`key`被存储了两次，那么新数据会覆盖旧数据。`localStorage`与`sessionStorage`相同。

  - `value`：要存储的值，可以是任意类型，最终会被解析成字符串形式进行存储。

  ```javascript
  // sessionStorage.setItem():
  sessionStorage.setItem('msg', 'Hello sessionStorage!')
  sessionStorage.setItem('num', 666)
  // 存储对象时，通常手动解析为JSON进行存储
  sessionStorage.setItem('person', JSON.stringify(person))

  // localStorage.setItem():
  localStorage.setItem('msg', 'Hello localStorage!')
  localStorage.setItem('num', 666)
  localStorage.setItem('person', JSON.stringify(person))
  ```

- `getItem(key)`：从`sessionStorage`或`localStorage`中读取对应`key`的一条数据。

  ```javascript
  // sessionStorage.setItem():
  console.log(sessionStorage.getItem('msg'));
  console.log(sessionStorage.getItem('num'));
  const result = sessionStorage.getItem('person')
  console.log(JSON.parse(result));
  // 读取不存在的数据
  console.log(sessionStorage.getItem('user'));  // 返回 null

  // JSON.parse(null)  // 返回 null

  // localStorage.setItem():
  console.log(localStorage.getItem('msg'));
  console.log(localStorage.getItem('num'));
  const result = localStorage.getItem('person')
  console.log(JSON.parse(result));
  // 读取不存在的数据
  console.log(localStorage.getItem('user'));  // 返回 null
  ```

- `removeItem(key)`：从`sessionStorage`或`localStorage`中删除对应`key`的一条数据。

  ```javascript
  sessionStorage.removeItem('msg')
  localStorage.removeItem('msg')
  ```

- `clear()`：将`sessionStorage`或`localStorage`中的所有数据清空。

  ```javascript
  // 清空 sessionStorage 中的数据
  sessionStorage.clear()

  // 清空 localStorage 中的数据
  localStorage.clear()
  ```
