---
title: 'MySQL 环境配置'
date: 2022-02-20 00:00:00
tags: [SQL,MySQL,环境搭建]
authors: Linner
categories: [memo]
---

# Ubuntu

MySQL 8 安装：

1.  首先更新本地存储库索引：

    ```bash
    sudo apt update
    ```

2.  从 APT 存储库安装 MySQL：

    ```bash
    sudo apt install MySQL-server -y
    ```

3.  查看 MySQL 版本，验证是否安装成功：

    ```bash
    mysql --version
    ```

4.  检查 MySQL 是否正在运行：

    ```bash
    systemctl status mysql.service
    ```

如果服务未运行：

```bash
sudo systemctl start mysql
```

***

MySQL 安装完成后需要更改密码。

1.  查看默认账户和密码：

    执行后找到 `user` 和 `password`，如：

    ```bash
    sudo vim /etc/mysql/debian.cnf
    ```

    或：

    ```bash
    sudo cat /etc/mysql/debian.cnf
    ```

    找到如下：

    ```纯文本
    user     = debian-sys-maint
    password = 12E0cDBeusG6vANp
    ```

2.  使用默认账户登录：

    然后输入默认密码：

    ```bash
    mysql -udebian-sys-maint -p
    ```

    ```纯文本
    12E0cDBeusG6vANp
    ```

3.  输入以下命令修改 root 密码：

    ```sql
    ALTER USER 'root'@'localhost' identified WITH mysql_native_password BY 'your_password';
    ```

4.  修改完成后执行：

    ```sql
    FLUSH privileges;
    QUIT
    ```

5.  最后重启 MySQL 后就可以使用 root 用户了：

    ```bash
    sudo service mysql restart
    ```

    ```bash
    mysql -uroot -p
    ```
