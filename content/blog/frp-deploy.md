---
title: '搭建 frp 服务'
date: 2022-07-23 00:00:00
tags: [Linux,Ubuntu]
authors: Linner
categories: [memo]
---

1.  下载frp：

    ```bash
    wget https://github.com/fatedier/frp/releases/download/v0.44.0/frp_0.44.0_linux_amd64.tar.gz
    ```

    或者访问[frp项目](https://github.com/fatedier/frp/ "frp项目")获取自己需要的版本。

2.  解压：

    ```bash
    tar -zxvf frp_0.44.0_linux_amd64.tar.gz
    ```

3.  进入目录后，配置`frps.ini`和`frpc.ini`：

    ```bash
    cd frp_0.44.0_linux_amd64
    ```

    *   服务端：

        ```bash
        vim frps.ini
        ```

        将文件按照下面修改：

        ```bash
        [common]
        # frp监听的端口，默认是7000，可以改成其他的
        bind_port = 7000
        # 授权码，请改成更复杂的
        token = 123456  # 这个token之后在客户端会用到

        # frp管理后台端口，请按自己需求更改
        dashboard_port = 7500
        # frp管理后台用户名和密码，请改成自己的
        dashboard_user = admin
        dashboard_pwd = admin
        enable_prometheus = true

        # frp日志配置
        log_file = /var/log/frps.log
        log_level = info
        log_max_days = 3
        ```

    *   客户端：

        ```bash
        vim frpc.ini
        ```

        将文件按照下面修改：

        ```bash
        # 客户端配置
        [common]
        server_addr = 服务器ip
        server_port = 7000 # 与frps.ini的bind_port一致
        token = 52010  # 与frps.ini的token一致

        # 配置ssh服务
        [ssh]
        type = tcp
        local_ip = 127.0.0.1 # 默认本地ip
        local_port = 22
        remote_port = 6000  # 这个自定义，之后再ssh连接的时候要用

        # 配置http服务，可用于小程序开发、远程调试等，如果没有可以不写下面的
        [web]
        type = http
        local_ip = 127.0.0.1
        local_port = 8080
        subdomain = test.hijk.pw  # web域名
        remote_port = 自定义的远程服务器端口，例如8080
        ```

4.  接着需要在防火墙开启相应的端口。

5.  启动服务：

    *   服务端：

        ```bash
        ./frps -s frps.ini
        ```

        或者配置`frps.server`文件：

        ```bash
        sudo vim /lib/systemd/system/frps.service
        ```

        ```bash
        [Unit]
        Description=The nginx HTTP and reverse proxy server
        After=network.target remote-fs.target nss-lookup.target

        [Service]
        Type=simple
        # 根据实际情况修改这条命令或者将frps、frps.ini移动到相应位置
        ExecStart=/usr/bin/frps -c /etc/frp/frps.ini
        KillSignal=SIGQUIT
        TimeoutStopSec=5
        KillMode=process
        PrivateTmp=true
        StandardOutput=syslog
        StandardError=inherit

        [Install]
        WantedBy=multi-user.target

        ```

    *   客户端：

        ```bash
        ./frpc -c frpc.ini
        ```

        如果是Linux也可以配置`frpc.service`：

        ```bash
        [Unit]
        Description=Frp Client Service
        After=network.target

        [Service]
        Type=simple
        #User=nobody
        Restart=on-failure
        RestartSec=5s
        # 根据实际情况修改以下两条命令或者将frps、frps.ini移动到相应位置
        ExecStart=/usr/bin/frpc -c /etc/frp/frpc.ini
        ExecReload=/usr/bin/frpc reload -c /etc/frp/frpc.ini

        [Install]
        WantedBy=multi-user.target


        ```

更多请见[Frp官方文档](https://frps.cn/11.html "Frp官方文档")。
