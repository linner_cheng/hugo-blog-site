---
# type: docs 
title: VS Code 中英字符对齐
date: 2023-08-10T08:40:27Z
featured: false
draft: false
comment: true
toc: false
reward: true
pinned: false
carousel: false
authors: Linner
categories: [memo]
tags: ["VS Code"]
images: []
---

在VS Code中，编写代码或者Markdown时，中文字符与英文字符通常是无法对齐的。可以通过使用Ubuntu Mono字体，让VS Code的中英字符能够对齐。

Ubuntu Mono字体可以从[https://github.com/powerline/fonts](https://github.com/powerline/fonts)下载。可以直接通过网页下载其`zip`压缩包，或者将其`clone`到本地：

```
git clone https://github.com/powerline/fonts
```

如果是在Windows上安装该字体，可以找到项目下的`UbuntuMono`文件夹，直接双击打开`.ttf`文件，根据显示的内容提示，就能完成安装。

找到`UbuntuMono`文件夹并打开，双击打开下方红框中所有`.tff`文件进行安装：

![uTools_1691657594650.png](uTools_1691657594650.png)

![uTools_1691657696387.png](uTools_1691657696387.png)

然后根据界面中显示的字体名称，修改VS Code的字体：

![uTools_1691657808584.png](uTools_1691657808584.png)

打开VS Code的设置，搜索`@id:editor.fontFamily`，然后将`Ubuntu Mono`字体加入后即修改成功：

![uTools_1691657918498.png](uTools_1691657918498.png)

如果要根据某种语言设置字体，可以在VS Code中按下 Ctrl+Shift+P，然后在打开的输入框中搜索`settings.json`：

![uTools_1691658592418.png](uTools_1691658592418.png)

Default Settings是VS Code默认的配置；Workspace Settings是当前工作区的配置（也就是当前工作目录）；User Settings是当前登录用户的配置；Markdown PDF这个配置是因为我已经进行过配置，所以才会显示。根据你的需要进行选择即可，这里我选择修改User Settings。

然后在打开的`settings.json`文件夹中按Ctrl+F查找`"[markdown]"`这个设置项（如果没有的话就自己创建）。在`"[markdown]"`项下，新增或修改如下内容：

```json
"[markdown]": {
    "editor.fontFamily": "Ubuntu Mono derivative Powerline",
},
```

如果觉得字体太小，可以：

```json
"[markdown]": {
    "editor.fontFamily": "Ubuntu Mono derivative Powerline",
    "editor.fontSize": 17
},
```
