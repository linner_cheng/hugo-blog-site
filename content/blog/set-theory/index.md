---
title: "集合论"
date: 2022-09-02T12:43:41+08:00
tags: ['数学']
draft: false
authors: Linner
categories: [note]
---

# 集合的概念

> - A **set** is a group of objects. (simplest way)
> - By a **set** we mean any collection M into a whole of definite disinct objects m (which we called **elements** of M) of our perception or thought. (Cantor's way)

**集合**（set）是由 <u>指定范围</u>内的<u>满足给定条件的所有对象</u>聚集在一起构成的，指定范围内的每一个对象称为这个集合的**元素**（element）。

- 集合中的元素是**无序**的。
- 集合中的元素是**不同**的（即，同个集合中相同或重复的元素被认为是一个元素）。

通常情况下，

- 用带（或不带）下标的 <u>大写</u>英文字母表示<u>集合</u>：$A,B,C,\dots,A_1,B_1,C_1,\dots$
- 用带（或不带）下标的 <u>小写</u>英文字母表示<u>元素</u>：$a,b,c,\dots,a_1,b_1,c_1,\dots$

ZFC 公理化集合论：

- 外延公理，
- 空集存在公理，
- 无序对公理，
- 并集公理，
- 幂集公理，
- 无穷公理，
- 替换公理，
- 正则公理，
- 选择公理。

---

## 常见的集合

- 空集 $\emptyset$；
- 正整数集 $\mathbf{N^+}$ or $\mathbf{W}$：$1,2,3,\cdots$
- 自然数集 $\mathbf{N}$：$0,1,2,3,\cdots$
- 整数集 $\mathbf{Z}$：$\cdots,-2,-1,0,1,2,\cdots$
- 质数/素数集 $\mathbf{P}$：$2,3,5,7,\cdots$
- 有理数集 $\mathbf{Q}$；
- 无理数集 $\mathbf{I}$；
- 实数集 $\mathbf{R}$；
- 复数集 $\mathbf{C}$；
- $\cdots\cdots$

关系：

$$
\mathbf{W} \subseteq \mathbf{N} \subseteq \mathbf{Z} \subseteq \mathbf{Q} \subseteq \mathbf{R} \subseteq \mathbf{C}
$$

---

# 集合的表示方法

## 枚举法

**枚举法**又称**列举法**或**显示法**，是枚举出集合中的所有或部分元素（要能看出其他元素之间的规律）。

Example：

- 枚举出集合中的所有元素：$A = \{a,b,c,d\}$；
- 枚举出集合中的部分元素：$B = \{1,3,5,\cdots,2n+1,\cdots\}$。

---

## 叙述法

**叙述法**又称**隐式法**，是通过刻画（或用自然语言描述）集合中元素所具备的某种特性来表示集合的方法，通常用符号 $P(x)$ 来表示不同对象 $x$ 所具有的性质 $P$ ，由 $P(x)$ 所定义的集合常记为

$$
\{x|P(x)\}。
$$

---

## 文氏图

**文氏图**（**Venn diagram**）又叫**维恩图**，用于展示集合或类之间的大致关系。

![维恩图](%E7%BB%B4%E6%81%A9%E5%9B%BE.png)

一般用方向表示全集，用圆形表示某一特定集合。

---

## 递归指定集合法

递归指定集合法是指通过计算规则定义集合中的元素的方法。

Example：

设 $a_0 = 1$，$a_{i+1}=2a_i(i \ge 0)$，定义 $S=\{a_0,a_1,\cdots,a_n,\cdots\}=\{a_k|k\ge0\}$，可以得出集合 $S$ 为

$$
S=\{1,2,2^2,\cdots,2^n,\cdots\}。
$$

---

## 归纳法

1. 指出集合至少要包含的元素

   - 第一部分：基础，指出某些最<u>基本元素</u>属于某集合；
   - 第二部分：归纳，指出<u>由基本元素构造新元素的方法</u>；

2. 指出集合至多要包含的元素

   - 第三部分：极小性，指出该<u>集合的界限</u>。

---

# 基数

Definition：

集合 $A$ 中的元素个数称为集合的**基数**（base number），记为 $|A|$。

对于任意集合 $A$ 来说，

- 若 $|A|$ 是<u>有限的</u>，称该集合为**有限集**（finite set）；
- 若 $|A|$ 是<u>无限的</u>，称该集合为**无限集**（infinite set）。

---

# 集合与元素的关系

元素与集合之间有两种关系：

- 属于：如 “$a$ 是集合 $A$ 中的元素” 或 “$a$ 属于 $A$ ” 记为

  $$
  a \in A。
  $$

- 不属于：如 “$a$ 不是是集合 $A$ 中的元素” 或 “$a$ 不属于 $A$” 记为

  $$
  a \notin A。
  $$

---

# 集合与集合的关系

## 外延性原理

Theorem：

两个集合 $A$ 和 $B$ **相等**，当且仅当它们的**元素完全相同**，记为 $A\ =\ B$，否则 $A$ 和 $B$ **不相等**，记为 $A\ \neq\ B$。

---

## 包含关系

Definitions：

设 $A$，$B$ 是任意两个集合，

- **包含与不包含**：如果 $B$ 的每个元素都是 $A$ 中的元素，则称 $B$ 是 $A$ 的**子集**（subset），也称 ${B}$ **被** ${A}$ **包含**或 ${A}$ **包含** ${B}$，记作 ${B \subseteq A}$ 或 $A \supseteq B$，称 $\subseteq$ 或 $\supseteq$ 为**被包含关系**（included relation）或**包含关系**（inclusion relation）；否则记作 ${B \nsubseteq A}$。

  “$\subseteq$” 定义的数学语言描述为：

  $$
  B \subseteq A\
  \Longleftrightarrow\ \forall x，
  如果 x \subseteq B，
  则 x \subseteq A。
  $$

  由子集的定义可推出 $A \subseteq A$。

- **真包含**：如果 $B \subseteq A$ 并且 $A \neq B$，则称 $B$ 是 $A$ 的**真子集**（proper subset），也称做 ${B}$ **被** ${A}$ **真包含**或 ${A}$ **真包含** ${B}$，记作 ${B \subset A}$，称 $\subset$ 为**真包含关系**（properly inclusion relation）。

  “$\subset$” 定义的数学语言描述为：

  $$
  B \subset A \Longleftrightarrow 对 \forall x，若 x \in B，则 x \in A，并且 \exists y \in A，但 y \notin B。
  $$

---

## 相等关系

Theorem：

设 $A$，$B$ 为任意两个集合，则 ${A\ =\ B}\ \Longleftrightarrow\ {A \subseteq B}$ 并且 ${B \subseteq A}$。

---

# 常见特殊的集合

## 空集

Definition：

不含任何元素的集合叫做**空集**（empty set），记作 $\emptyset$。

$$
\emptyset = \{x|x \neq x\}
$$

- 空集是一切集合的子集。
- 空集是**绝对唯一**的。

Example：

- $|\emptyset| = 0$，
- $|\{\emptyset\}| = 1$。

---

### 证明空集是绝对唯一的

> 对 “唯一性” 的证明通常采用反证法（先假设 “不唯一”，得出矛盾，从而证明 “唯一性” 是正确的）。

证明：

假设有两个不同的空集 $\emptyset_1$ 和 $\emptyset_2$ ，由空集是一切集合的子集得

$$
\empty_1 \subseteq \emptyset_2\ 和\ \emptyset_2 \subseteq \emptyset_1
$$

根据集合的相等关系，得 $\emptyset_1 = \emptyset_2$，与假设矛盾。因此空集是绝对唯一的。

---

## 全集

Definition：

在一个相对固定的范围内，包含此范围内所有元素的集合，称为**全集**或**论集**（universal set），用 $U$ 或 $E$ 表示。

在维恩图中一般用方形表示全集。

![全集](%E5%85%A8%E9%9B%86.png)

全集是**相对唯一**的。

---

## m 元子集

Definition：

如果一个集合含有 $n$ 个元素，则称集合 $A$ 为 $n$ 元子集，称含有 $A$ 中 $m(0 \le m \le n)$个元素的子集为它的 $m$ 元子集。

对于任意 $n$ 元集合 $A$，它的 $m$ 元（$0 \le m \le n$）子集（包含空集）个数为 $C_n^m$ 个，所以不同的子集个数为：

$$
C_n^0 + C_n^1 + \cdots + C_n^n = (1 + 1)^n = 2^n。
\tag{1}
$$

---

## 幂集

Definition：

设 $A$ 为任意集合，把 $A$ 的所有不同子集构成的集合叫做 $A$ 的**幂集**（power set），记作 $P(A)$，即

$$
P(A)\ =\ \{x|x \subseteq A\}。
$$

由公式 $(1)$ 可得 $|A| = 2^n(n=|A|)$。

幂集也叫做**集族**（family of set）或**集合的集合**。

> 对集族的研究在数学方面、知识库和表处理语言以及人工智能等方面都有十分重要的意义。

---

# 集合的运算

## 集合运算的定义

Definition：

设 $U$ 是全集，$A$、$B$ 是 $U$ 的两个子集，则

1. “$\cup$” **并运算**（union operation）：$A \cup B = \{x|x \in A\ or\ x \in B\}$ 是 $A$ 与 $B$ 的**并集**（union）。

   ![并集](%E5%B9%B6%E9%9B%86.png)

   并集可代表两个集合 “相加”。

2. “$\cap$” **交运算**（intersection operation）：$A \cap B = \{x|x \in A\ and\ B\}$ 是 $A$ 与 $B$ 的**交集**（intersection）。

   ![交集](%E4%BA%A4%E9%9B%86.png)

3. “$\overline{A}$” **补运算**（complement operation）：$\overline{A} = U - A$ 是集合 $A$ 的**补集**（complement）（也可记为 $A'$、$~A$、$A^c$ 等）。$A$ 对于全集 $U$ 的补集是**绝对补集**。

   ![补集](%E8%A1%A5%E9%9B%86.png)

4. “$-$” **差运算**（subtraction operation）：$A-B=\{x|x \in A\ and\ x \notin B\} = A \cap \overline{B}$ 是 $A$ 与 $B$ 的**差集**（subtraction)，又称 $B$ 在 $A$ 中的**相对补集**。

   ![差集](%E5%B7%AE%E9%9B%86.png)

   当 $A=U$ 时，$A-B=\overline{B}$。

5. “$\oplus$” **对称差运算**（symmetric difference operation）：$A \oplus B = \{x|(x \in A\ and\ x \notin B)\ or\ (x\in B\ and\ x \notin A\} = (A-B) \cup (B-A)$ 是 $A$ 与 $B$ 的**对称差集**（symmetric difference of set）。

   ![相对补集](%E7%9B%B8%E5%AF%B9%E8%A1%A5%E9%9B%86.png)

扩展：

设 $A_1,A_2,\cdots,A_n$ 是任意 $n$ 个集合，则

- 这 $\mathbf n$ **个集合的并集**是包含那些**至少是这组集合中一个集合成员的元素**的集合，即

  $$
  \displaystyle \bigcup_{i=1}^{n}{A_i}
  = A_1 \cup A_2 \cup \cdots \cup A_n
  = \{x|x \in A_1\ or\ x \in A_2 \cdots or\ x \in A_n\}
  $$
  
- 这 $\mathbf n$ **个集合的交集**是那些属于**这组集合中所有集合成员的元素**的集合，即

  $$
  \displaystyle \bigcap_{i=1}^{n}{A_i}
  = A_1 \cap A_2 \cap \cdots \cap A_n
  = \{x|x \in A_1\ and\ x \in A_2 \cdots and\ x \in A_n\}
  $$

当 $n$ 无限增大时，可记为

- $\displaystyle \bigcup^{\infin}_{i=1}A_i = A_1 \cup A_2 \cup \cdots$
- $\displaystyle \bigcap^{\infin}_{i=1}A_i = A_1 \cap A_2 \cap \cdots$

---

## 集合运算的基本等式

设 $U$ 为全集，$A$，$B$，$C$ 为任意集合，

   性质    | 等式
:--------: | :---------------------------------
  幂等率   | $A \cup A = A$<br>$A \cap A = A$
  交换律   | $A \cup B = B \cup A$<br>$A \cap B = B \cap A$
  结合律   | $A \cup (B \cup C) = (A \cup B) \cup C$<br>$A \cap (B \cap C) = (A \cap B) \cap C$
  同一律   | $A \cup \emptyset = A$<br>$A \cap U = A$
   零律    | $A \cup U = U$<br>$A \cap \emptyset = \emptyset$
  分配律   | $A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$<br>$A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
  吸收律   | $A \cup (A \cap B) = A$<br>$A \cap (A \cup B)$
  矛盾律   | $\overline{A} \cap A = \emptyset$
  排中律   | $\overline{A} \cup A = U$
双重否定律 | $\overline{\overline{A}}=A$
 德摩根律  | $\overline{A \cup B} = \overline{A} \cap \overline{B}$<br>$\overline{A \cap B} = \overline{A}$

Example：

![分配律](%E5%88%86%E9%85%8D%E5%BE%8B.png)

证明德摩根律的等式之一：$\overline{A \cup B} = \overline{A} \cap \overline{B}$

证明：

1. 证明 $\overline{A \cup B} \subseteq \overline{A} \cap \overline{B}$

   $\begin{aligned} \forall x \in \overline{A \cup B} \quad & \Rightarrow\ x \notin A \cup B\ \Rightarrow\ x \notin A\ and\ x \notin B\\ & \Rightarrow x \in \overline{A}\ and\ x \in \overline{B}\ \Rightarrow\ x \in \overline{A} \cap \overline{B}， \end{aligned}$

   即 $\overline{A \cup B} \subseteq \overline{A} \cap \overline{B}$；

2. 证明 $\overline{A} \cap \overline{B} \subseteq \overline{A \cup B}$

   $\begin{aligned} \forall x \in \overline{A} \cap \overline{B} \quad & \Rightarrow\ x \in \overline{A}\ and\ x \in \overline{B}\ \Rightarrow\ x \notin A\ and\ x \notin B\\ & \Rightarrow\ x \notin A \cup B\ \Rightarrow\ x \in \overline{A} \cap \overline{B}， \end{aligned}$

   即 $\overline{A} \cap \overline{B} \subseteq \overline{A \cup B}$。

综上可得 $\overline{A \cup B} = \overline{A} \cap \overline{B}$。

---

# 无限集合

有限集合与无限集合的根本区别是：对于无限集合，表面上个数完全不相等的两个集合之间仍可能存在等势关系，如集合与真子集之间。

## 等势

**冯·诺依曼的自然数定义**：

基于基数，利用一个集合的序列来定义自然数。

1. $\emptyset \in \mathbf{N}$；
2. 若 $n \in \mathbf{N}$，则 $n' \equiv n \cup \{n\} \in \mathbf{N}$。

从而，这个<u>集合序列的基数</u>可以来定义自然数：

- $0 \equiv \emptyset$；
- $1 \equiv \emptyset \cup \{\emptyset\} = \{\emptyset\} = \{0\}$；
- $2 \equiv \{\emptyset\} \cup \{\{\emptyset\}\} = \{\emptyset, \{\emptyset\}\} = \{0,1\}$；
- $\cdots$
- $n \equiv \{0,1,2,3,\cdots,n-1\}$；
- $\cdots$
- $\mathbf{N} \equiv \{0,1,2,\cdots,n,\cdots\}$。

> 实际上，任意含有 $n$ 个元素的集合都可以用 $n$ 表示。即，任意两个基数相同的集合之间都可以建立一一对应关系。

---

Definition：

设 $A$，$B$ 为两个集合，若在 $A$，$B$ 之间存在一种一一对应的关系：

$$
\Psi:\ A \rightarrow B
$$

则称 $A$ 与 $B$ 是**等势的**（equipotential），记作：

$$
A \sim B
$$

也称集合 $A$、$B$ **等势**（equipotent）。

由等势定义可得，如果 $A = B$，那么 $A \sim B$，反之则不一定成立。

Theorem：

1. 两个有限集合等式当且仅当它们有相同的元素个数。
2. 有限集合不和其任何真子集等势。
3.<u>可数集合可以与其可数的真子集等势</u>。

---

## 可数集合

Definition：

凡与自然数集合 $\mathbf{N}$ 等势的集合，称之为**可数集合**（countable set），该类集合的基数记为 $\aleph_0$（aleph，阿列夫）。

Example：

证明以下集合是可数集合，

1. $O^+ = \{x|x \in \mathbf{N}，x是正奇数\}$

   在 $O^+$ 与 $\mathbf{N}$ 之间建立一个一一对应关系 $\varphi_1 : \mathbf{N} \rightarrow O^+$：

   $$
   \begin{matrix}
   0& 1& 2& \cdots& n& \cdots\\
   \downarrow& \downarrow& \downarrow& \downarrow& \downarrow& \downarrow\\
   1& 3& 5& \cdots& 2n+1& \cdots&\\
   \end{matrix}
   $$

   所以 $O^+$ 是可数集合。

2. $P = \{x|x \in \mathbf{N}，x是素数\}$

   在 $P$ 与 $\mathbf{N}$ 之间建立一个一一对应关系 $\varphi_2 : \mathbf{N} \rightarrow P$：

   $$
   \begin{matrix}
   0& 1& 2& 3& 4& \cdots\\
   \downarrow& \downarrow& \downarrow& \downarrow& \downarrow& \downarrow\\
   2& 3& 5& 7& 11& \cdots&\\
   \end{matrix}
   $$

   所以 $P$ 是可数集合。

3. 有理数集合 $\mathbf{Q}$

   将 $\mathbf{Q}$ 中的所有元素都写成 $p/q$（$p$、$q$ 是整数，且 $q \neq 0$）的形式，从 $0/1^{[0]}$ 开始，将所有有理数与自然数一一配对（其中 $p/q^{[n]}$ 的上标 $[n]$ 代表对应于该有理数的自然数）：

   ![证明Q是可数集合](%E8%AF%81%E6%98%8EQ%E6%98%AF%E5%8F%AF%E6%95%B0%E9%9B%86%E5%90%88-16308441770882.jpg)

   所以 $\mathbf{Q}$ 是可数集合。

---

## 不可数集合

Definition：

开区间 $(0,1)$ 称为不可数集合，凡**与开区间 $(0,1)$ 等势**的集合，都称为**不可数集合**，该类集合的基数记为 $\aleph$（或 $\aleph_1$）。

Example：

1. 闭区间 $[0,1]$ 是不可数集合。

   证明：在 $[0,1]$ 和 $(0,1)$ 之间建立如下对应关系：

   $$
   R:
   \begin{cases}
   \begin{matrix}
   0& \rightarrow& 1/4 \\
   1& \rightarrow& 1/2 \\
   \cfrac{1}{2^n}& \rightarrow& \cfrac{1}{2^{n+2}},& n=1,2,3,\cdots \\
   n& \rightarrow& n,& 其他 n \in (0,1)\\
   \end{matrix}
   \end{cases}
   $$

   显然 $[0,1]$ 与 $(0,1)$ 是等势的，所以 $[0,1]$ 是不可数集合。

2. 实数集 $\mathbf R$ 是不可数集合。

   证明：在实数集 $\mathbf R$ 和开区间 $(0,1)$ 之间建立如下对应关系：

   $$
   n \rightarrow \tan{\pi\bigg(\cfrac{2n-1}{2}\bigg)}
   $$

   显然 $(0,1)$ 与 $\mathbf{R}$ 之间是等势的，所以 $\mathbf{R}$ 是一个不可数集合。
