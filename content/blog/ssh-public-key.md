---
title: 'SSH 公钥配置'
date: 2022-08-13 00:00:00
tags: [Linux,Ubuntu]
authors: Linner
categories: [memo]
---

创建`.ssh`目录并生成公钥:

```bash
ssh-keygen -t rsa
```

所有选项全部回车, 使用默认选项即可.

然后切换到用户目录下的`.ssh`目录:

```bash
cd ~/.ssh
```

该目录下默认有三个文件:

- `id_rsa`: 用户私钥, 不要复制发送给任何人.
- `id_rsa.pub`: 公钥, 将里面内容发送给客户端.
- `authorized_keys`: 信任的公钥.

只需要将`id_rsa.pub`里的内容以 (`ssh-rsa`开头的一行内容) 复制到客户端的`authorized_keys`的目录下即可.

接着在客户端使用:

```bash
ssh username@ip -p port
```

即可连接到服务器.
