---
title: 'vbs 后台运行 bat'
date: 2022-01-12 00:00:00
tags: [Windows]
authors: Linner
categories: [memo]
---

假设有三个 bat 程序需要后台运行（称为 `A.bat`、`B.bat`、`C.bat`）：

```vbs
Set ws = CreateObject("Wscript.Shell")
ws.run "cmd /c A.bat",0
ws.run "cmd /c B.bat",0
ws.run "cmd /c C.bat",0
```
