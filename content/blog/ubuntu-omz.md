---
title: 'Ubuntu 安装 Oh my zsh'
date: 2022-01-26 00:00:00
tags: [Linux,Ubuntu,Shell]
authors: Linner
categories: [memo]
---

oh-my-zsh主页： [https://ohmyz.sh/](https://ohmyz.sh/ "https://ohmyz.sh/")

# 安装并设置zsh

安装zsh：

```bash
sudo apt install zsh
```

查看系统中所有的shell：

```bash
cat /etc/shells
```

```纯文本
# /etc/shells: valid login shells
/bin/sh
/bin/bash
/usr/bin/bash
/bin/rbash
/usr/bin/rbash
/bin/dash
/usr/bin/dash
/usr/bin/tmux
/usr/bin/screen
/bin/zsh
/usr/bin/zsh
```

将zsh设置为默认的shell：

```bash
sudo chsh -s /bin/zsh
```

重新连接到Ubuntu，然后查看当前默认shell：

```bash
echo $SHELL
```

如果显示`/bin/zsh`则配置成功。

***

# 安装 oh-my-zsh

下载install.sh：

```bash
wget https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/install.sh

```

给install.sh添加权限：

```bash
chmod +x install.sh
```

执行install.sh：

```bash
./install.sh
```

> 📌在执行install.sh之前还需要安装git。否则会报错。
>
> 安装git：
>
> ```bash
> sudo apt install git
> ```

***

# 配置zsh

zsh的配置文件为：`~/.zshrc`。

```bash
vim ~/.zshrc
```

## 修改主题

主题配置项为：`ZSH_THEME`。

默认主题配置为：

```text
ZSH_THEME="robbyrussell"
```

可以修改为其中之一：

```text
ZSH_THEME="ys"
```

```text
ZSH_THEME="geoffgarside"
```

还可以访问[https://github.com/ohmyzsh/ohmyzsh/wiki/Themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes "https://github.com/ohmyzsh/ohmyzsh/wiki/Themes") 查看更多主题。
