---
title: 'Linux 终端中文编码设置'
date: 2022-09-13 00:00:00
tags: [Linux,Ubuntu,Debian]
authors: Linner
categories: [memo]
---

# Ubuntu

安装中文支持：

```bash
apt-get install language-pack-zh-hans -y
```

查看系统语言包：

```bash
locale -a
```

修改`~/.bashrc`，加入：

```bash
export LC_ALL=zh_CN.UTF-8
export LANG=zh_CN.UTF-8
```

修改`/etc/default/locale`：

```bash
LANG="zh_CN.UTF-8"
```

# Debian

安装`locales`：

```bash
apt install locales -y
```

安装完成后可能会提示设置`locales`，如果安装完之后想要更改相关设置或者没有提示设置，可以使用如下命令重新设置`locales`相关信息：

```bash
dpkg-reconfigure locales
```

# ZSH

修改`~/.zshrc`，加入：

```bash
export LC_ALL=zh_CN.UTF-8
export LANG=zh_CN.UTF-8
```
