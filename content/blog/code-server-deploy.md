---
title: 'Code Server 部署'
date: 2022-07-23 00:00:00
tags: [Linux,Ubuntu]
authors: Linner
categories: [memo]
---


1.  下载code-server：

    ```bash
    wget https://github.com/coder/code-server/releases/download/v4.5.1/code-server-4.5.1-linux-amd64.tar.gz
    ```

    可以访问[code-server GitHub 页面](https://github.com/coder/code-server "code-server GitHub 页面")获取下载连接。

2.  解压：

    ```bash
    tar -zxvf code-server-4.5.1-linux-amd64.tar.gz
    ```

3.  将解压后的文件移动到`/opt`目录下，并重命名：

    ```bash
    mv code-server-4.5.1-linux-amd64 /opt/code-server
    ```

4.  创建软链到`/usr/bin`目录下：

    ```bash
    ln -s /opt/code-server/code-server /usr/bin/code-server
    ```

5.  修改配置文件`~/.config/code-server/config.yaml`：

    ```bash
    vim ~/.config/code-server/config.yaml
    ```

6.  添加code-server服务：

    ```bash
    vim /lib/systemd/system/codeweb.service
    ```

    写入如下内容：

    ```bash
    [Unit]
    Description=The nginx HTTP and reverse proxy server
    After=network.target remote-fs.target nss-lookup.target

    [Service]
    Type=simple
    ExecStart=/usr/bin/code-server
    KillSignal=SIGQUIT
    TimeoutStopSec=5
    KillMode=process
    PrivateTmp=true
    StandardOutput=syslog
    StandardError=inherit

    [Install]
    WantedBy=multi-user.target
    ```
