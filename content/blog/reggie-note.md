---
title: '瑞吉外卖项目实战'
date: 2023-03-21 00:00:00
tags: [Java,Spring,Maven,SpringBoot]
keywords: [瑞吉外卖,项目实战,Reggie]
authors: Linner
categories: [memo]
series: [Spring]
# pinned: true
# pinnedWeight: 2
---

# 项目笔记

项目笔记仓库：[https://gitee.com/linner_cheng/reggie_note](https://gitee.com/linner_cheng/reggie_note)

B站视频：[黑马程序员Java项目实战《瑞吉外卖》](https://www.bilibili.com/video/BV13a411q753/?p=190&spm_id_from=333.1007.top_right_bar_window_history.content.click)

---

# 项目基础功能开发

[项目基础功能开发](https://gitee.com/linner_cheng/reggie_note/tree/v1.0/)。包含项目前台和后台功能开发还有项目部署。

---

# Redis

[Redis](https://gitee.com/linner_cheng/reggie_note/tree/redis/)。包含Jedis和Spirng data redis。

---

# 项目优化

[Redis缓存](https://gitee.com/linner_cheng/reggie_note/tree/v1.1/)。

[Sharding-JDBC读写分离](https://gitee.com/linner_cheng/reggie_note/tree/v1.2/)。

[Swagger API文档](https://gitee.com/linner_cheng/reggie_note/tree/v1.3/)。

