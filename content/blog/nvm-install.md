---
# type: docs 
title: NVM 安装
date: 2023-08-06T05:31:54Z
featured: false
draft: false
comment: true
toc: true
reward: true
pinned: false
carousel: false
categories: [memo]
authors: Linner
series: ["Node.js"]
---

# Debian 安装 NVM

NVM（Node Version Manager）是一个用于管理 Node.js 版本的工具。它允许你在同一台机器上同时安装和切换不同的 Node.js 版本，以便于在不同项目中使用特定的 Node.js 版本。

NVM 支持在 Linux、macOS、和 Windows 系统上使用，并且与不同的 Shell（如 Bash、Zsh）兼容。

在Debian上安装NVM的步骤如下（Ubuntu同理）：

1. 安装NVM之前，需要先安装依赖：

    ```shell
    sudo apt update
    sudo apt upgrade
    sudo apt install curl build-essential gnupg2 -y
    ```

2. 下载并执行NVM的安装脚本：

    ```shell
    curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
    ```

    如果你要指定安装NVM的版本，可以修改以下命令：

    ```shell
    curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
    ```

    将`v0.38.0`换成你所需的版本即可。

3. 等待下载，并且脚本执行成功后，重新加载终端的配置信息：

    ```shell
    source .bashrc
    ```

    如果你使用的是ZSH，可以：

    ```shell
    source .zshrc
    ```

    不同的终端，重新加载对应的配置文件即可。

---

# 安装脚本无法下载的问题

如果遇到NVM的`install.sh`无法下载的问题，例如`curl`报以下错误：

```
curl: (6) Could not resolve host: raw.githubusercontent.com
```

多半是域名污染导致的。就算你使用科学的方法，也不一定能下载成功，还是会报一样的错误。

解决这种问题可以先从域名IP查询网站上查询IP：[https://www.ipaddress.com/](https://www.ipaddress.com/)。

然后将查询到的IP通过本地的`hosts`文件来解析。例如：

```shell
vim /etc/hosts
```

然后加入以下内容：

```
185.199.108.133 raw.githubusercontent.com
```

上方的`185.199.108.133`这个IP，替换成你查询到的结果即可。
