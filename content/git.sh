#!/bin/bash

### 配置信息-START ###
# WORK_DIR="/root/my-blog" # 工作目录
WORK_DIR=".."
PUBLIC_DIR="public"     # 生成的网站页面的目录（工作目录的相对路径）
### 配置信息-END ###

add(){
    if [[ "$1" == "" || $public_flag == true ]]
    then
        git add --all
    else
        args=()
        for arg in $@
        do
            args[$index]="$CONTENT_DIR/$arg"
        done
        git add $args
    fi
}

commit(){
    add ${@:2}
    git commit -m "$1"
}

public_push(){
    if [ $public_flag == true ]
    then
        echo "[local-repo]:"
        git push local main
        echo "--------------------------------"
        echo "[remote-repo]:"
    fi
}

push(){
    if [ "$1" == "" ]
    then
        public_push
        git push
    elif [ "$1" == "-o" ]
    then
        git push ${@:2}
    else
        # echo "--------------------------------"
        commit "$1" ${@:2}
        echo "--------------------------------"
        public_push
        git push
    fi
}

pull(){
    git pull $@
}

# 命令参数解析
param(){
    if [ "$1" == "add" ]
    then
        add ${@:2}
    elif [ "$1" == "commit" ]
    then
        commit "$2" ${@:3}
    elif [ "$1" == "push" ]
    then
        push "$2" ${@:3}
    elif [ "$1" == "pull" ]
    then
        pull "$2" ${@:3}
    else
        echo "请正确指定参数"
    fi
}

public_flag=false

public_fun(){
    public_flag=true
    cd "$PUBLIC_DIR"
    PUBLIC_DIR=`pwd`
    param $1 "$2" ${@:3}
    public_flag=false
    cd $WORK_DIR
}

cd $WORK_DIR
WORK_DIR=`pwd`
CONTENT_DIR="$WORK_DIR/content"

if [ "$1" == "public" ]
then
    public_fun $2 "$3"
elif [ "$1" == "all" ]
then
    echo "[$WORK_DIR]："
    # pull
    param $2 "$3"
    echo "================================================================"
    hugo
    echo "================================================================"
    echo "[$WORK_DIR/$PUBLIC_DIR]："
    public_fun $2 "$3"
elif [[ "$1" == "add" || "$1" == "commit" || "$1" == "push" || "$1" == "pull" ]]
then
    param $1 "$2" ${@:3}
else
    echo "请正确使用命令"
fi
